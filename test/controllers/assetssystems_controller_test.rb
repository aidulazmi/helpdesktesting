require 'test_helper'

class AssetssystemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assetssystem = assetssystems(:one)
  end

  test "should get index" do
    get assetssystems_url
    assert_response :success
  end

  test "should get new" do
    get new_assetssystem_url
    assert_response :success
  end

  test "should create assetssystem" do
    assert_difference('Assetssystem.count') do
      post assetssystems_url, params: { assetssystem: { lokasi: @assetssystem.lokasi, nama: @assetssystem.nama, nama_perangkat: @assetssystem.nama_perangkat, tahun: @assetssystem.tahun, type: @assetssystem.type } }
    end

    assert_redirected_to assetssystem_url(Assetssystem.last)
  end

  test "should show assetssystem" do
    get assetssystem_url(@assetssystem)
    assert_response :success
  end

  test "should get edit" do
    get edit_assetssystem_url(@assetssystem)
    assert_response :success
  end

  test "should update assetssystem" do
    patch assetssystem_url(@assetssystem), params: { assetssystem: { lokasi: @assetssystem.lokasi, nama: @assetssystem.nama, nama_perangkat: @assetssystem.nama_perangkat, tahun: @assetssystem.tahun, type: @assetssystem.type } }
    assert_redirected_to assetssystem_url(@assetssystem)
  end

  test "should destroy assetssystem" do
    assert_difference('Assetssystem.count', -1) do
      delete assetssystem_url(@assetssystem)
    end

    assert_redirected_to assetssystems_url
  end
end
