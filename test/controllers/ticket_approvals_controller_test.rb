require 'test_helper'

class TicketApprovalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket_approval = ticket_approvals(:one)
  end

  test "should get index" do
    get ticket_approvals_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_approval_url
    assert_response :success
  end

  test "should create ticket_approval" do
    assert_difference('TicketApproval.count') do
      post ticket_approvals_url, params: { ticket_approval: { catatan: @ticket_approval.catatan, status: @ticket_approval.status, ticket_id: @ticket_approval.ticket_id, user_id: @ticket_approval.user_id } }
    end

    assert_redirected_to ticket_approval_url(TicketApproval.last)
  end

  test "should show ticket_approval" do
    get ticket_approval_url(@ticket_approval)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_approval_url(@ticket_approval)
    assert_response :success
  end

  test "should update ticket_approval" do
    patch ticket_approval_url(@ticket_approval), params: { ticket_approval: { catatan: @ticket_approval.catatan, status: @ticket_approval.status, ticket_id: @ticket_approval.ticket_id, user_id: @ticket_approval.user_id } }
    assert_redirected_to ticket_approval_url(@ticket_approval)
  end

  test "should destroy ticket_approval" do
    assert_difference('TicketApproval.count', -1) do
      delete ticket_approval_url(@ticket_approval)
    end

    assert_redirected_to ticket_approvals_url
  end
end
