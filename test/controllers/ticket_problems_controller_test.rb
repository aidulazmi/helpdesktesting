require 'test_helper'

class TicketProblemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket_problem = ticket_problems(:one)
  end

  test "should get index" do
    get ticket_problems_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_problem_url
    assert_response :success
  end

  test "should create ticket_problem" do
    assert_difference('TicketProblem.count') do
      post ticket_problems_url, params: { ticket_problem: { kategori: @ticket_problem.kategori, keterangan: @ticket_problem.keterangan, no_ticket_problem: @ticket_problem.no_ticket_problem, priority: @ticket_problem.priority, status: @ticket_problem.status, sub_kategori: @ticket_problem.sub_kategori, teknisi: @ticket_problem.teknisi, ticket_id: @ticket_problem.ticket_id } }
    end

    assert_redirected_to ticket_problem_url(TicketProblem.last)
  end

  test "should show ticket_problem" do
    get ticket_problem_url(@ticket_problem)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_problem_url(@ticket_problem)
    assert_response :success
  end

  test "should update ticket_problem" do
    patch ticket_problem_url(@ticket_problem), params: { ticket_problem: { kategori: @ticket_problem.kategori, keterangan: @ticket_problem.keterangan, no_ticket_problem: @ticket_problem.no_ticket_problem, priority: @ticket_problem.priority, status: @ticket_problem.status, sub_kategori: @ticket_problem.sub_kategori, teknisi: @ticket_problem.teknisi, ticket_id: @ticket_problem.ticket_id } }
    assert_redirected_to ticket_problem_url(@ticket_problem)
  end

  test "should destroy ticket_problem" do
    assert_difference('TicketProblem.count', -1) do
      delete ticket_problem_url(@ticket_problem)
    end

    assert_redirected_to ticket_problems_url
  end
end
