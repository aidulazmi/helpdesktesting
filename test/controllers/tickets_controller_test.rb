require 'test_helper'

class TicketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get tickets_url
    assert_response :success
  end

  test "should get new" do
    get new_ticket_url
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post tickets_url, params: { ticket: { area_pemilik_kendala: @ticket.area_pemilik_kendala, area_pemohon: @ticket.area_pemohon, asset_id: @ticket.asset_id, divisi_pemilik_kendala: @ticket.divisi_pemilik_kendala, divisi_pemohon: @ticket.divisi_pemohon, jenis_layanan: @ticket.jenis_layanan, kategori: @ticket.kategori, keterangan: @ticket.keterangan, nama_pemilik_kendala: @ticket.nama_pemilik_kendala, nama_pemohon: @ticket.nama_pemohon, no_tiket: @ticket.no_tiket, status_teknisi: @ticket.status_teknisi, status_tiket: @ticket.status_tiket, sub_kategori: @ticket.sub_kategori, teknisi: @ticket.teknisi } }
    end

    assert_redirected_to ticket_url(Ticket.last)
  end

  test "should show ticket" do
    get ticket_url(@ticket)
    assert_response :success
  end

  test "should get edit" do
    get edit_ticket_url(@ticket)
    assert_response :success
  end

  test "should update ticket" do
    patch ticket_url(@ticket), params: { ticket: { area_pemilik_kendala: @ticket.area_pemilik_kendala, area_pemohon: @ticket.area_pemohon, asset_id: @ticket.asset_id, divisi_pemilik_kendala: @ticket.divisi_pemilik_kendala, divisi_pemohon: @ticket.divisi_pemohon, jenis_layanan: @ticket.jenis_layanan, kategori: @ticket.kategori, keterangan: @ticket.keterangan, nama_pemilik_kendala: @ticket.nama_pemilik_kendala, nama_pemohon: @ticket.nama_pemohon, no_tiket: @ticket.no_tiket, status_teknisi: @ticket.status_teknisi, status_tiket: @ticket.status_tiket, sub_kategori: @ticket.sub_kategori, teknisi: @ticket.teknisi } }
    assert_redirected_to ticket_url(@ticket)
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete ticket_url(@ticket)
    end

    assert_redirected_to tickets_url
  end
end
