require 'test_helper'

class FeatureAssignmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @feature_assignment = feature_assignments(:one)
  end

  test "should get index" do
    get feature_assignments_url
    assert_response :success
  end

  test "should get new" do
    get new_feature_assignment_url
    assert_response :success
  end

  test "should create feature_assignment" do
    assert_difference('FeatureAssignment.count') do
      post feature_assignments_url, params: { feature_assignment: { feature_id: @feature_assignment.feature_id, role_id: @feature_assignment.role_id } }
    end

    assert_redirected_to feature_assignment_url(FeatureAssignment.last)
  end

  test "should show feature_assignment" do
    get feature_assignment_url(@feature_assignment)
    assert_response :success
  end

  test "should get edit" do
    get edit_feature_assignment_url(@feature_assignment)
    assert_response :success
  end

  test "should update feature_assignment" do
    patch feature_assignment_url(@feature_assignment), params: { feature_assignment: { feature_id: @feature_assignment.feature_id, role_id: @feature_assignment.role_id } }
    assert_redirected_to feature_assignment_url(@feature_assignment)
  end

  test "should destroy feature_assignment" do
    assert_difference('FeatureAssignment.count', -1) do
      delete feature_assignment_url(@feature_assignment)
    end

    assert_redirected_to feature_assignments_url
  end
end
