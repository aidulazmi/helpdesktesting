require 'test_helper'

class AssetlisensisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assetlisensi = assetlisensis(:one)
  end

  test "should get index" do
    get assetlisensis_url
    assert_response :success
  end

  test "should get new" do
    get new_assetlisensi_url
    assert_response :success
  end

  test "should create assetlisensi" do
    assert_difference('Assetlisensi.count') do
      post assetlisensis_url, params: { assetlisensi: { divisi: @assetlisensi.divisi, jumlah_lisensi: @assetlisensi.jumlah_lisensi, keterangan: @assetlisensi.keterangan, lokasi: @assetlisensi.lokasi, nama_lisensi: @assetlisensi.nama_lisensi, nama_perangkat: @assetlisensi.nama_perangkat, penerima: @assetlisensi.penerima, pic: @assetlisensi.pic, serial_number: @assetlisensi.serial_number, tahun_pembelian: @assetlisensi.tahun_pembelian, type_lisensi: @assetlisensi.type_lisensi, upload: @assetlisensi.upload, waktu_expired: @assetlisensi.waktu_expired, waktu_instalasi: @assetlisensi.waktu_instalasi } }
    end

    assert_redirected_to assetlisensi_url(Assetlisensi.last)
  end

  test "should show assetlisensi" do
    get assetlisensi_url(@assetlisensi)
    assert_response :success
  end

  test "should get edit" do
    get edit_assetlisensi_url(@assetlisensi)
    assert_response :success
  end

  test "should update assetlisensi" do
    patch assetlisensi_url(@assetlisensi), params: { assetlisensi: { divisi: @assetlisensi.divisi, jumlah_lisensi: @assetlisensi.jumlah_lisensi, keterangan: @assetlisensi.keterangan, lokasi: @assetlisensi.lokasi, nama_lisensi: @assetlisensi.nama_lisensi, nama_perangkat: @assetlisensi.nama_perangkat, penerima: @assetlisensi.penerima, pic: @assetlisensi.pic, serial_number: @assetlisensi.serial_number, tahun_pembelian: @assetlisensi.tahun_pembelian, type_lisensi: @assetlisensi.type_lisensi, upload: @assetlisensi.upload, waktu_expired: @assetlisensi.waktu_expired, waktu_instalasi: @assetlisensi.waktu_instalasi } }
    assert_redirected_to assetlisensi_url(@assetlisensi)
  end

  test "should destroy assetlisensi" do
    assert_difference('Assetlisensi.count', -1) do
      delete assetlisensi_url(@assetlisensi)
    end

    assert_redirected_to assetlisensis_url
  end
end
