require 'test_helper'

class LossEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @loss_event = loss_events(:one)
  end

  test "should get index" do
    get loss_events_url
    assert_response :success
  end

  test "should get new" do
    get new_loss_event_url
    assert_response :success
  end

  test "should create loss_event" do
    assert_difference('LossEvent.count') do
      post loss_events_url, params: { loss_event: { deskripsi_kejadian: @loss_event.deskripsi_kejadian, kategori: @loss_event.kategori, nama: @loss_event.nama, pic: @loss_event.pic, status: @loss_event.status, tanggal: @loss_event.tanggal, tempat: @loss_event.tempat } }
    end

    assert_redirected_to loss_event_url(LossEvent.last)
  end

  test "should show loss_event" do
    get loss_event_url(@loss_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_loss_event_url(@loss_event)
    assert_response :success
  end

  test "should update loss_event" do
    patch loss_event_url(@loss_event), params: { loss_event: { deskripsi_kejadian: @loss_event.deskripsi_kejadian, kategori: @loss_event.kategori, nama: @loss_event.nama, pic: @loss_event.pic, status: @loss_event.status, tanggal: @loss_event.tanggal, tempat: @loss_event.tempat } }
    assert_redirected_to loss_event_url(@loss_event)
  end

  test "should destroy loss_event" do
    assert_difference('LossEvent.count', -1) do
      delete loss_event_url(@loss_event)
    end

    assert_redirected_to loss_events_url
  end
end
