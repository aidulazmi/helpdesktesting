require 'test_helper'

class AssetssitsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assetssit = assetssits(:one)
  end

  test "should get index" do
    get assetssits_url
    assert_response :success
  end

  test "should get new" do
    get new_assetssit_url
    assert_response :success
  end

  test "should create assetssit" do
    assert_difference('Assetssit.count') do
      post assetssits_url, params: { assetssit: { merk: @assetssit.merk, nama_perangkat: @assetssit.nama_perangkat } }
    end

    assert_redirected_to assetssit_url(Assetssit.last)
  end

  test "should show assetssit" do
    get assetssit_url(@assetssit)
    assert_response :success
  end

  test "should get edit" do
    get edit_assetssit_url(@assetssit)
    assert_response :success
  end

  test "should update assetssit" do
    patch assetssit_url(@assetssit), params: { assetssit: { merk: @assetssit.merk, nama_perangkat: @assetssit.nama_perangkat } }
    assert_redirected_to assetssit_url(@assetssit)
  end

  test "should destroy assetssit" do
    assert_difference('Assetssit.count', -1) do
      delete assetssit_url(@assetssit)
    end

    assert_redirected_to assetssits_url
  end
end
