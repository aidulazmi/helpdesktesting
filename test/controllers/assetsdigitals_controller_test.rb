require 'test_helper'

class AssetsdigitalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assetsdigital = assetsdigitals(:one)
  end

  test "should get index" do
    get assetsdigitals_url
    assert_response :success
  end

  test "should get new" do
    get new_assetsdigital_url
    assert_response :success
  end

  test "should create assetsdigital" do
    assert_difference('Assetsdigital.count') do
      post assetsdigitals_url, params: { assetsdigital: { antivirus: @assetsdigital.antivirus, availability: @assetsdigital.availability, bahasa: @assetsdigital.bahasa, biaya: @assetsdigital.biaya, biaya_hardware: @assetsdigital.biaya_hardware, biaya_jaringan: @assetsdigital.biaya_jaringan, biaya_keamanan: @assetsdigital.biaya_keamanan, biaya_pemeliharaan: @assetsdigital.biaya_pemeliharaan, bug_fixing: @assetsdigital.bug_fixing, current_version: @assetsdigital.current_version, deskripsi: @assetsdigital.deskripsi, domain: @assetsdigital.domain, framework: @assetsdigital.framework, internet: @assetsdigital.internet, intruder_incident: @assetsdigital.intruder_incident, jam_layanan: @assetsdigital.jam_layanan, lisensi: @assetsdigital.lisensi, nama_aplikasi: @assetsdigital.nama_aplikasi, pemeliharaan: @assetsdigital.pemeliharaan, pemilik_proses: @assetsdigital.pemilik_proses, periode: @assetsdigital.periode, pic: @assetsdigital.pic, proteksi_akses: @assetsdigital.proteksi_akses, retensi: @assetsdigital.retensi, server: @assetsdigital.server, server_hardening: @assetsdigital.server_hardening, storage_server: @assetsdigital.storage_server, string: @assetsdigital.string, total: @assetsdigital.total, uji_keamanan: @assetsdigital.uji_keamanan, waktu_eskalasi: @assetsdigital.waktu_eskalasi, waktu_respon: @assetsdigital.waktu_respon } }
    end

    assert_redirected_to assetsdigital_url(Assetsdigital.last)
  end

  test "should show assetsdigital" do
    get assetsdigital_url(@assetsdigital)
    assert_response :success
  end

  test "should get edit" do
    get edit_assetsdigital_url(@assetsdigital)
    assert_response :success
  end

  test "should update assetsdigital" do
    patch assetsdigital_url(@assetsdigital), params: { assetsdigital: { antivirus: @assetsdigital.antivirus, availability: @assetsdigital.availability, bahasa: @assetsdigital.bahasa, biaya: @assetsdigital.biaya, biaya_hardware: @assetsdigital.biaya_hardware, biaya_jaringan: @assetsdigital.biaya_jaringan, biaya_keamanan: @assetsdigital.biaya_keamanan, biaya_pemeliharaan: @assetsdigital.biaya_pemeliharaan, bug_fixing: @assetsdigital.bug_fixing, current_version: @assetsdigital.current_version, deskripsi: @assetsdigital.deskripsi, domain: @assetsdigital.domain, framework: @assetsdigital.framework, internet: @assetsdigital.internet, intruder_incident: @assetsdigital.intruder_incident, jam_layanan: @assetsdigital.jam_layanan, lisensi: @assetsdigital.lisensi, nama_aplikasi: @assetsdigital.nama_aplikasi, pemeliharaan: @assetsdigital.pemeliharaan, pemilik_proses: @assetsdigital.pemilik_proses, periode: @assetsdigital.periode, pic: @assetsdigital.pic, proteksi_akses: @assetsdigital.proteksi_akses, retensi: @assetsdigital.retensi, server: @assetsdigital.server, server_hardening: @assetsdigital.server_hardening, storage_server: @assetsdigital.storage_server, string: @assetsdigital.string, total: @assetsdigital.total, uji_keamanan: @assetsdigital.uji_keamanan, waktu_eskalasi: @assetsdigital.waktu_eskalasi, waktu_respon: @assetsdigital.waktu_respon } }
    assert_redirected_to assetsdigital_url(@assetsdigital)
  end

  test "should destroy assetsdigital" do
    assert_difference('Assetsdigital.count', -1) do
      delete assetsdigital_url(@assetsdigital)
    end

    assert_redirected_to assetsdigitals_url
  end
end
