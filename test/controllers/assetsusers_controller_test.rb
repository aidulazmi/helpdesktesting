require 'test_helper'

class AssetsusersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @assetsuser = assetsusers(:one)
  end

  test "should get index" do
    get assetsusers_url
    assert_response :success
  end

  test "should get new" do
    get new_assetsuser_url
    assert_response :success
  end

  test "should create assetsuser" do
    assert_difference('Assetsuser.count') do
      post assetsusers_url, params: { assetsuser: { divisi: @assetsuser.divisi, jenis_kontrak: @assetsuser.jenis_kontrak, keterangan: @assetsuser.keterangan, lokasi: @assetsuser.lokasi, merk: @assetsuser.merk, nama_komputer: @assetsuser.nama_komputer, nama_perangkat: @assetsuser.nama_perangkat, nip: @assetsuser.nip, no_inventaris: @assetsuser.no_inventaris, penyedia: @assetsuser.penyedia, tahun: @assetsuser.tahun, tipe: @assetsuser.tipe, user: @assetsuser.user, wo_ke: @assetsuser.wo_ke } }
    end

    assert_redirected_to assetsuser_url(Assetsuser.last)
  end

  test "should show assetsuser" do
    get assetsuser_url(@assetsuser)
    assert_response :success
  end

  test "should get edit" do
    get edit_assetsuser_url(@assetsuser)
    assert_response :success
  end

  test "should update assetsuser" do
    patch assetsuser_url(@assetsuser), params: { assetsuser: { divisi: @assetsuser.divisi, jenis_kontrak: @assetsuser.jenis_kontrak, keterangan: @assetsuser.keterangan, lokasi: @assetsuser.lokasi, merk: @assetsuser.merk, nama_komputer: @assetsuser.nama_komputer, nama_perangkat: @assetsuser.nama_perangkat, nip: @assetsuser.nip, no_inventaris: @assetsuser.no_inventaris, penyedia: @assetsuser.penyedia, tahun: @assetsuser.tahun, tipe: @assetsuser.tipe, user: @assetsuser.user, wo_ke: @assetsuser.wo_ke } }
    assert_redirected_to assetsuser_url(@assetsuser)
  end

  test "should destroy assetsuser" do
    assert_difference('Assetsuser.count', -1) do
      delete assetsuser_url(@assetsuser)
    end

    assert_redirected_to assetsusers_url
  end
end
