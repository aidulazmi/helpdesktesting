require "application_system_test_case"

class TicketsTest < ApplicationSystemTestCase
  setup do
    @ticket = tickets(:one)
  end

  test "visiting the index" do
    visit tickets_url
    assert_selector "h1", text: "Tickets"
  end

  test "creating a Ticket" do
    visit tickets_url
    click_on "New Ticket"

    fill_in "Area pemilik kendala", with: @ticket.area_pemilik_kendala
    fill_in "Area pemohon", with: @ticket.area_pemohon
    fill_in "Asset", with: @ticket.asset_id
    fill_in "Divisi pemilik kendala", with: @ticket.divisi_pemilik_kendala
    fill_in "Divisi pemohon", with: @ticket.divisi_pemohon
    fill_in "Jenis layanan", with: @ticket.jenis_layanan
    fill_in "Kategori", with: @ticket.kategori
    fill_in "Keterangan", with: @ticket.keterangan
    fill_in "Nama pemilik kendala", with: @ticket.nama_pemilik_kendala
    fill_in "Nama pemohon", with: @ticket.nama_pemohon
    fill_in "No tiket", with: @ticket.no_tiket
    fill_in "Status teknisi", with: @ticket.status_teknisi
    fill_in "Status tiket", with: @ticket.status_tiket
    fill_in "Sub kategori", with: @ticket.sub_kategori
    fill_in "Teknisi", with: @ticket.teknisi
    click_on "Create Ticket"

    assert_text "Ticket was successfully created"
    click_on "Back"
  end

  test "updating a Ticket" do
    visit tickets_url
    click_on "Edit", match: :first

    fill_in "Area pemilik kendala", with: @ticket.area_pemilik_kendala
    fill_in "Area pemohon", with: @ticket.area_pemohon
    fill_in "Asset", with: @ticket.asset_id
    fill_in "Divisi pemilik kendala", with: @ticket.divisi_pemilik_kendala
    fill_in "Divisi pemohon", with: @ticket.divisi_pemohon
    fill_in "Jenis layanan", with: @ticket.jenis_layanan
    fill_in "Kategori", with: @ticket.kategori
    fill_in "Keterangan", with: @ticket.keterangan
    fill_in "Nama pemilik kendala", with: @ticket.nama_pemilik_kendala
    fill_in "Nama pemohon", with: @ticket.nama_pemohon
    fill_in "No tiket", with: @ticket.no_tiket
    fill_in "Status teknisi", with: @ticket.status_teknisi
    fill_in "Status tiket", with: @ticket.status_tiket
    fill_in "Sub kategori", with: @ticket.sub_kategori
    fill_in "Teknisi", with: @ticket.teknisi
    click_on "Update Ticket"

    assert_text "Ticket was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket" do
    visit tickets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket was successfully destroyed"
  end
end
