require "application_system_test_case"

class AssetlisensisTest < ApplicationSystemTestCase
  setup do
    @assetlisensi = assetlisensis(:one)
  end

  test "visiting the index" do
    visit assetlisensis_url
    assert_selector "h1", text: "Assetlisensis"
  end

  test "creating a Assetlisensi" do
    visit assetlisensis_url
    click_on "New Assetlisensi"

    fill_in "Divisi", with: @assetlisensi.divisi
    fill_in "Jumlah lisensi", with: @assetlisensi.jumlah_lisensi
    fill_in "Keterangan", with: @assetlisensi.keterangan
    fill_in "Lokasi", with: @assetlisensi.lokasi
    fill_in "Nama lisensi", with: @assetlisensi.nama_lisensi
    fill_in "Nama perangkat", with: @assetlisensi.nama_perangkat
    fill_in "Penerima", with: @assetlisensi.penerima
    fill_in "Pic", with: @assetlisensi.pic
    fill_in "Serial number", with: @assetlisensi.serial_number
    fill_in "Tahun pembelian", with: @assetlisensi.tahun_pembelian
    fill_in "Type lisensi", with: @assetlisensi.type_lisensi
    fill_in "Upload", with: @assetlisensi.upload
    fill_in "Waktu expired", with: @assetlisensi.waktu_expired
    fill_in "Waktu instalasi", with: @assetlisensi.waktu_instalasi
    click_on "Create Assetlisensi"

    assert_text "Assetlisensi was successfully created"
    click_on "Back"
  end

  test "updating a Assetlisensi" do
    visit assetlisensis_url
    click_on "Edit", match: :first

    fill_in "Divisi", with: @assetlisensi.divisi
    fill_in "Jumlah lisensi", with: @assetlisensi.jumlah_lisensi
    fill_in "Keterangan", with: @assetlisensi.keterangan
    fill_in "Lokasi", with: @assetlisensi.lokasi
    fill_in "Nama lisensi", with: @assetlisensi.nama_lisensi
    fill_in "Nama perangkat", with: @assetlisensi.nama_perangkat
    fill_in "Penerima", with: @assetlisensi.penerima
    fill_in "Pic", with: @assetlisensi.pic
    fill_in "Serial number", with: @assetlisensi.serial_number
    fill_in "Tahun pembelian", with: @assetlisensi.tahun_pembelian
    fill_in "Type lisensi", with: @assetlisensi.type_lisensi
    fill_in "Upload", with: @assetlisensi.upload
    fill_in "Waktu expired", with: @assetlisensi.waktu_expired
    fill_in "Waktu instalasi", with: @assetlisensi.waktu_instalasi
    click_on "Update Assetlisensi"

    assert_text "Assetlisensi was successfully updated"
    click_on "Back"
  end

  test "destroying a Assetlisensi" do
    visit assetlisensis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assetlisensi was successfully destroyed"
  end
end
