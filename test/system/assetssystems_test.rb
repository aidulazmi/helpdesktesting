require "application_system_test_case"

class AssetssystemsTest < ApplicationSystemTestCase
  setup do
    @assetssystem = assetssystems(:one)
  end

  test "visiting the index" do
    visit assetssystems_url
    assert_selector "h1", text: "Assetssystems"
  end

  test "creating a Assetssystem" do
    visit assetssystems_url
    click_on "New Assetssystem"

    fill_in "Lokasi", with: @assetssystem.lokasi
    fill_in "Nama", with: @assetssystem.nama
    fill_in "Nama perangkat", with: @assetssystem.nama_perangkat
    fill_in "Tahun", with: @assetssystem.tahun
    fill_in "Type", with: @assetssystem.type
    click_on "Create Assetssystem"

    assert_text "Assetssystem was successfully created"
    click_on "Back"
  end

  test "updating a Assetssystem" do
    visit assetssystems_url
    click_on "Edit", match: :first

    fill_in "Lokasi", with: @assetssystem.lokasi
    fill_in "Nama", with: @assetssystem.nama
    fill_in "Nama perangkat", with: @assetssystem.nama_perangkat
    fill_in "Tahun", with: @assetssystem.tahun
    fill_in "Type", with: @assetssystem.type
    click_on "Update Assetssystem"

    assert_text "Assetssystem was successfully updated"
    click_on "Back"
  end

  test "destroying a Assetssystem" do
    visit assetssystems_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assetssystem was successfully destroyed"
  end
end
