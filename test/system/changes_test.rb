require "application_system_test_case"

class ChangesTest < ApplicationSystemTestCase
  setup do
    @change = changes(:one)
  end

  test "visiting the index" do
    visit changes_url
    assert_selector "h1", text: "Changes"
  end

  test "creating a Change" do
    visit changes_url
    click_on "New Change"

    fill_in "Deskripsi", with: @change.deskripsi
    fill_in "Judul", with: @change.judul
    fill_in "Kategori asset", with: @change.kategori_asset
    fill_in "Nama aplikasi", with: @change.nama_aplikasi
    fill_in "Nama perangkat", with: @change.nama_perangkat
    fill_in "Pemilik proses", with: @change.pemilik_proses
    fill_in "Ram", with: @change.ram
    fill_in "Tanggal", with: @change.tanggal
    fill_in "Teknisi", with: @change.teknisi
    click_on "Create Change"

    assert_text "Change was successfully created"
    click_on "Back"
  end

  test "updating a Change" do
    visit changes_url
    click_on "Edit", match: :first

    fill_in "Deskripsi", with: @change.deskripsi
    fill_in "Judul", with: @change.judul
    fill_in "Kategori asset", with: @change.kategori_asset
    fill_in "Nama aplikasi", with: @change.nama_aplikasi
    fill_in "Nama perangkat", with: @change.nama_perangkat
    fill_in "Pemilik proses", with: @change.pemilik_proses
    fill_in "Ram", with: @change.ram
    fill_in "Tanggal", with: @change.tanggal
    fill_in "Teknisi", with: @change.teknisi
    click_on "Update Change"

    assert_text "Change was successfully updated"
    click_on "Back"
  end

  test "destroying a Change" do
    visit changes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Change was successfully destroyed"
  end
end
