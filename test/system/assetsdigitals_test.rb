require "application_system_test_case"

class AssetsdigitalsTest < ApplicationSystemTestCase
  setup do
    @assetsdigital = assetsdigitals(:one)
  end

  test "visiting the index" do
    visit assetsdigitals_url
    assert_selector "h1", text: "Assetsdigitals"
  end

  test "creating a Assetsdigital" do
    visit assetsdigitals_url
    click_on "New Assetsdigital"

    fill_in "Antivirus", with: @assetsdigital.antivirus
    fill_in "Availability", with: @assetsdigital.availability
    fill_in "Bahasa", with: @assetsdigital.bahasa
    fill_in "Biaya", with: @assetsdigital.biaya
    fill_in "Biaya hardware", with: @assetsdigital.biaya_hardware
    fill_in "Biaya jaringan", with: @assetsdigital.biaya_jaringan
    fill_in "Biaya keamanan", with: @assetsdigital.biaya_keamanan
    fill_in "Biaya pemeliharaan", with: @assetsdigital.biaya_pemeliharaan
    fill_in "Bug fixing", with: @assetsdigital.bug_fixing
    fill_in "Current version", with: @assetsdigital.current_version
    fill_in "Deskripsi", with: @assetsdigital.deskripsi
    fill_in "Domain", with: @assetsdigital.domain
    fill_in "Framework", with: @assetsdigital.framework
    fill_in "Internet", with: @assetsdigital.internet
    fill_in "Intruder incident", with: @assetsdigital.intruder_incident
    fill_in "Jam layanan", with: @assetsdigital.jam_layanan
    fill_in "Lisensi", with: @assetsdigital.lisensi
    fill_in "Nama aplikasi", with: @assetsdigital.nama_aplikasi
    fill_in "Pemeliharaan", with: @assetsdigital.pemeliharaan
    fill_in "Pemilik proses", with: @assetsdigital.pemilik_proses
    fill_in "Periode", with: @assetsdigital.periode
    fill_in "Pic", with: @assetsdigital.pic
    fill_in "Proteksi akses", with: @assetsdigital.proteksi_akses
    fill_in "Retensi", with: @assetsdigital.retensi
    fill_in "Server", with: @assetsdigital.server
    fill_in "Server hardening", with: @assetsdigital.server_hardening
    fill_in "Storage server", with: @assetsdigital.storage_server
    fill_in "String", with: @assetsdigital.string
    fill_in "Total", with: @assetsdigital.total
    fill_in "Uji keamanan", with: @assetsdigital.uji_keamanan
    fill_in "Waktu eskalasi", with: @assetsdigital.waktu_eskalasi
    fill_in "Waktu respon", with: @assetsdigital.waktu_respon
    click_on "Create Assetsdigital"

    assert_text "Assetsdigital was successfully created"
    click_on "Back"
  end

  test "updating a Assetsdigital" do
    visit assetsdigitals_url
    click_on "Edit", match: :first

    fill_in "Antivirus", with: @assetsdigital.antivirus
    fill_in "Availability", with: @assetsdigital.availability
    fill_in "Bahasa", with: @assetsdigital.bahasa
    fill_in "Biaya", with: @assetsdigital.biaya
    fill_in "Biaya hardware", with: @assetsdigital.biaya_hardware
    fill_in "Biaya jaringan", with: @assetsdigital.biaya_jaringan
    fill_in "Biaya keamanan", with: @assetsdigital.biaya_keamanan
    fill_in "Biaya pemeliharaan", with: @assetsdigital.biaya_pemeliharaan
    fill_in "Bug fixing", with: @assetsdigital.bug_fixing
    fill_in "Current version", with: @assetsdigital.current_version
    fill_in "Deskripsi", with: @assetsdigital.deskripsi
    fill_in "Domain", with: @assetsdigital.domain
    fill_in "Framework", with: @assetsdigital.framework
    fill_in "Internet", with: @assetsdigital.internet
    fill_in "Intruder incident", with: @assetsdigital.intruder_incident
    fill_in "Jam layanan", with: @assetsdigital.jam_layanan
    fill_in "Lisensi", with: @assetsdigital.lisensi
    fill_in "Nama aplikasi", with: @assetsdigital.nama_aplikasi
    fill_in "Pemeliharaan", with: @assetsdigital.pemeliharaan
    fill_in "Pemilik proses", with: @assetsdigital.pemilik_proses
    fill_in "Periode", with: @assetsdigital.periode
    fill_in "Pic", with: @assetsdigital.pic
    fill_in "Proteksi akses", with: @assetsdigital.proteksi_akses
    fill_in "Retensi", with: @assetsdigital.retensi
    fill_in "Server", with: @assetsdigital.server
    fill_in "Server hardening", with: @assetsdigital.server_hardening
    fill_in "Storage server", with: @assetsdigital.storage_server
    fill_in "String", with: @assetsdigital.string
    fill_in "Total", with: @assetsdigital.total
    fill_in "Uji keamanan", with: @assetsdigital.uji_keamanan
    fill_in "Waktu eskalasi", with: @assetsdigital.waktu_eskalasi
    fill_in "Waktu respon", with: @assetsdigital.waktu_respon
    click_on "Update Assetsdigital"

    assert_text "Assetsdigital was successfully updated"
    click_on "Back"
  end

  test "destroying a Assetsdigital" do
    visit assetsdigitals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assetsdigital was successfully destroyed"
  end
end
