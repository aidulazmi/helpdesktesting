require "application_system_test_case"

class AssetssitsTest < ApplicationSystemTestCase
  setup do
    @assetssit = assetssits(:one)
  end

  test "visiting the index" do
    visit assetssits_url
    assert_selector "h1", text: "Assetssits"
  end

  test "creating a Assetssit" do
    visit assetssits_url
    click_on "New Assetssit"

    fill_in "Merk", with: @assetssit.merk
    fill_in "Nama perangkat", with: @assetssit.nama_perangkat
    click_on "Create Assetssit"

    assert_text "Assetssit was successfully created"
    click_on "Back"
  end

  test "updating a Assetssit" do
    visit assetssits_url
    click_on "Edit", match: :first

    fill_in "Merk", with: @assetssit.merk
    fill_in "Nama perangkat", with: @assetssit.nama_perangkat
    click_on "Update Assetssit"

    assert_text "Assetssit was successfully updated"
    click_on "Back"
  end

  test "destroying a Assetssit" do
    visit assetssits_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assetssit was successfully destroyed"
  end
end
