require "application_system_test_case"

class FeatureAssignmentsTest < ApplicationSystemTestCase
  setup do
    @feature_assignment = feature_assignments(:one)
  end

  test "visiting the index" do
    visit feature_assignments_url
    assert_selector "h1", text: "Feature Assignments"
  end

  test "creating a Feature assignment" do
    visit feature_assignments_url
    click_on "New Feature Assignment"

    fill_in "Feature", with: @feature_assignment.feature_id
    fill_in "Role", with: @feature_assignment.role_id
    click_on "Create Feature assignment"

    assert_text "Feature assignment was successfully created"
    click_on "Back"
  end

  test "updating a Feature assignment" do
    visit feature_assignments_url
    click_on "Edit", match: :first

    fill_in "Feature", with: @feature_assignment.feature_id
    fill_in "Role", with: @feature_assignment.role_id
    click_on "Update Feature assignment"

    assert_text "Feature assignment was successfully updated"
    click_on "Back"
  end

  test "destroying a Feature assignment" do
    visit feature_assignments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Feature assignment was successfully destroyed"
  end
end
