require "application_system_test_case"

class MessageTemplatesTest < ApplicationSystemTestCase
  setup do
    @message_template = message_templates(:one)
  end

  test "visiting the index" do
    visit message_templates_url
    assert_selector "h1", text: "Message Templates"
  end

  test "creating a Message template" do
    visit message_templates_url
    click_on "New Message Template"

    fill_in "Jenis layanan", with: @message_template.jenis_layanan
    fill_in "Sub category", with: @message_template.sub_category
    fill_in "Template", with: @message_template.template
    click_on "Create Message template"

    assert_text "Message template was successfully created"
    click_on "Back"
  end

  test "updating a Message template" do
    visit message_templates_url
    click_on "Edit", match: :first

    fill_in "Jenis layanan", with: @message_template.jenis_layanan
    fill_in "Sub category", with: @message_template.sub_category
    fill_in "Template", with: @message_template.template
    click_on "Update Message template"

    assert_text "Message template was successfully updated"
    click_on "Back"
  end

  test "destroying a Message template" do
    visit message_templates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Message template was successfully destroyed"
  end
end
