require "application_system_test_case"

class TicketProblemsTest < ApplicationSystemTestCase
  setup do
    @ticket_problem = ticket_problems(:one)
  end

  test "visiting the index" do
    visit ticket_problems_url
    assert_selector "h1", text: "Ticket Problems"
  end

  test "creating a Ticket problem" do
    visit ticket_problems_url
    click_on "New Ticket Problem"

    fill_in "Kategori", with: @ticket_problem.kategori
    fill_in "Keterangan", with: @ticket_problem.keterangan
    fill_in "No ticket problem", with: @ticket_problem.no_ticket_problem
    fill_in "Priority", with: @ticket_problem.priority
    fill_in "Status", with: @ticket_problem.status
    fill_in "Sub kategori", with: @ticket_problem.sub_kategori
    fill_in "Teknisi", with: @ticket_problem.teknisi
    fill_in "Ticket", with: @ticket_problem.ticket_id
    click_on "Create Ticket problem"

    assert_text "Ticket problem was successfully created"
    click_on "Back"
  end

  test "updating a Ticket problem" do
    visit ticket_problems_url
    click_on "Edit", match: :first

    fill_in "Kategori", with: @ticket_problem.kategori
    fill_in "Keterangan", with: @ticket_problem.keterangan
    fill_in "No ticket problem", with: @ticket_problem.no_ticket_problem
    fill_in "Priority", with: @ticket_problem.priority
    fill_in "Status", with: @ticket_problem.status
    fill_in "Sub kategori", with: @ticket_problem.sub_kategori
    fill_in "Teknisi", with: @ticket_problem.teknisi
    fill_in "Ticket", with: @ticket_problem.ticket_id
    click_on "Update Ticket problem"

    assert_text "Ticket problem was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket problem" do
    visit ticket_problems_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket problem was successfully destroyed"
  end
end
