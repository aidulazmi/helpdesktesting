require "application_system_test_case"

class TicketApprovalsTest < ApplicationSystemTestCase
  setup do
    @ticket_approval = ticket_approvals(:one)
  end

  test "visiting the index" do
    visit ticket_approvals_url
    assert_selector "h1", text: "Ticket Approvals"
  end

  test "creating a Ticket approval" do
    visit ticket_approvals_url
    click_on "New Ticket Approval"

    fill_in "Catatan", with: @ticket_approval.catatan
    fill_in "Status", with: @ticket_approval.status
    fill_in "Ticket", with: @ticket_approval.ticket_id
    fill_in "User", with: @ticket_approval.user_id
    click_on "Create Ticket approval"

    assert_text "Ticket approval was successfully created"
    click_on "Back"
  end

  test "updating a Ticket approval" do
    visit ticket_approvals_url
    click_on "Edit", match: :first

    fill_in "Catatan", with: @ticket_approval.catatan
    fill_in "Status", with: @ticket_approval.status
    fill_in "Ticket", with: @ticket_approval.ticket_id
    fill_in "User", with: @ticket_approval.user_id
    click_on "Update Ticket approval"

    assert_text "Ticket approval was successfully updated"
    click_on "Back"
  end

  test "destroying a Ticket approval" do
    visit ticket_approvals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ticket approval was successfully destroyed"
  end
end
