require "application_system_test_case"

class AssetsusersTest < ApplicationSystemTestCase
  setup do
    @assetsuser = assetsusers(:one)
  end

  test "visiting the index" do
    visit assetsusers_url
    assert_selector "h1", text: "Assetsusers"
  end

  test "creating a Assetsuser" do
    visit assetsusers_url
    click_on "New Assetsuser"

    fill_in "Divisi", with: @assetsuser.divisi
    fill_in "Jenis kontrak", with: @assetsuser.jenis_kontrak
    fill_in "Keterangan", with: @assetsuser.keterangan
    fill_in "Lokasi", with: @assetsuser.lokasi
    fill_in "Merk", with: @assetsuser.merk
    fill_in "Nama komputer", with: @assetsuser.nama_komputer
    fill_in "Nama perangkat", with: @assetsuser.nama_perangkat
    fill_in "Nip", with: @assetsuser.nip
    fill_in "No inventaris", with: @assetsuser.no_inventaris
    fill_in "Penyedia", with: @assetsuser.penyedia
    fill_in "Tahun", with: @assetsuser.tahun
    fill_in "Tipe", with: @assetsuser.tipe
    fill_in "User", with: @assetsuser.user
    fill_in "Wo ke", with: @assetsuser.wo_ke
    click_on "Create Assetsuser"

    assert_text "Assetsuser was successfully created"
    click_on "Back"
  end

  test "updating a Assetsuser" do
    visit assetsusers_url
    click_on "Edit", match: :first

    fill_in "Divisi", with: @assetsuser.divisi
    fill_in "Jenis kontrak", with: @assetsuser.jenis_kontrak
    fill_in "Keterangan", with: @assetsuser.keterangan
    fill_in "Lokasi", with: @assetsuser.lokasi
    fill_in "Merk", with: @assetsuser.merk
    fill_in "Nama komputer", with: @assetsuser.nama_komputer
    fill_in "Nama perangkat", with: @assetsuser.nama_perangkat
    fill_in "Nip", with: @assetsuser.nip
    fill_in "No inventaris", with: @assetsuser.no_inventaris
    fill_in "Penyedia", with: @assetsuser.penyedia
    fill_in "Tahun", with: @assetsuser.tahun
    fill_in "Tipe", with: @assetsuser.tipe
    fill_in "User", with: @assetsuser.user
    fill_in "Wo ke", with: @assetsuser.wo_ke
    click_on "Update Assetsuser"

    assert_text "Assetsuser was successfully updated"
    click_on "Back"
  end

  test "destroying a Assetsuser" do
    visit assetsusers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Assetsuser was successfully destroyed"
  end
end
