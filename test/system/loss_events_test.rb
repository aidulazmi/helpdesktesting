require "application_system_test_case"

class LossEventsTest < ApplicationSystemTestCase
  setup do
    @loss_event = loss_events(:one)
  end

  test "visiting the index" do
    visit loss_events_url
    assert_selector "h1", text: "Loss Events"
  end

  test "creating a Loss event" do
    visit loss_events_url
    click_on "New Loss Event"

    fill_in "Deskripsi kejadian", with: @loss_event.deskripsi_kejadian
    fill_in "Kategori", with: @loss_event.kategori
    fill_in "Nama", with: @loss_event.nama
    fill_in "Pic", with: @loss_event.pic
    fill_in "Status", with: @loss_event.status
    fill_in "Tanggal", with: @loss_event.tanggal
    fill_in "Tempat", with: @loss_event.tempat
    click_on "Create Loss event"

    assert_text "Loss event was successfully created"
    click_on "Back"
  end

  test "updating a Loss event" do
    visit loss_events_url
    click_on "Edit", match: :first

    fill_in "Deskripsi kejadian", with: @loss_event.deskripsi_kejadian
    fill_in "Kategori", with: @loss_event.kategori
    fill_in "Nama", with: @loss_event.nama
    fill_in "Pic", with: @loss_event.pic
    fill_in "Status", with: @loss_event.status
    fill_in "Tanggal", with: @loss_event.tanggal
    fill_in "Tempat", with: @loss_event.tempat
    click_on "Update Loss event"

    assert_text "Loss event was successfully updated"
    click_on "Back"
  end

  test "destroying a Loss event" do
    visit loss_events_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Loss event was successfully destroyed"
  end
end
