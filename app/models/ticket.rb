class Ticket < ActiveRecord::Base
    belongs_to :status_ticket
    has_many :ticket_history, dependent: :destroy
    has_many :ticket_problem, dependent: :destroy 
    has_many :ticket_approval,  dependent: :destroy #dependent: :destroy 
    has_many_attached  :lampiran
    has_many :notification_read_ticket, dependent: :destroy
    attr_accessor :current_user
    #attr_accessor :current_user, :keterangan
    # after_create :updateStatus
    after_update :auto_reminder
    after_create :update_and_autorespond
    belongs_to :user, foreign_key: :nama_pemilik_kendala, primary_key: :name

    def name_or_username
      self.nama_pemilik_kendala.strip == self.nama_pemilik_kendala.strip.downcase ? self.nama_pemilik_kendala.strip : nil
    end

    # assetuser
    # belongs_to :assetsuser, foreign_key: 'nama_pemilik_kendala', primary_key: 'user'
    # belongs_to :manual_assetsuser, foreign_key: 'nama_pemilik_kendala', primary_key: 'manual_user', class_name: 'AssetsUser', optional: true
    

    
    def updateStatus
        @checkTechnician = Ticket.find_by(teknisi: self.teknisi)
        if @checkTechnician.status_teknisi.present?
            #Ticket.update(self.id, {:status_tiket => keterangan, :status_teknisi => "Waiting"})
            @checkWaiting = Ticket.where('teknisi LIKE ? and status_teknisi LIKE ?', "%#{self.teknisi}%", '%Waiting%')
            if @checkWaiting.exists?
                Ticket.update(self.id, {:status_tiket => "Waiting", :status_teknisi => "Waiting"})
                TicketHistory.create(
                    :ticket_id => self.id,
                    :no_tiket => self.no_tiket,
                    :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                    :inputer => self.nama_pemohon,
                    :status_tiket => "Waiting",
                    :keterangan => self.keterangan,
                    #:keterangan => keterangan,
                    :issued_by => current_user.name
                )
                
            else
                Ticket.update(self.id,  {:status_tiket => "Waiting", :status_teknisi => "Waiting"})
                TicketHistory.create(
                    :ticket_id => self.id,
                    :no_tiket => self.no_tiket,
                    :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                    :inputer => self.nama_pemohon,
                    :status_tiket => self.status_tiket,
                    :keterangan => self.keterangan,
                    :issued_by => current_user.name
                )
            end
        else
            Ticket.update(self.id, :status_teknisi => "Waiting")
            TicketHistory.create(
                :ticket_id => self.id,
                :no_tiket => self.no_tiket,
                :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                :inputer => self.nama_pemohon,
                :status_tiket => self.status_tiket,
                :keterangan => self.keterangan,
                :issued_by => current_user.name
            )
        end
    end


    def autorespond
        TicketHistory.create(
          ticket_id: self.id,
          no_tiket: self.no_tiket,
          waktu: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
          inputer: self.nama_pemohon,
          status_tiket: "Auto Respond",
          issued_by: current_user.name,
          keterangan: "Terimakasih telah menghubungi Helpdesk PGAS Solution, permintaan Bapak/Ibu sudah kami terima dan akan diteruskan pada teknisi terkait penanganannya",
        )

        # notif email
        
        # notif email
        # current_host = request.host
        @open_url = "https://helpdesk.pgn-solution.co.id/tickets/#{self.id}"
        @subject = "Informasi Tiket #{self.no_tiket}"
        @email_body = "Terimakasih telah menghubungi Helpdesk PGAS Solution, permintaan Bapak/Ibu sudah kami terima dan akan diteruskan pada teknisi terkait penanganannya:" +
        "<br>" +
        "No Tiket: "+self.try(:no_tiket)+"<br>"+
        "Issued by: "+self.try(:nama_pemilik_kendala)+"<br>"+
        "Inputer: "+self.try(:nama_pemohon)+"<br>"+
        # "Catatan: "+"Silakan berikan penilaian  Tiket Sesuai Ketentuan"+"<br>"+         
        "<br>Salam,<br>"+
        "<b>HELP DESK<b><br><br><br>"+
        "<a href='"+@open_url+"' target='_blank'>Klik untuk membuka Detail Tiket</a>"
        # @email_to = RoleAssignment.joins(:user).where("role_id = ?", 4).pluck("users.email")
        # menggabungkan email dari dua query yang berbeda
        emails = RoleAssignment.joins(:user).where("lower(regexp_replace(users.username, '[^\\w]+', '', 'g')) = ?", self.try(:nama_pemilik_kendala).to_s.downcase.gsub(/[^\w]/, '')).pluck("users.email")
        #   User.where("LOWER(name) LIKE ? OR LOWER(name) LIKE ?", "%#{self.nama_pemohon.downcase}%", "%#{self.nama_pemilik_kendala.downcase}%")
        # .pluck(:email)

        # menghapus duplikat email yang dihasilkan
        if emails.present?
           # menghapus duplikat email yang dihasilkan
           @email_to = emails.uniq
           begin
             TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
           rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
             logger.warn mailing_lists_error
             flash.discard[:notice] = "There was a problem with sending to destination email. Please give information to the SIT " + "#{mailing_lists_error}"
             return redirect_to root_path
           end
         else
           logger.warn "No recipient email found for ticket #{self.no_tiket}"
         end
          
    end

     # Constants for reminder email settings
    REMINDER_INTERVAL = 24.hours
    REMINDER_MESSAGE = "Reminder: your ticket is still waiting for attention"
    REMINDER_SUBJECT = "Ticket update reminder"

    #notif email
    def auto_reminder
        # Check if last reminder was sent
        # return if self.notification_read_ticket.exists?(notification_type: "reminder")
      
        # Check if the technician has changed
        # if self.saved_change_to_teknisi?
        #   # Notifies the new technician via email
        #   @open_url = "http://192.168.22.55:3003/tickets/#{self.id}"
        #   @body = "Ticket #{self.no_tiket} has been assigned to you. Please take action." + "<br>" +
        #           "<br>" +
        #           "<br>Salam,<br>"+
        #           "<b>HELP DESK</b><br><br><br>"+
        #           "<a href='"+@open_url+"' target='_blank'>Klik untuk membuka Detail Tiket</a>"
        #   @subject = "New ticket assigned to you - #{self.no_tiket}"
      
        #   email_to = User.where("LOWER(name) LIKE ? ", "%#{self.teknisi.downcase}%").pluck(:email)
        #   begin
        #     TicketMailer.send_email(email_to, @subject, @body).deliver_now!
        #   rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
        #     logger.error "There was a problem with sending email. Error message: #{mailing_lists_error.message}"
        #     return
        #   end
        # end
      
        # # Check if the ticket has not been updated for more than 24 hours
        # if self.status_tiket && self.updated_at < REMINDER_INTERVAL.ago
        #   # Notifies the owner of the ticket via email
        #   @open_url = "http://192.168.22.55:3003/tickets/#{self.id}"
        #   @body = REMINDER_MESSAGE + ":" + "<br>" +
        #           "<br>" +
        #           "<br>Salam,<br>"+
        #           "<b>HELP DESK</b><br><br><br>"+
        #           "<a href='"+@open_url+"' target='_blank'>Klik untuk membuka Detail Tiket</a>"
        #   @subject = REMINDER_SUBJECT + " - " + self.no_tiket
      
        #   email_to = User.where("LOWER(name) LIKE ? ", "%#{self.teknisi.downcase}%").pluck(:email)
        #   begin
        #     TicketMailer.send_email(email_to, @subject, @body).deliver_now!
        #     # Create notification read for reminder
        #     self.notification_read_ticket.create!(notification_type: "reminder")
        #   rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
        #     logger.error "There was a problem with sending email. Error message: #{mailing_lists_error.message}"
        #     return
        #   end
        # end
      end
      
    private
    def update_and_autorespond
      updateStatus
      autorespond
    end

end
