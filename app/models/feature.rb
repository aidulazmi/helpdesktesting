class Feature < ActiveRecord::Base
    # has_many :feature_assignments
    has_many :roles, through: :feature_assignments
    has_many :feature_assignments, as: :feature
    
    has_many :model_features
    has_many :models, through: :model_features
end
