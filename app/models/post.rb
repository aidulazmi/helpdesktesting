class Post < ApplicationRecord
    def change
        create_table :posts do |t|
    
          t.string :title
          t.string :header
          t.text :desc
    
          t.timestamps
        end
    
    validates :title, presence: true
    validates :header, presence: true
    mount_uploader :header, HeaderUploader
    validates :desc, presence: true
end
