class Category < ActiveRecord::Base
  belongs_to :service_type
  has_many :message_templates, foreign_key: 'kategori', primary_key: 'name'
  has_one_attached  :lampiran_icon
end
