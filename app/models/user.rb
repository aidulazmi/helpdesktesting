class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :role_assignments, dependent: :destroy
  has_many :roles, through: :role_assignments
  has_many :areas, through: :role_assignments
  has_many :ticket_approval, dependent: :destroy
  belongs_to :division, optional: true
  belongs_to :position, optional: true
  devise :database_authenticatable, :recoverable, :validatable, :trackable,:timeoutable
  has_many :features
  has_many :tickets, foreign_key: :nama_pemilik_kendala, primary_key: :name

  def self.find_by_name_or_username(name)
    where("LOWER(name) = ? OR LOWER(username) = ?", name.strip.downcase, name.strip.downcase).first
  end

  def can?(&block)
    roles.map(&:permissions).any?(&block)
  end

  def computed_permissions
    roles.map(&:computed_permissions).reduce(RoleCore::ComputedPermissions.new, &:concat)
  end
end
