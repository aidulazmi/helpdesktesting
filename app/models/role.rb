class Role < RoleCore::Role
  has_many :role_assignments, dependent: :destroy
  has_many :users, through: :role_assignments
  has_many :feature_assignments, as: :feature
  # has_many :feature_assignments, as: :feature
  has_many :features, through: :feature_assignments
  end