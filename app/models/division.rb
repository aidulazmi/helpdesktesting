class Division < ActiveRecord::Base
    has_one :user
    def set_divisi
        parsed = JSON.parse(migrateDivisi.body)
        f = {}
        parsed['result']['data'].each do |e|
            @nama = e['desc']
            unless Division.exists?(nama: @nama)
                @checkDuplicate = Division.find_by_nama(@nama)
                unless @checkDuplicate.present?
                    Division.create!(nama: @nama)
                end
            end
        end
    end

    def set_direktorat
        parsed = JSON.parse(migrateDirektorat.body)
        f = {}
        parsed['result']['data'].each do |e|
            @nama = "Direktur " + e['desc']
            if e['sub_group'].match(/DIREKTORAT/)
                unless Division.exists?(nama: @nama)
                    @checkDuplicate = Division.find_by_nama(@nama)
                    unless @checkDuplicate.present?
                        Division.create!(nama: @nama)
                    end
                end
            end
        end
    end

    def set_department
        parsed = JSON.parse(migrateDirektorat.body)
        f = {}
        parsed['result']['data'].each do |e|
            @nama = e['desc']
            if e['sub_group'].match(/DEPARTEMEN/)
                unless Division.exists?(nama: @nama)
                    @checkDuplicate = Division.find_by_nama(@nama)
                    unless @checkDuplicate.present?
                        Division.create!(nama: @nama)
                    end
                end
            end
        end
    end

    private
    def migrateDivisi
        uri = URI.parse('http://192.168.60.136:3000/fast/master_params/division')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = false
        request = Net::HTTP::Get.new(uri.request_uri)
        request["Authorization"] ="Bearer 4854b5b486f4159566c80d842850b967"
        response = http.request(request)
        return response
    end

    def migrateDirektorat
        uri = URI.parse('http://192.168.60.136:3000/fast/master_params')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = false
        request = Net::HTTP::Get.new(uri.request_uri)
        request["Authorization"] ="Bearer 4854b5b486f4159566c80d842850b967"
        response = http.request(request)
        return response
    end
end
