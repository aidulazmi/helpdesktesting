class Area < ActiveRecord::Base
    has_many :role_assignments, dependent: :destroy
    has_many :users, through: :role_assignments
    
    def set_area
        parsed = JSON.parse(migrateArea.body)
        f = {}
        parsed['result']['data'].each do |e|
            @nama = e['desc']
            unless Area.exists?(nama: @nama)
                @checkDuplicate = Area.find_by_nama(@nama)
                unless @checkDuplicate.present?
                    Area.create!(nama: @nama)
                end
            end
        end
    end

    private
    def migrateArea
        uri = URI.parse('http://192.168.60.136:3000/fast/master_params/organisasi')
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = false
        request = Net::HTTP::Get.new(uri.request_uri)
        request["Authorization"] ="Bearer 4854b5b486f4159566c80d842850b967"
        response = http.request(request)
        return response
    end
end
