class Assetsdigital < ActiveRecord::Base
    def self.import(file)
        spreadsheet = Roo::Spreadsheet.open(file.path)
        header = spreadsheet.row(1)
        (2..spreadsheet.last_row).each do |i|
          row = Hash[[header, spreadsheet.row(i)].transpose]
          asset = find_by_id(row["id"]) || new
          asset.attributes = row.to_hash.slice(*column_names)
          asset.save!
        end
      end
end
