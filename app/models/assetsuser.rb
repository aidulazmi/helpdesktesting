class Assetsuser < ActiveRecord::Base
  has_one_attached  :lampiran
  # has_many :features, as: :featureable

  has_many :model_features, as: :model
  has_many :features, through: :model_features
  # has_many :tickets, foreign_key: 'nama_pemilik_kendala', primary_key: 'user'
  # has_many :manual_tickets, foreign_key: 'nama_pemilik_kendala', primary_key: 'manual_user', class_name: 'Ticket'
  

  

  def self.import(file)
    spreadsheet = Roo::Spreadsheet.open(file.path)
    data_count = 0
    
    # iterate through each worksheet in the file
    spreadsheet.each_with_pagename do |name, worksheet|
      # skip the worksheet if the name is "kontrak payung" or "list laptop"
      next if name.downcase == "kontrak payung" || name.downcase == "list laptop"
    
      header_row = worksheet.first_row.upto(worksheet.last_row).find { |i| worksheet.row(i).include?("Nama Komputer") || worksheet.row(i).include?("Computer Name") || worksheet.row(i).include?("NAMA KOMPUTER") }
      header = worksheet.row(header_row).map { |h| h.nil? ? nil : h.downcase }
    
      (header_row+1..worksheet.last_row).each do |i|
        row = Hash[[header, worksheet.row(i)].transpose]
        asset = Assetsuser.find_or_initialize_by(nama_komputer: row["nama komputer"] || row["computer name"] )
        nama_pengguna = row["nama pengguna"] || row["nama"] || row["nama user"]
        if nama_pengguna.present? || !nama_pengguna.blank?
          nama_pengguna = nama_pengguna.to_s.split.length > 1 ? nama_pengguna.to_s.split.values_at(0, -1).join(" ") : nama_pengguna.to_s
        end
        area_name = row["area"].present? ? row["area"].downcase : nil
        area = Area.find_by("lower(nama) LIKE ?", "%#{area_name}%")

        divisi_name = row["satuan kerja"].present? ? row["satuan kerja"].downcase : nil || row["nama divisi"].present? ? row["nama divisi"].downcase : nil || row["divisi"].present? ? row["divisi"].downcase : nil
        divisi = Division.find_by("lower(nama) LIKE ?", "%#{divisi_name}%")
        

    
        if !row.values.all?(&:nil?) && asset.update(
          lokasi: area ? area.nama : nil,
          keterangan: row["keterangan"] || area ? nil : (row['area'].present? ? "#{row['area']}*" : nil),
          serial_number: row["serial no"] || row["serial number"] || (row["serial"] && row["no"] ? row["serial"] + row["no"] : nil),
          nama_komputer: row["nama komputer"] || row["computer name"],
          manual_user: row["nama pengguna"] || row["nama"] || row["nama user"],         
          nip: row["nip"] || row["nipg"]  ,
          divisi:  divisi ? divisi.nama : nil,
          status_pekerja: row["status pekerja"],
          email: row["email"].to_s.include?("@") ? row["email"] : (nama_pengguna.present? ? nama_pengguna.gsub(" ",".").downcase.to_s + "@pgn-solution.co.id" : ""),
          no_hp: row["no handphone"],
          status_laptop: row["status laptop"],
          jenis_perangkat: row["komputer"],
          periode_laptop: row["periode laptop"] || name,
          spesifikasi: row["spesifikasi"],
          no_bast: row["no bast"],
          no_hbb: row["no hbb"] || row["nomor hbb"],
          penyedia: row["penyedia"],
          tahun: row["tahun"] || (name.match(/\d{4}/) ? name.match(/\d{4}/)[0] : "20#{name.scan(/\d{2}$/)[-1]}")

          # jabatan: row["jabatan"] 
          # kepala_divisi: row["kepala divisi"]
        )
          data_count += 1
        end
      end
    end
    
    
    
    
    return data_count
  rescue StandardError => e
    puts e.message
    raise "Import failed: #{e.message}"
  end
  
  
  
  
  
end
