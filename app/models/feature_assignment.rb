class FeatureAssignment < ActiveRecord::Base
  belongs_to :role
  belongs_to :feature
  belongs_to :featureable, polymorphic: true

  after_create :check_and_add_feature_id_column
  after_update :check_and_add_feature_id_column

  def check_and_add_feature_id_column
    permissions = self.role.permissions
    model_names = permissions.to_h.keys.map { |key| key.to_s.split("_").first.capitalize }

    model_names.each do |model_name|
      table_name = model_name.downcase.pluralize
      column_name = 'feature_id'

      unless ActiveRecord::Base.connection.table_exists?(table_name) && migration_has_column?(table_name, column_name)
        # Kolom feature_id belum ada, buat migrasi untuk menambahkannya
        migration_name = "add_feature_id_to_#{table_name}"
        migration_class_name = migration_name.camelize

        migration_content = <<~MIGRATION
          class #{migration_class_name} < ActiveRecord::Migration[6.0]
            def up
              add_column :#{table_name}, :#{column_name}, :integer
            end

            def down
              remove_column :#{table_name}, :#{column_name}, :integer
            end
          end
        MIGRATION

        migration_file_path = Rails.root.join('db', 'migrate', "#{Time.now.strftime('%Y%m%d%H%M%S')}_#{migration_name.underscore}.rb")

        begin
          File.write(migration_file_path, migration_content)

          puts "Created migration file: #{migration_file_path}"

          # Jalankan migrasi
          `rails db:migrate`
        rescue => e
          puts "Error occurred while creating migration file or running migration: #{e.message}"
        end
      else
        puts "Column #{column_name} already exists in table #{table_name}"
      end
    end
  end

  def migration_has_column?(table_name, column_name)
    connection = ActiveRecord::Base.connection

    return true if connection.column_exists?(table_name, column_name)

    migration_table_name = connection.schema_exists?(:schema_migrations) ? 'schema_migrations' : 'schema_migrations'

    migration_paths = ActiveRecord::Migrator.migrations_paths.to_a

    migrator = ActiveRecord::MigrationContext.new(migration_paths)
    pending_migrations = migrator.migrations_status

    migration_versions = pending_migrations.map { |status| status.version.to_i }

    migration_versions.each do |version|
      migration = migrator.migration(version)

      if migration.name.include?("add_column_to_#{table_name}")
        return true
      end
    end

    # Jika kolom belum ada, tambahkan kolom dengan migrasi baru
    migration_file_path = Rails.root.join('db', 'migrate')
    migration_file_name = "add_#{column_name}_to_#{table_name}.rb"
    migration_file = migration_file_path.join(migration_file_name)

    unless migration_file.exist?
      migration_version = migration_versions.last.to_i + 1
      migration_class_name = "Add#{column_name.camelize}To#{table_name.camelize}"
      migration_content = <<~MIGRATION
        class #{migration_class_name} < ActiveRecord::Migration[6.0]
          def change
            add_column :#{table_name}, :#{column_name}, :integer
          end
        end
      MIGRATION

      File.write(migration_file, migration_content)

      migrator.migrate(migration_version)

      if connection.column_exists?(table_name, column_name)
        puts "Kolom #{column_name} telah berhasil ditambahkan ke tabel #{table_name}."
        return true
      else
        puts "Gagal menambahkan kolom #{column_name} ke tabel #{table_name}."
        return false
      end
    end

    connection.column_exists?(table_name, column_name)
  end
end
