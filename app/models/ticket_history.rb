class TicketHistory < ActiveRecord::Base
  belongs_to :ticket
  # has_many :notification_read_ticket
  has_many :notification_read_tickets, dependent: :destroy
  include ActionView::Helpers::DateHelper
  
  def autoEskalasi
    @getStatus = Ticket.where('status_tiket = ?', "Processing Assign").select('*')
    @getStatus.each do |eskalasi|
      if Time.current - 4.hour >= eskalasi.created_at
        TicketHistory.create(
          :ticket_id => eskalasi.id,
          :no_tiket => eskalasi.no_tiket,
          :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
          :inputer => eskalasi.nama_pemohon,
          :status_tiket => "Problem"
          
        )
        @token = "5394039056:AAF_TFgMD_n-bj8-VLngitdMiKF2mh7j3zk"
            Telegram::Bot::Client.run(@token) do |bot|
                @otp_code = rand(1111..9999)
                # @setOtp = User.find_by(telegram_id: current_user.member.try(:telegram_id))
                # @setOtp.update(otp_code: @otp_code)
                
                # @current_date = DateTime.now.strftime("%Y-%m-%d %T")
                # @convert = @current_date.to_datetime
                # @duration = 45.seconds     
                # @current_expired = @convert + @duration

                # @setOtp.update(expired_in: @current_expired.strftime("%Y-%m-%d %T"))       
                
                # @getOtp = User.find_by_telegram_id(current_user.member.try(:telegram_id))
                @arrayID = [1011681088]
                @arrayID.each do |f|
                  bot.api.send_message(chat_id: f, text:"OII #{params[:ticket][:teknisi]}\n Ada tiket yang belum diproses sudah menjadi problem \n #{params[:ticket][:keterangan]}\n segera lakukan tanggung jawabmu")
                end
            end
        Ticket.update(eskalasi.id, {:status_tiket => "Problem"})
      end
    end
  end
  def autoClosed
    @getStatus = Ticket.where('status_tiket = ?', "Solved").select('*')
    @time_now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    @getStatus.each do |solved|
      if Time.current - 1.hour >= solved.created_at
        TicketHistory.create(
          :ticket_id => solved.id,
          :no_tiket => solved.no_tiket,
          :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
          :inputer => solved.nama_pemohon,
          :status_tiket => "Closed"
        )
        @resolution_time = distance_of_time_in_words(@time_now, solved.created_at, true, :only => [:weeks, :days, :hours, :minutes])
        #notif apps
        Ticket.update(solved.id, {:status_tiket => "Closed", :status_teknisi => "Available", :resolution_time => @resolution_time})
        
        # notif email
        @open_url ="https://helpdesk.pgn-solution.co.id/tickets/#{solved.id}"
        @subject = "Tiket #{solved.no_tiket} telah Closed"
        @email_body = "Tiket Anda telah Closed.Mohon berikan rating untuk layanan yang telah diberikan. Informasi tambahan sebagai berikut:" +
        "<br>" +
         "No Tiket: "+solved.try(:no_tiket)+"<br>"+
         "Issued by: "+solved.try(:nama_pemilik_kendala)+"<br>"+
         "Inputer: "+solved.try(:nama_pemohon)+"<br>"+
         "Resolution Time: "+solved.try(:resolution_time)+"<br>"+
        # "Catatan: "+"Silakan berikan penilaian  Tiket Sesuai Ketentuan"+"<br>"+   
        "<br>Terimakasih telah menggunakan layanan Helpdesk, permintaan Bapak/Ibu sudah terselesaikan, harap ketersediaannya untuk memberikan penilaian pada bagian satisfaction."+"<br>"+        
        
        "<br>Salam,<br>"+
        "<b>HELP DESK<b><br><br><br>"+
        "<a href='"+@open_url+"' target='_blank'>Klik untuk melihat Detail Tiket dan berikan penilaian satisfaction</a>"
        # @email_to = RoleAssignment.joins(:user).where("role_id = ?", 4).pluck("users.email")
        # menggabungkan email dari dua query yang berbeda
        emails = RoleAssignment.joins(:user).where("lower(regexp_replace(users.username, '[^\\w]+', '', 'g')) = ?", solved.try(:nama_pemilik_kendala).to_s.downcase.gsub(/[^\w]/, '')).pluck("users.email") 

        # menghapus duplikat email yang dihasilkan
        @email_to = emails.uniq
        begin
          TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
        rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
          logger.warn mailing_lists_error
          flash.discard[:notice] = "There was a problem with sending to destination email.  Please give information to the SIT " + "#{mailing_lists_error}"
          return redirect_to root_path
        end
      end
    end

  end
  def autoSatisfaction
    @getStatus = Ticket.where('status_tiket = ?', "Closed").where(satisfaction: nil).select('*')
    # @time_now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    # @resolution_time = distance_of_time_in_words(@time_now, @ticket.update_at, true, :only => [:weeks, :days, :hours, :minutes])
    @getStatus.each do |solved|
      if Time.current - 24.hour >= solved.updated_at
        Ticket.update(solved.id, {:satisfaction => "5", :satisfaction_note => "Auto Satisfaction"})
      end
    end
  end
  
  def atuoRespond

  end

  # def reminderchatbyemail
  # end
end
