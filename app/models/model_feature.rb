class ModelFeature < ApplicationRecord
  # belongs_to :model, polymorphic: true
  # belongs_to :feature
  belongs_to :feature
  belongs_to :model, polymorphic: true
end
