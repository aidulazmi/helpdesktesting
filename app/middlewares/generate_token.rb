class GenerateToken
    def initialize(app)
      @app = app
    end
  
    def call(env)
      env['token'] = SecureRandom.hex
      @app.call(env)
    end
  end