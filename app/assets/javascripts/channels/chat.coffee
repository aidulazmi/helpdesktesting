App.chat = App.cable.subscriptions.create {
  channel: 'ChatChannel',
  ticket_id: $('meta[name="ticket-id"]').attr('content')
},
  connected: ->
  disconnected: ->
  received: (data) ->
    $('#chat-list').append("<p>" + data.message + "</p>")
  send_message: (message) ->
    @perform('send_message', { message: message })
