App.chat_channel = App.cable.subscriptions.create({
    channel: 'ChatChannel',
    ticket_id: $("#ticket-id").data("ticket-id")
  }, {
    connected: function() {},
    disconnected: function() {},
    received: function(data) {
      $("#chat-messages").append("<p>" + data.user + ": " + data.message + "</p>");
    },
    send_message: function(message) {
      this.perform('send_message', { message: message });
    }
  });
  
  $(document).on('submit', '#new-message', function(e) {
    e.preventDefault();
    var message = $("#message-content").val();
    $("#message-content").val('');
    App.chat_channel.send_message(message);
  });
  