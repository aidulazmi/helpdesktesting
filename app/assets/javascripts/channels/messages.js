import consumer from "./consumer"

consumer.subscriptions.create("MessagesChannel", {
  connected() {
    console.log("Connected to messages channel.")
  },

  disconnected() {
    console.log("Disconnected from messages channel.")
  },

  received(data) {
    const messagesContainer = document.querySelector(`[data-channel="${data.room}"]`)
    const messageContent = `
      <div class="message ${data.username == current_user.name ? 'sent' : 'received'}">
        <div class="message-avatar">
          <i class="fa fa-user" title="${data.username}"></i>
        </div>
        <div class="message-body">
          <div class="message-content">${data.message}</div>
          <div class="message-time">${new Date().toLocaleTimeString()}</div>
        </div>
      </div>
    `
    messagesContainer.insertAdjacentHTML('beforeend', messageContent)
  }
})
