// app/assets/javascripts/channels/messages.coffee

App.messages = App.cable.subscriptions.create("ChatChannel", {
  connected: function() {
    console.log("Connected to chat channel.");
  },
  
  received: function(data) {
    console.log(data.sender + ": " + data.message);
    // add code here to display the received message
  },
  
  send: function(message, no_tiket) {
    return this.perform('send_message', {
      message: message,
      no_tiket: no_tiket
    });
  }
});
