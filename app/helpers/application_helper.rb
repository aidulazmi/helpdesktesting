module ApplicationHelper


    def is_active_controller(controller_name)
        params[:controller] == controller_name ? "active" : nil
    end

    def is_active_action(action_name)
        params[:action] == action_name ? "active" : nil
    end

    def satisfactionForSelfService
        @getData = Ticket.where(nama_pemohon: current_user.name, status_tiket: "Closed", satisfaction: nil).select('*')
        return @getData
    end

    def satisfactionForSelfServiceBYPemilikKendala
        @getData = Ticket.includes(:ticket_problem).where(nama_pemilik_kendala: current_user.name, status_tiket: "Closed", satisfaction: nil)
        return @getData
    end
    def satisfactionForSelfServiceBYPemilikKendalaEskalasi
        @getData = Ticket.includes(:ticket_problem).where(nama_pemilik_kendala: current_user.name).where(ticket_problems: { status: "Closed", satisfaction: nil })
        return @getData
      end
      
      

    def roleActive
        return unless session[:active_role]
        @active_role ||= Role.find_by(id: session[:active_role])
    end
    

    def sessionRole
        @roleAssign = RoleAssignment.left_outer_joins(:role).where(user_id: current_user.id).select('roles.name AS nameroles, role_assignments.role_id')
        return @roleAssign
    end

    def featureActive
        return unless session[:active_role]
        @feature ||= FeatureAssignment.find_by(role_id: session[:active_role])
    end

    # def sessionFeature
    #     seasonFeature = featureActive&.role&.permissions&.[](:ticket)&.[](:manage)
    #     return seasonFeature
    #   end
    def sessionFeature
        if featureActive
          @sessionFeature ||= FeatureAssignment.find_by(role_id: session[:active_role])
        else
          @sessionFeature = nil
        end
    end
      
      

    def sessionArea
        @roleAssign = RoleAssignment.left_outer_joins(:user,:area).where(user_id: current_user.id).select('areas.nama AS nameareas')
        @value = @roleAssign.each_with_index.map { |area| "#{area.try(:nameareas)}" }.join(", ").gsub(",","")
        return @value
    end

    def sessionDivision
        @value = current_user.division.try(:nama)
        return @value
    end
    def sessionPriority
        @value = current_user.division.try(:priorty)
        return @value
    end

    def sessionName
        @value = current_user.try(:name)
        return @value
    end
    
    def show_active_session_feature
        active_feature_id = session[:feature_id]
        # Lakukan sesuatu dengan nilai session feature yang aktif
    end
    def show_active_session_feature_delete
        active_feature_id = session[:feature_id]
        # Lakukan sesuatu dengan nilai session feature yang aktif
      
        # Menghapus session feature yang aktif
        session.delete(:feature_id)
    end
      

    public def superadmingetTeknisiParams(parameter)
        if parameter.present?

            @getUserTeknisi = RoleAssignment.where(role_id: 3).select('user_id')
            h = []
            @getUserTeknisi.each do |getUserTeknisi|
                h[getUserTeknisi.user_id.to_i] = getUserTeknisi.user_id
            end

            @getTeknisi = RoleAssignment.left_outer_joins(:role,:user).select('users.name').where('role_assignments.user_id IN (?)', h).order('users.name asc')
            option = '<option value=""></value>'
            f = {}
            
            @getTeknisi.each do |getTeknisi|
            f["#{getTeknisi.name}"] = getTeknisi.name
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if parameter === v
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    public def getTeknisiParams(parameter)
        if parameter.present?
            @getArea = RoleAssignment.left_outer_joins(:area).where(user_id: current_user.id).select('area_id')
            i = []
            @getArea.each do |getArea|
                i[getArea.area_id.to_i] = getArea.area_id
            end

            @getUserTeknisi = RoleAssignment.where(role_id: 3).select('user_id')
            h = []
            @getUserTeknisi.each do |getUserTeknisi|
                h[getUserTeknisi.user_id.to_i] = getUserTeknisi.user_id
            end

            @getTeknisi = RoleAssignment.left_outer_joins(:role,:user).select('users.name').where('role_assignments.user_id IN (?) and role_assignments.area_id IN (?)', h, i).order('users.name asc')
            option = '<option value=""></value>'
            f = {}
            
            @getTeknisi.each do |getTeknisi|
            f["#{getTeknisi.name}"] = getTeknisi.name
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if parameter === v
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getPic
        
        @getArea = RoleAssignment.left_outer_joins(:area).select('area_id')
        i = []
        @getArea.each do |getArea|
            i[getArea.area_id.to_i] = getArea.area_id
        end
        @getUserTeknisi = RoleAssignment.where(role_id: 3).select('user_id')
        h = []
        @getUserTeknisi.each do |getUserTeknisi|
            h[getUserTeknisi.user_id.to_i] = getUserTeknisi.user_id
        end

        @getPic = RoleAssignment.left_outer_joins(:role,:user,:area).select('users.name, areas.nama').where(' role_assignments.user_id IN (?) and role_assignments.area_id IN (?)', h,i).order('users.name asc')
        option = '<option value=""></value>'
        f = {}
        
        @getPic.each do |getPic|
        f["#{getPic.name}"] = getPic.name + " - " + getPic.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    def getTeknisi
        @getArea = RoleAssignment.left_outer_joins(:area).where('user_id = ? or area_id = ?', current_user.id, 36).select('area_id')
        i = []
        @getArea.each do |getArea|
            i[getArea.area_id.to_i] = getArea.area_id
        end
        
        @getUserTeknisi = RoleAssignment.where(role_id: 3).select('user_id')
        h = []
        @getUserTeknisi.each do |getUserTeknisi|
            h[getUserTeknisi.user_id.to_i] = getUserTeknisi.user_id
        end

        @getTeknisi = RoleAssignment.left_outer_joins(:role,:user,:area).select('users.name, areas.nama').where('role_assignments.user_id IN (?) and role_assignments.area_id IN (?)', h, i).order('users.name asc')
        option = '<option value=""></value>'
        f = {}
        
        @getTeknisi.each do |getTeknisi|
        f["#{getTeknisi.name}"] = getTeknisi.name + " - " + getTeknisi.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    public def getTemplateStatusParams(parameter)
        if parameter.present?
            @status_ticket = StatusTicket.where('nama LIKE ?', "Processing%").order(:id => :asc)
            option = '<option value=""></value>'
            f = {}
            
            @status_ticket.each do |status_ticket|
            f["#{status_ticket.nama}"] = status_ticket.nama
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if v.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getTemplateStatus
        @status_ticket = StatusTicket.where('nama LIKE ?', "Processing%").order(:id => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @status_ticket.each do |status_ticket|
        f["#{status_ticket.nama}"] = status_ticket.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    def getNamaSystem
        @nama_system = CollectionAsset.where('jenis_layanan LIKE ?', "Layanan System%").order(:id => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @nama_system.each do |nama_system|
        f["#{nama_system.device_name}"] = nama_system.device_name
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    def getPerangkat
        @asset_user = Assetsuser.order(:id => :asc)
        option = '<option value=""></option>'
        f = {}
      
        @asset_user.each do |asset_user|
          value = asset_user.user.present? ? "#{asset_user.user} | #{asset_user.nama_komputer}" : asset_user.manual_user
          f[value] = "#{asset_user.user} | #{asset_user.nama_komputer}"
        end
      
        f = f.sort_by { |key| key }
      
        f.each do |k, v|
          option += '<option value="' + k + '">' + v + '</option>'
        end
      
        return option
      end
      

    def getPerangkatAssetParams(parameter)
      if parameter.present?
        # parameter ="irvan.hilmi"
        parameter = parameter.downcase.gsub(' ', '.').strip
        # Assetsuser.where("LOWER(user) = ?", user_name).inspect
        # @asset_user = Assetsuser.where(user: parameter).order(id: :asc)
        @asset_user = Assetsuser.where("LOWER(user) = :param OR LOWER(manual_user) = :param", param: parameter).order(id: :asc)

        # Assetsuser.where(user: parameter.strip).or(Assetsuser.where(manual_user: parameter.strip)).order(id: :asc)
        option = '<option value=""></option>'
        f = {}
    
        @asset_user.each do |asset_user|
            f["#{asset_user.nama_komputer}"] = asset_user.nama_komputer
        end
    
        f = f.sort_by { |key| key }
    
        f.each do |k, v|
          option += '<option value="' + k + '">' + v + '</option>'
        end
    
        return option
      end
    end

    public def getPerangkatChangeParams(parameter)
    if parameter.present?
        @change_asset = Change.order(:id => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @change_asset.each do |change_asset|
        f["#{change_asset.nama_perangkat}"] = change_asset.nama_perangkat
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
            if v.match(/#{parameter}/)
                selected = "selected"
            end
            option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
        end
        
        return option           
    end
end
      
      


    public def getJabatanParams(parameter)
        if parameter.present?
            @jabatan = Position.order(:nama => :asc)
            option = '<option value=""></option>' # Ubah tag penutup menjadi "</option>"
            f = {}
            # parameter = "Staff" # Contoh nilai parameter
        
            @jabatan.each do |jabatan|
            f[jabatan.nama] = jabatan.nama # Tidak perlu menggunakan string interpolation
            end 
        
            f.sort_by { |key, _| key } # Gunakan underscore (_) untuk mengabaikan nilai yang tidak digunakan
            f.each do |k,v|
            selected = v.match(/#{Regexp.escape(parameter)}/i) ? "selected" : "" # Gunakan Regexp.escape untuk menghindari karakter khusus
            option += '<option value="'+k+'" '+selected+'>'+v+'</option>' # Tambahkan pada variabel option menggunakan +=
            end
        
            return option           
        end
      
    end

    def getJabatan
        @jabatan = Position.order(:nama => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @jabatan.each do |jabatan|
        f["#{jabatan.nama}"] = jabatan.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end


    public def getCollectionAssetParams(parameter)
        if parameter.present?
            @collection_asset = CollectionAsset.order(:device_name => :asc)
            option = '<option value=""></value>'
            f = {}
            
            @collection_asset.each do |collection_asset|
            f["#{collection_asset.device_name}"] = collection_asset.device_name
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if v.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getCollectionAsset
        @collection_asset = CollectionAsset.order(:device_name => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @collection_asset.each do |collection_asset|
        f["#{collection_asset.device_name}"] = collection_asset.device_name
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    public def getKodePerangkatParams(parameter)
        if parameter.present?
            @kode_perangkat = CollectionAsset.order(:device_name => :asc)
            option = '<option value=""></value>'
            f = {}
            
            @kode_perangkat.each do |kode_perangkat|
            f["#{kode_perangkat.id}"] = kode_perangkat.device_name
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if v.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getKodePerangkat
        @kode_perangkat = CollectionAsset.order(:device_name => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @kode_perangkat.each do |kode_perangkat|
        f["#{kode_perangkat.id}"] = kode_perangkat.device_name
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    def getDivisiAset
        @divisi_aset = Division.order(:nama => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @divisi_aset.each do |divisi_aset|
        f["#{divisi_aset.id}"] = divisi_aset.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end
    
    def getSubKategori
        @kategori = Category.order(name: :asc)
        option = '<option value=""></option>'
        f = {}
      
        @kategori.each do |kategori|
          sub_categories = kategori.sub_category.gsub(/\[|\]|"/, '').split(/,\s*/)
          sub_categories.each do |sub_category|
            f[sub_category] = sub_category
          end
        end
      
        f.sort_by { |key| key }
        f.each do |k, v|
          selected = ''
          option += '<option value="' + k + '" ' + selected + '>' + v + '</option>'
        end
      
        return option
      end
      
      
 
    def getSubKategoriParams(parameter)
        if parameter.present?
          @kategori = Category.where("sub_category LIKE ?", "%#{parameter}%").order(name: :asc)
          option = '<option value=""></option>'
          f = {}
      
          @kategori.each do |kategori|
            sub_categories = kategori.sub_category.gsub(/\[|\]|"/, '').split(/,\s*/)
            sub_categories.each do |sub_category|
              f[sub_category] = sub_category
            end
          end
      
          f.sort_by { |key| key }
          f.each do |k, v|
            selected = k.strip == parameter ? 'selected="selected"' : ''
            option += '<option value="' + k.strip + '" ' + selected + '>' + k.strip + '</option>'
          end
      
          return option.html_safe
        end
      end
      
      
    

    public def getKategoriParams(parameter)
        if parameter.present?
            @kategori = Category.order(:name => :asc)
            option = '<option value=""></value>'
            f = {}
            
            @kategori.each do |kategori|
            f["#{kategori.id}"] = kategori.name
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if k.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+v+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    public def getKategoriParamsbyName(parameter)
    if parameter.present?
        @kategori = Category.order(:name => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @kategori.each do |kategori|
        f["#{kategori.name}"] = kategori.name
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
            if k.match(/#{parameter}/)
                selected = "selected"
            end
            option = option + '<option value="'+v+'" '+selected.to_s+'>'+v+'</option>'
        end
        
        return option           
    end
end

    def getKategori
        @kategori = Category.order(:name => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @kategori.each do |kategori|
        f["#{kategori.name}"] = kategori.name 
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end
    

    def getStatus
        @status = Ticket.order(:status_tiket => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @status.each do |status|
        f["#{status.status_tiket}"] = status.status_tiket 
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    public def getAreaParams(parameter)
        if parameter.present?
            @area = Area.all
            option = '<option value=""></value>'
            f = {}
            
            @area.each do |area|
            f["#{area.nama}"] = area.nama
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if v.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getArea
        @area = Area.all
        option = '<option value=""></value>'
        f = {}
        
        @area.each do |area|
        f["#{area.nama}"] = area.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    public def getDivisiParams(parameter)
        if parameter.present?
            @division = Division.order(:nama => :asc)
            option = '<option value=""></value>'
            f = {}
            
            @division.each do |divisi|
            f["#{divisi.nama}"] = divisi.nama
            end 
            f.sort_by { |key| key }
            f.each do |k,v|
                if v.match(/#{parameter}/)
                    selected = "selected"
                end
                option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
            
            return option           
        end
    end

    def getDivisi
        @division = Division.order(:nama => :asc)
        option = '<option value=""></value>'
        f = {}
        
        @division.each do |divisi|
        f["#{divisi.nama}"] = divisi.nama
        end 
        f.sort_by { |key| key }
        f.each do |k,v|
        option = option + '<option value="'+k+'">'+v+'</option>'
        end
        
        return option
    end

    public def getLDAPParams(parameter)
        if parameter.present?
            begin
                ldap = Net::LDAP.new :host => '192.168.60.159',
                                    :port => 389,
                                    :auth => {
                                        :method => :simple,
                                        :username => "cn=manager, dc=pgn-solution, dc=co, dc=id",
                                        :password => "4lh4mdul1ll4h"
                                    }

                filter = Net::LDAP::Filter.eq("cn", "*")
                treebase = "dc=pgn-solution, dc=co, dc=id"
                option = '<option value=""></value>'
                f = {}
                ldap.search(:base => treebase, :filter => filter) do |entry|
                    f["#{entry["uid"].map(&:inspect).join(', ').gsub('"', '')}"] = entry["uid"].map(&:inspect).join(', ').gsub('"', '')
                end
                f.sort_by { |key| key }
                f.map do |k,v|
                    if v.match(/#{parameter}/)
                        selected = "selected"
                    end
                    option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
                end
                
                return option
            rescue Errno::ECONNRESET => e
                return false
            end
        end
    end

    public def getLdap(parameter)
        if parameter.present?
        begin
            ldap = Net::LDAP.new :host => '192.168.60.159',
                                :port => 389,
                                :auth => {
                                    :method => :simple,
                                    :username => "cn=manager, dc=pgn-solution, dc=co, dc=id",
                                    :password => "4lh4mdul1ll4h"
                                }
    
            filter = Net::LDAP::Filter.eq("cn", "*")
            treebase = "dc=pgn-solution, dc=co, dc=id"
            option = '<option value=""></value>'
            f = {}
            ldap.search(:base => treebase, :filter => filter) do |entry|
            f["#{entry["uid"].map(&:inspect).join(', ').gsub('"', '')}"] = entry["uid"].map(&:inspect).join(', ').gsub('"', '')
            end
            f.sort_by { |key| key }
            f.each do |k, v|
            if v.downcase.gsub(".", " ").split.include?(parameter.downcase.strip.split[0]) && v.downcase.gsub(".", " ").split.include?(parameter.downcase.split[1])
                selected = "selected"
            else
                selected = ""
            end
            option = option + '<option value="'+k+'" '+selected.to_s+'>'+v+'</option>'
            end
    
            return option
        rescue Errno::ECONNRESET => e
            return false
        end
        end
    end
    
    
  


    def getLDAP
        begin
            ldap = Net::LDAP.new :host => '192.168.60.159',
                                :port => 389,
                                :auth => {
                                    :method => :simple,
                                    :username => "cn=manager, dc=pgn-solution, dc=co, dc=id",
                                    :password => "4lh4mdul1ll4h"
                                }

            filter = Net::LDAP::Filter.eq("cn", "*")
            treebase = "dc=pgn-solution, dc=co, dc=id"
            option = '<option value=""></value>'
            f = {}
            ldap.search(:base => treebase, :filter => filter) do |entry|
                f["#{entry["uid"].map(&:inspect).join(', ').gsub('"', '')}"] = entry["uid"].map(&:inspect).join(', ').gsub('"', '')
            end
            f.sort_by { |key| key }
            f.map do |k,v|
                option = option + '<option value="'+k+'">'+v+'</option>'
            end
            
            return option
        rescue Errno::ECONNRESET => e
            return false
        end
    end
end
