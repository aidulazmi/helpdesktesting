module TicketsHelper
    def updateStatus(id, teknisi)
        @checkTechnician = Ticket.find_by(teknisi: teknisi)
        if !@checkTechnician.status_tiket.match(/Closed/)
            Ticket.update(id, {:status_tiket => "Waiting", :status_teknisi => "Waiting"})
            TicketHistory.create(
                :status_tiket => "Waiting"
            )
        end
    end
    
end
