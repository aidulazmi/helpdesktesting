class ApplicationMailer < ActionMailer::Base
    default from: 'servicedesk@pgn-solution.co.id'
    layout 'mailer'
  end
  