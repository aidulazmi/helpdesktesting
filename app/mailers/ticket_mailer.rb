class TicketMailer < ApplicationMailer
    default from: 'servicedesk@pgn-solution.co.id'
  
    def send_email(_to,_subject,_body)
        # @user = params[:user]
        @email_to = _to
        @subject = _subject
        @body = _body
        # @url  = 'http://example.com/login'
        mail(to:@email_to, subject:@subject, body:@body, content_type:"text/html")
    end
    
  end
  