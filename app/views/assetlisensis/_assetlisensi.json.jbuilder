json.extract! assetlisensi, :id, :type_lisensi, :nama_lisensi, :jumlah_lisensi, :divisi, :penerima, :lokasi, :tahun_pembelian, :waktu_instalasi, :waktu_expired, :nama_perangkat, :serial_number, :pic, :upload, :keterangan, :created_at, :updated_at
json.url assetlisensi_url(assetlisensi, format: :json)
