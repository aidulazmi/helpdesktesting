json.set! :data do
  json.array! @assetlisensis do |assetlisensi|
    json.partial! 'assetlisensis/assetlisensi', assetlisensi: assetlisensi
    json.url  "
              #{link_to 'Show', assetlisensi }
              #{link_to 'Edit', edit_assetlisensi_path(assetlisensi)}
              #{link_to 'Destroy', assetlisensi, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end