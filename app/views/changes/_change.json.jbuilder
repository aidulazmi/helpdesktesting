json.extract! change, :id, :tanggal, :judul, :teknisi, :kategori_asset, :nama_perangkat, :ram, :nama_aplikasi, :pemilik_proses, :deskripsi, :created_at, :updated_at
json.url change_url(change, format: :json)
