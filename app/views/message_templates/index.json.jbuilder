json.set! :data do
  json.array! @message_templates do |message_template|
    json.partial! 'message_templates/message_template', message_template: message_template
    json.url  "
              #{link_to 'Show', message_template }
              #{link_to 'Edit', edit_message_template_path(message_template)}
              #{link_to 'Destroy', message_template, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end