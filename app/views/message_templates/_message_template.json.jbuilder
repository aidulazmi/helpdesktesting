json.extract! message_template, :id, :template, :sub_category, :jenis_layanan, :created_at, :updated_at
json.url message_template_url(message_template, format: :json)
