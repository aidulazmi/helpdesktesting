json.set! :data do
  json.array! @loss_events do |loss_event|
    json.partial! 'loss_events/loss_event', loss_event: loss_event
    json.url  "
              #{link_to 'Show', loss_event }
              #{link_to 'Edit', edit_loss_event_path(loss_event)}
              #{link_to 'Destroy', loss_event, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end