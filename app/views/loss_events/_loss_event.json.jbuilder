json.extract! loss_event, :id, :nama, :tanggal, :tempat, :status, :pic, :deskripsi_kejadian, :kategori, :created_at, :updated_at
json.url loss_event_url(loss_event, format: :json)
