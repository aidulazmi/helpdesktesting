json.extract! asset, :id, :device_name, :created_at, :updated_at
json.url asset_url(asset, format: :json)
