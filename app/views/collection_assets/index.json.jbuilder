json.set! :data do
  json.array! @assets do |asset|
    json.partial! 'assets/asset', asset: asset
    json.url  "
              #{link_to 'Show', asset }
              #{link_to 'Edit', edit_asset_path(asset)}
              #{link_to 'Destroy', asset, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end