json.set! :data do
  json.array! @feature_assignments do |feature_assignment|
    json.partial! 'feature_assignments/feature_assignment', feature_assignment: feature_assignment
    json.url  "
              #{link_to 'Show', feature_assignment }
              #{link_to 'Edit', edit_feature_assignment_path(feature_assignment)}
              #{link_to 'Destroy', feature_assignment, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end