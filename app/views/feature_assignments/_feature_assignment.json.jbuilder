json.extract! feature_assignment, :id, :role_id, :feature_id, :created_at, :updated_at
json.url feature_assignment_url(feature_assignment, format: :json)
