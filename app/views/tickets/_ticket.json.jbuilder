json.extract! ticket, :id, :no_tiket, :nama_pemohon, :divisi_pemohon, :area_pemohon, :nama_pemilik_kendala, :divisi_pemilik_kendala, :area_pemilik_kendala, :kategori, :sub_kategori, :jenis_layanan, :asset_id, :keterangan, :teknisi, :status_teknisi, :status_tiket, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
