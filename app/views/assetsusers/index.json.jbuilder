json.set! :data do
  json.array! @assetsusers do |assetsuser|
    json.partial! 'assetsusers/assetsuser', assetsuser: assetsuser
    json.url  "
              #{link_to 'Show', assetsuser }
              #{link_to 'Edit', edit_assetsuser_path(assetsuser)}
              #{link_to 'Destroy', assetsuser, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end