json.extract! assetsuser, :id, :user, :divisi, :nip, :lokasi, :nama_perangkat, :tahun, :jenis_kontrak, :wo_ke, :nama_komputer, :merk, :tipe, :no_inventaris, :penyedia, :keterangan, :created_at, :updated_at
json.url assetsuser_url(assetsuser, format: :json)
