json.set! :data do
  json.array! @assetsdigitals do |assetsdigital|
    json.partial! 'assetsdigitals/assetsdigital', assetsdigital: assetsdigital
    json.url  "
              #{link_to 'Show', assetsdigital }
              #{link_to 'Edit', edit_assetsdigital_path(assetsdigital)}
              #{link_to 'Destroy', assetsdigital, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end