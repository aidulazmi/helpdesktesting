json.set! :data do
  json.array! @features do |feature|
    json.partial! 'features/feature', feature: feature
    json.url  "
              #{link_to 'Show', feature }
              #{link_to 'Edit', edit_feature_path(feature)}
              #{link_to 'Destroy', feature, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end