json.set! :data do
  json.array! @ticket_approvals do |ticket_approval|
    json.partial! 'ticket_approvals/ticket_approval', ticket_approval: ticket_approval
    json.url  "
              #{link_to 'Show', ticket_approval }
              #{link_to 'Edit', edit_ticket_approval_path(ticket_approval)}
              #{link_to 'Destroy', ticket_approval, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end