json.extract! ticket_approval, :id, :ticket_id, :user_id, :status, :catatan, :created_at, :updated_at
json.url ticket_approval_url(ticket_approval, format: :json)
