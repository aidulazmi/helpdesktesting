json.set! :data do
  json.array! @assetssystems do |assetssystem|
    json.partial! 'assetssystems/assetssystem', assetssystem: assetssystem
    json.url  "
              #{link_to 'Show', assetssystem }
              #{link_to 'Edit', edit_assetssystem_path(assetssystem)}
              #{link_to 'Destroy', assetssystem, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end