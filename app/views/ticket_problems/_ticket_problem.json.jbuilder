json.extract! ticket_problem, :id, :ticket_id, :no_ticket_problem, :status, :teknisi, :priority, :kategori, :sub_kategori, :keterangan, :created_at, :updated_at
json.url ticket_problem_url(ticket_problem, format: :json)
