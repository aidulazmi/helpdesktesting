json.set! :data do
  json.array! @ticket_problems do |ticket_problem|
    json.partial! 'ticket_problems/ticket_problem', ticket_problem: ticket_problem
    json.url  "
              #{link_to 'Show', ticket_problem }
              #{link_to 'Edit', edit_ticket_problem_path(ticket_problem)}
              #{link_to 'Destroy', ticket_problem, method: :delete, data: { confirm: 'Are you sure?' }}
              "
  end
end