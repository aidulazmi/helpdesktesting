json.extract! category, :id, :name, :sub_category, :service_type_id, :created_at, :updated_at
json.url category_url(category, format: :json)
