class ChatChannel < ApplicationCable::Channel
  def subscribed
    # Bergabung dengan channel chat untuk tiket tertentu
    stream_from "chat_channel_#{params[:ticket_id]}"
  end
  
  def unsubscribed
    # Keluar dari channel chat untuk tiket tertentu
  end
  
  def send_message(data)
    # Menyimpan pesan baru dan mengirimkannya ke semua pengguna yang terhubung ke channel
    current_user.messages.create(ticket_id: params[:ticket_id], content: data['message'])
    ActionCable.server.broadcast("chat_channel_#{params[:ticket_id]}", {
      message: data['message'],
      user: current_user.name
    })
  end
end
