# app/channels/chat_room_channel.rb

class ChatRoomChannel < ApplicationCable::Channel
  def subscribed
    stream_from "chat_room_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_message(data)
    message = TicketHistory.create(
      ticket_id: data['ticket_id'],
      no_tiket: data['no_tiket'],
      waktu: Time.current.strftime("%Y-%m-%d %H:%M:%S"),
      inputer: data['inputer'],
      status_tiket: "Answer",
      issued_by: current_user.name,
      keterangan: data['message']
    )
    ActionCable.server.broadcast 'chat_room_channel', message: render_message(message)
  end

  private

  def render_message(message)
    ApplicationController.render(partial: 'tickets/chat_message', locals: { ticket_history: message })
  end
end
