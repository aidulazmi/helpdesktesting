class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "notifications_#{current_user.id}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_notification(data)
    ActionCable.server.broadcast("notifications_#{data['user_id']}", message: data['message'])
  end
end