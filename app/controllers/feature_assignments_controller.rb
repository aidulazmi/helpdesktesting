class FeatureAssignmentsController < ApplicationController
  before_action :set_feature_assignment, only: %i[ show edit update destroy ]

  # GET /feature_assignments or /feature_assignments.json
  def index
    @feature_assignments = FeatureAssignment.all
  end

  # GET /feature_assignments/1 or /feature_assignments/1.json
  def show
  end

  # GET /feature_assignments/new
  def new
    @feature_assignment = FeatureAssignment.new
  end

  # GET /feature_assignments/1/edit
  def edit
  end

  # POST /feature_assignments or /feature_assignments.json
  def create
    @feature_assignment = FeatureAssignment.new(feature_assignment_params)

    respond_to do |format|
      if @feature_assignment.save
        format.html { redirect_to feature_assignment_url(@feature_assignment), notice: "Feature assignment was successfully created." }
        format.json { render :show, status: :created, location: @feature_assignment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @feature_assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /feature_assignments/1 or /feature_assignments/1.json
  def update
    respond_to do |format|
      if @feature_assignment.update(feature_assignment_params)
        format.html { redirect_to feature_assignment_url(@feature_assignment), notice: "Feature assignment was successfully updated." }
        format.json { render :show, status: :ok, location: @feature_assignment }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @feature_assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /feature_assignments/1 or /feature_assignments/1.json
  def destroy
    @feature_assignment.destroy

    respond_to do |format|
      format.html { redirect_to feature_assignments_url, notice: "Feature assignment was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_feature_assignment
      @feature_assignment = FeatureAssignment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def feature_assignment_params
      params.require(:feature_assignment).permit(:role_id, :feature_id)
    end
end
