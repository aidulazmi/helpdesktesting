class TicketsController < ApplicationController
  before_action :set_ticket, only: %i[show edit update destroy]
  load_and_authorize_resource
  respond_to :html, :json
  include TicketsHelper

  def answer
    @ticket = Ticket.find(params[:id])
    @histories = TicketHistory.where(:ticket_id => @ticket.id)
    @histories = @histories.sort_by { |h| h[:id] }
    @histories = @histories.reverse
    @history = TicketHistory.new
  end

  def cetak_pdf
    # kategori = params[:kategori]
    # tipe = params[:tipe]
    # teknisi = params[:teknisi]
    # status = params[:status]
    # if kategori.present? || tipe.present? || teknisi.present? || status.present?
      
    #   @tickets = Ticket.all
    #   @tickets = @tickets.where(kategori: kategori) if kategori.present?
    #   @tickets = @tickets.where(jenis_layanan: tipe) if tipe.present?
    #   @tickets = @tickets.where(teknisi: teknisi) if teknisi.present?
    #   @tickets = @tickets.where(status_tiket: status) if status.present?
    #   @tickets = @tickets.order("tickets.created_at asc")
      
    # else
    #    @tickets = Ticket.all.order("tickets.created_at asc")
      
    # end

    kategori = params[:kategori]
    tipe = params[:tipe]
    teknisi = params[:teknisi]
    status = params[:status]
    start_date = Date.strptime(params[:start_date], '%d/%m/%Y') if params[:start_date].present?
    end_date = Date.strptime(params[:end_date], '%d/%m/%Y') if params[:end_date].present?
    
    if kategori.present? || tipe.present? || teknisi.present? || status.present? || start_date.present? || end_date.present?
      @tickets = Ticket.all
      @tickets = @tickets.where(kategori: kategori) if kategori.present?
      @tickets = @tickets.where(jenis_layanan: tipe) if tipe.present?
      @tickets = @tickets.where(teknisi: teknisi) if teknisi.present?
      @tickets = @tickets.where(status_tiket: status) if status.present?
    
      if start_date.present? || end_date.present?
        if Ticket.column_names.include?("date_ticket") && Ticket.column_names.include?("created_at")
          @tickets = @tickets.where("tickets.date_ticket >= ? OR tickets.created_at >= ?", start_date, start_date)
                              .where("tickets.date_ticket <= ? OR tickets.created_at <= ?", end_date, end_date)
                              .order("COALESCE(tickets.date_ticket, tickets.created_at) asc")
        elsif Ticket.column_names.include?("date_ticket")
          @tickets = @tickets.where("tickets.date_ticket >= ? AND tickets.date_ticket <= ?", start_date, end_date)
                              .order("tickets.date_ticket asc")
        else
          @tickets = @tickets.where("tickets.created_at >= ? AND tickets.created_at <= ?", start_date, end_date)
                              .order("tickets.created_at asc")
        end
      end
    
      @tickets = @tickets.order("tickets.created_at asc")
    else
      @tickets = Ticket.all
    
      if start_date.present? || end_date.present?
        if Ticket.column_names.include?("date_ticket") && Ticket.column_names.include?("created_at")
          @tickets = @tickets.where("tickets.date_ticket >= ? OR tickets.created_at >= ?", start_date, start_date)
                              .where("tickets.date_ticket <= ? OR tickets.created_at <= ?", end_date, end_date)
                              .order("COALESCE(tickets.date_ticket, tickets.created_at) asc")
        elsif Ticket.column_names.include?("date_ticket")
          @tickets = @tickets.where("tickets.date_ticket >= ? AND tickets.date_ticket <= ?", start_date, end_date)
                              .order("tickets.date_ticket asc")
        else
          @tickets = @tickets.where("tickets.created_at >= ? AND tickets.created_at <= ?", start_date, end_date)
                              .order("tickets.created_at asc")
        end
      end
      
    
      @tickets = @tickets.order("tickets.created_at asc")
    end
    
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "tickets", 
        disable_smart_shrinking: true,
        layout:'pdf_simple.html',
        template: 'tickets/download',
        page_size: 'A4',
        orientation: 'Landscape',
        encoding:"UTF-8",
        show_as_html: params.key?('debug')
      end
    end
  end

  # def cetak_excel
  #   id = params[:id]
  #   cek = Ticket.where(id: id)
  #   if cek.blank?
  #      @contacts = Ticket.all.order("contacts.created_at asc")
  #   else
  #      @contacts = Ticket.where(id: id)
  #   end
  #   @reporting_date = params[:date].nil? ? '': params[:date]
  #   @date = Time.zone.now.strftime("%m/%d/%Y at %I:%M%p") 
  #   render xlsx: "Data Tiket", template: "tickets/report.xlsx.axlsx"
  # end

  def cetak_excel
    kategori = params[:kategori]
    tipe = params[:tipe]
    teknisi = params[:teknisi]
    status = params[:status]
    start_date = Date.strptime(params[:start_date], '%d/%m/%Y') if params[:start_date].present?
    end_date = Date.strptime(params[:end_date], '%d/%m/%Y') if params[:end_date].present?
    
    if kategori.present? || tipe.present? || teknisi.present? || status.present? || start_date.present? || end_date.present?
      @tickets = Ticket.all
      @tickets = @tickets.where(kategori: kategori) if kategori.present?
      @tickets = @tickets.where(jenis_layanan: tipe) if tipe.present?
      @tickets = @tickets.where(teknisi: teknisi) if teknisi.present?
      @tickets = @tickets.where(status_tiket: status) if status.present?
    
      if start_date.present? || end_date.present?
        if Ticket.column_names.include?("date_ticket") && Ticket.column_names.include?("created_at")
          @tickets = @tickets.where("tickets.date_ticket >= ? OR tickets.created_at >= ?", start_date, start_date)
                              .where("tickets.date_ticket <= ? OR tickets.created_at <= ?", end_date, end_date)
                              .order("COALESCE(tickets.date_ticket, tickets.created_at) asc")
        elsif Ticket.column_names.include?("date_ticket")
          @tickets = @tickets.where("tickets.date_ticket >= ? AND tickets.date_ticket <= ?", start_date, end_date)
                              .order("tickets.date_ticket asc")
        else
          @tickets = @tickets.where("tickets.created_at >= ? AND tickets.created_at <= ?", start_date, end_date)
                              .order("tickets.created_at asc")
        end
      end
    
      @tickets = @tickets.order("tickets.created_at asc")
    else
      @tickets = Ticket.all
    
      if start_date.present? || end_date.present?
        if Ticket.column_names.include?("date_ticket") && Ticket.column_names.include?("created_at")
          @tickets = @tickets.where("tickets.date_ticket >= ? OR tickets.created_at >= ?", start_date, start_date)
                              .where("tickets.date_ticket <= ? OR tickets.created_at <= ?", end_date, end_date)
                              .order("COALESCE(tickets.date_ticket, tickets.created_at) asc")
        elsif Ticket.column_names.include?("date_ticket")
          @tickets = @tickets.where("tickets.date_ticket >= ? AND tickets.date_ticket <= ?", start_date, end_date)
                              .order("tickets.date_ticket asc")
        else
          @tickets = @tickets.where("tickets.created_at >= ? AND tickets.created_at <= ?", start_date, end_date)
                              .order("tickets.created_at asc")
        end
      end
      
    
      @tickets = @tickets.order("tickets.created_at asc")
    end
    
    @reporting_date = params[:date].nil? ? '': params[:date]
    @date = Time.zone.now.strftime("%m/%d/%Y at %I:%M%p") 
    render xlsx: "Data Tiket", template: "tickets/report.xlsx.axlsx"
  end
  

  # GET /tickets or /tickets.json
  def index
    #filter
    @ticket = Ticket.all
    @kategori = @ticket.select(:kategori).map(&:kategori).uniq
    @tipe =  @ticket.select(:jenis_layanan).map(&:jenis_layanan).uniq
    @teknisi = @ticket.select(:teknisi).map(&:teknisi).uniq      
    @status = @ticket.select(:status_tiket).map(&:status_tiket).uniq    

    # Deskripsi ID
    # 2 (Self-Service)
    # 3 (Teknisi)
    if roleActive.id == 2
      @tickets = Ticket.where("nama_pemohon = ? OR nama_pemilik_kendala = ?", current_user.name, current_user.name).order(id: :desc)
    elsif roleActive.id == 3
      @tickets = Ticket.where(teknisi: current_user.name).order(:id => :desc)
    elsif roleActive.id == 4
      @tickets = Ticket.order(:id => :desc)
    else
      @tickets = Ticket.order(:id => :desc)
    end 
  end

  # GET /tickets/1 or /tickets/1.json
  def show
  end

  # GET /tickets/new
  # def new
  #   ActiveRecord::Base.transaction do
  #     @rand = rand(111..999)
  #     @no_tiket = "#{Time.now.strftime('%Y')}#{Time.now.strftime('%m')}#{@rand}"
  #     @ticket = Ticket.new
  #   end 
  # end
  def new

      # rand_number = rand(111..999)
      # @no_tiket = "#{Time.now.strftime('%Y')}#{Time.now.strftime('%m')}#{Time.now.strftime('%d')}#{rand_number}"
      # Menghitung jumlah tiket saat ini
    @ticket = Ticket.new
  end
  

  # GET /tickets/1/edit
  def edit
    ActiveRecord::Base.transaction do
      @ticket = Ticket.find(params[:id])
    end
  end

  def superadminEdit
    @ticket = Ticket.find(params[:id])
  end

  def follow_up
    @ticket = Ticket.find(params[:id])
  end

  # POST /tickets or /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    
    @ticket.current_user = current_user
    @atasan = params[:ticket][:atasan_id]
    @user = User.find_by(username: @atasan)
    @ticket_approval = TicketApproval.new
    @ticket.no_tiket = generate_ticket_number 
    respond_to do |format|
      x
      begin
        if @ticket.save
        
        
          if @user.present? && @atasan.present?
            #update table approve atasan
            @ticket_approval.ticket_id = @ticket.id
            @ticket_approval.user_id = @user.id
            @ticket_approval.status = "On Approval"
            @ticket_approval.save 
          else
            #create user, updat table approve atasan
            @user_ldap = User.new
            @user_ldap.username = @atasan
            ldap = Net::LDAP.new :host => '192.168.60.159',
                                :port => 389,
                                :auth => {
                                    :method => :simple,
                                    :username => "cn=manager, dc=pgn-solution, dc=co, dc=id",
                                    :password => "4lh4mdul1ll4h"
                                }
            filter = Net::LDAP::Filter.eq("cn", "#{@atasan}")
            treebase = "dc=pgn-solution, dc=co, dc=id"
            ldap.search(:base => treebase, :filter => filter) do |entry|
              @email = entry["mail"].map(&:inspect).join(', ').gsub('"', '')
              @password = entry["userpassword"].map(&:inspect).join(', ').gsub('"', '')
            end
            @user_ldap.email = "#{@email.present? ? @email : @atasan + '@pgn-solution.co.id' }"
            @user_ldap.password = "#{@password}"
            @user_ldap.name = @atasan.capitalize
            @user_ldap.user_type = "LDAP"
            @user_ldap.role_ids = 2 #self-service #params[:user][:role_ids]
            @user_ldap.save

            if  @atasan.present?
              @ticket_approval.ticket_id = @ticket.id
              @ticket_approval.user_id = @user_ldap.id
              @ticket_approval.status = "On Approval"
              @ticket_approval.save 
            end
          end
          
            @token = "5708298901:AAFc_opJM8HztYRLFzyLFIUhNm4kjsKoP3c" #production"5708298901:AAFc_opJM8HztYRLFzyLFIUhNm4kjsKoP3c"
            Telegram::Bot::Client.run(@token) do |bot|
                @otp_code = rand(1111..9999)
                @arrayID =  [1011681088]
                @arrayID.each do |f|
                  bot.api.send_message(chat_id: f, text:"Requester : #{params[:ticket][:nama_pemilik_kendala]}\nArea : #{params[:ticket][:area_pemilik_kendala]} \nPriority : #{params[:ticket][:priority]} \nAsigne Tech : #{params[:ticket][:teknisi]}\nKategori : #{params[:ticket][:kategori]}\nSub Kategori : #{params[:ticket][:sub_kategori]}\nJenis Layanan : #{params[:ticket][:jenis_layanan]}\nKeterangan : #{params[:ticket][:keterangan]} \nantrian tiketmu :  #{params[:ticket][:antrian]}\nTicket Url :  #{"http://192.168.60.175/tickets/"+"#{@ticket.id}"+"/follow_up"}\n")
                end
            end
          
        
          #email notif
          # @subject = "Tiket Baru"
          # @email_body = "Telah direquest tiket baru oleh user.Dengan informasi sebagai berikut:" +
          # "<br>" +
          # "<br>" +
          # "<br>Salam,<br>"+
          # "<b>HELP DESK<b><br><br><br>"+
          # @email_to = "irvan6500@gmail.com"# RoleAssignment.includes(:user).where(role_id: 13).map{|value| value.user.email}
          # # NotifConfirmationMailer.send_email(@email).deliver_now!
          # begin
          #   TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
          # rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
          #   logger.warn mailing_lists_error
          #   flash.discard[:notice] = "There was a problem with sending to destination email.  Please give information to the SIT " + "#{mailing_lists_error}"
          #   return redirect_to root_path
          # end
          
      
          format.html { redirect_to ticket_url(@ticket), notice: "Ticket was successfully created." }
          format.json { render :show, status: :created, location: @ticket }
        else
          format.html { render :new, status: :unprocessable_entity }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
      rescue StandardError => e
        format.html { redirect_to root_path, alert: "An error occurred while creating the ticket: #{e.message}" }
        format.json { render json: { error: e.message }, status: :unprocessable_entity }
      end 
    end
  end

  def detail_notif
    # @getNotifUser = Ticket.where(status_tiket: ["Closed", "Planning", "Problem"], nama_pemohon: current_user.name).where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).select(:ticket_id)).order(:updated_at => :desc)
    @getNotifAutoRespon = TicketHistory.includes(:ticket)
    .where("tickets.nama_pemohon": current_user.name, status_tiket: ["Answer","Auto Respond"])  
    .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id))

    @getNotifChat = TicketHistory.includes(:ticket)
    .where("tickets.nama_pemohon": current_user.name, status_tiket: ["Answer"])  
    .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id))

    if roleActive.id == 2
      @ticket = TicketHistory.includes(:ticket)
                              .where("tickets.nama_pemohon": current_user.name,  status_tiket: ["Waiting", "On Processing", "Closed", "Problem","Reject","Approved"])
                              .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id)).order(:updated_at => :desc)
    elsif roleActive.id == 3
      @ticket =  TicketHistory.includes(:ticket)
                              .where("tickets.teknisi": current_user.name,  status_tiket: ["Answer","Waiting","On Processing","Approved","Reject"])
                              .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id)).order(:updated_at => :desc)
      
      # Ticket.where(status_tiket: ["Answer","Waiting"], teknisi: current_user.name).where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).select(:ticket_id)).order(:updated_at => :desc)
      # Ticket.where(status_tiket:  ["Waiting", "On Processing", "Problem"] , teknisi: current_user.name).where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).select(:ticket_id)).order(:updated_at => :desc)
    elsif roleActive.id == 4 || roleActive.id == 1
      @ticket = TicketHistory.includes(:ticket)
                              .where("tickets.nama_pemohon": current_user.name,  status_tiket: ["Waiting", "Problem"])
                              .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id)).order(:updated_at => :desc)
      # Ticket.where(status_tiket:["Waiting","Problem"],).where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).select(:ticket_id)).order(:updated_at => :desc)
      # @tickets = Ticket.where(status_tiket: params[:status_tiket]).order(:id => :desc)
    else
      @ticket = TicketHistory.includes(:ticket)
                              .where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).pluck(:ticket_history_id)).order(:updated_at => :desc)
      # Ticket.where.not(id: NotificationReadTicket.where(user_id: current_user.id, read: true).select(:ticket_id)).order(:updated_at => :desc)
      #@tickets = Ticket.where(status_tiket:"Waiting").order(:id => :desc)
    end 
  end


  def create_or_update_user_and_approver(atasannama)
  @user = User.find_by(username: atasannama)
  @atasan = atasannama
  @ticket_approval = TicketApproval.new

  if @user.present? && @atasan.present?
    #update table approve atasan
    @ticket_approval.ticket_id = @ticket.id
    @ticket_approval.user_id = @user.id
    @ticket_approval.status = "On Approval"
    @ticket_approval.save
  else
    #create user, updat table approve atasan
    @user_ldap = User.new
    @user_ldap.username = @atasan
    ldap = Net::LDAP.new :host => '192.168.60.159',
                         :port => 389,
                         :auth => {
                             :method => :simple,
                             :username => "cn=manager, dc=pgn-solution, dc=co, dc=id",
                             :password => "4lh4mdul1ll4h"
                         }
    filter = Net::LDAP::Filter.eq("cn", "#{@atasan}")
    treebase = "dc=pgn-solution, dc=co, dc=id"
    ldap.search(:base => treebase, :filter => filter) do |entry|
      email = entry["mail"].map(&:inspect).join(', ').gsub('"', '')
      password = entry["userpassword"].map(&:inspect).join(', ').gsub('"', '')
      @user_ldap.email = "#{email.present? ? email : @atasan + '@pgn-solution.co.id' }"
      @user_ldap.password = "#{password}"
      @user_ldap.name = @atasan.capitalize
      @user_ldap.user_type = "LDAP"
      @user_ldap.role_ids = 2 #self-service #params[:user][:role_ids]
      @user_ldap.save
    end

    if  @atasan.present?
      @ticket_approval.ticket_id = @ticket.id
      @ticket_approval.user_id = @user_ldap.id
      @ticket_approval.status = "On Approval"
      @ticket_approval.save 
    end
  end
end


  # PATCH/PUT /tickets/1 or /tickets/1.json
  def update
    
    @statusTiket = Category.find_by(name: params[:ticket][:kategori])
    respond_to do |format|
      if URI(request.referrer).path ==  "/tickets/#{params[:id]}/answer"
        begin
          TicketHistory.create(
            ticket_id: @ticket.id,
            no_tiket: @ticket.no_tiket,
            waktu: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            inputer: @ticket.nama_pemohon,
            status_tiket: "Answer",
            issued_by: current_user.name,
            keterangan: params[:ticket][:keterangan],
          )   
          format.html { redirect_to answer_ticket_path(@ticket), notice: "Pesan Terkirim." }
          format.json { render :show, status: :ok, location: @ticket }
        rescue
          format.html { render :edit, status: :unprocessable_entity }
          format.json { render json: @ticket.errors, status: :unprocessable_entity }
        end
        
      elsif @ticket.update(ticket_params)
        if (URI(request.referrer).path == "/tickets/#{params[:id]}/follow_up") && (roleActive.id != 2 && roleActive.id != 3)
          
            # notif ke user tikcet fllwup
            TicketHistory.create(
              :ticket_id => @ticket.id,
              :no_tiket => @ticket.no_tiket,
              :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
              :inputer => @ticket.nama_pemohon,
              :status_tiket => "On Processing", #followup
              :issued_by => current_user.name,
              :keterangan => params[:ticket][:keterangan],
            )
            # Ticket.update(params[:id], {:status_tiket => "On Processing",:teknisi => @ticket.teknisi , :catatan_status => params[:ticket][:catatan_status]}) #params[:ticket][:status_ticket]
            if params[:id].present?
              Ticket.update(params[:id], {
                :status_tiket => "On Processing",
                :teknisi => @ticket.teknisi,
                :catatan_status => params[:ticket][:catatan_status]
              }.merge(ticket_params))
            else
              @ticket.update(ticket_params)
            end
            
            
          @ticket_approval = TicketApproval.find_by(ticket_id: @ticket.id) 
          if params[:ticket][:atasan_id].present? &&  @ticket_approval.present?
            @ticket_approval.update(confirm_admin: true)
          end
          
          @token = "5708298901:AAFc_opJM8HztYRLFzyLFIUhNm4kjsKoP3c"
          Telegram::Bot::Client.run(@token) do |bot|
              @otp_code = rand(1111..9999)
              @arrayID =  [1011681088]
              @arrayID.each do |f|
                bot.api.send_message(chat_id: f, text:"Telah terjadi perubahan teknisi #{params[:ticket][:teknisi]}\n Requester : #{params[:ticket][:nama_pemilik_kendala]}\nArea : #{params[:ticket][:area_pemilik_kendala]} \nPriority : #{params[:ticket][:priority]} \nAsigne Tech : #{params[:ticket][:teknisi]}\nKategori : #{params[:ticket][:kategori]}\nSub Kategori : #{params[:ticket][:sub_kategori]}\nJenis Layanan : #{params[:ticket][:jenis_layanan]}\nKeterangan : #{params[:ticket][:keterangan]} \nantrian tiketmu :  #{params[:ticket][:antrian]}\nSegera dikerjakan! Semangat")
              end
          end
        end
        
        if (URI(request.referrer).path == "/tickets/#{params[:id]}/superadminEdit") && (roleActive.id != 2 && roleActive.id != 3)
          
          # notif ke user tikcet fllwup
            TicketHistory.create(
              :ticket_id => @ticket.id,
              :no_tiket => @ticket.no_tiket,
              :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
              :inputer => @ticket.nama_pemohon,
              :status_tiket => params[:ticket][:status_ticket],
              :issued_by => current_user.name,
              :keterangan => params[:ticket][:keterangan],
            )
            create_or_update_user_and_approver(params[:ticket][:atasan_id])
            # Ticket.update(params[:id], {:status_tiket => "On Processing",:teknisi => @ticket.teknisi , :catatan_status => params[:ticket][:catatan_status]}) #params[:ticket][:status_ticket]
            if params[:id].present?
              Ticket.update(params[:id], {
                :status_tiket => "On Processing",
                :teknisi => @ticket.teknisi,
                :catatan_status => params[:ticket][:catatan_status],

              }.merge(ticket_params))
            else
              @ticket.update(ticket_params)
            end
          # notif ke user tikcet editsuperadmin

          # Ticket.update(params[:id], {:status_tiket => "On Processing",:teknisi => @ticket.teknisi , :catatan_status => params[:ticket][:catatan_status]}) #params[:ticket][:status_ticket]
          if params[:id].present?
            Ticket.update(params[:id], {
              :status_tiket => params[:ticket][:status_ticket],
            }.merge(ticket_params))
          end
  
          @ticket_approval = TicketApproval.find_by(ticket_id: @ticket.id) 
          if params[:ticket][:atasan_id].present? &&  @ticket_approval.present?
            @ticket_approval.update(confirm_admin: true)
          end
          
          @token = "5708298901:AAFc_opJM8HztYRLFzyLFIUhNm4kjsKoP3c"
          Telegram::Bot::Client.run(@token) do |bot|
              @otp_code = rand(1111..9999)
              @arrayID =  [1011681088]
              @arrayID.each do |f|
                bot.api.send_message(chat_id: f, text:"Telah terjadi perubahan data tiket\n Requester : #{params[:ticket][:nama_pemilik_kendala]}\nArea : #{params[:ticket][:area_pemilik_kendala]} \nPriority : #{params[:ticket][:priority]} \nAsigne Tech : #{params[:ticket][:teknisi]}\nKategori : #{params[:ticket][:kategori]}\nSub Kategori : #{params[:ticket][:sub_kategori]}\nJenis Layanan : #{params[:ticket][:jenis_layanan]}\nKeterangan : #{params[:ticket][:keterangan]} \nantrian tiketmu :  #{params[:ticket][:antrian]}\nSegera dikerjakan! Semangat")
              end
          end
        end

        format.html { redirect_to ticket_url(@ticket), notice: "Ticket berhasil diupdate." }
        format.json { render :show, status: :ok, location: @ticket }

      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1 or /tickets/1.json
  def destroy
    unless 
      if @ticket.status_tiket.present?
        @ticket.status_tiket.match(/(Waiting|Processing Assign|Processing Plan|Solved|Closed|Eskalasi|Problem)/) 
        @ticket.destroy
      else
        @ticket.destroy
      end
    end
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: "Ticket was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  
  def read_ticket 
    begin
      @Ticket = Ticket.find_by(id: params[:id]) 

      @notif = NotificationReadTicket.new
      @notif.read = true
      @notif.user_id = current_user.id
      @notif.ticket_id = @Ticket.id
      @notif.ticket_history_id = params[:ticket_history]
      @notif.save 
    rescue
      redirect_to tickets_path,alert: "Data Tidak ditemukan."
    end
  end


  def read_ticket_chat

      @ticket_history = TicketHistory.find_by(id: params[:ticket_history]) 
      if @ticket_history.nil?
        # handle the error condition here
        @notif = NotificationReadTicket.new
        @notif.read = false
        @notif.user_id = current_user.id
        @notif.ticket_id = params[:id]
        @notif.ticket_history_id = params[:ticket_history]
        if @notif.save 
          redirect_to tickets_path, alert: "Terjadi kesalahan saat membaca data."
        else
          Rails.logger.error @notif.errors.full_messages.to_sentence
          redirect_to tickets_path, alert: "Terjadi kesalahan saat menyimpan data."
        end
      else
        # continue with normal processing
        @notif = NotificationReadTicket.new
        @notif.read = true
        @notif.user_id = current_user.id
        @notif.ticket_id = params[:id]
        @notif.ticket_history_id = params[:ticket_history]
        if @notif.save
          redirect_to answer_ticket_path(params[:id])
        else
          Rails.logger.error @notif.errors.full_messages.to_sentence
          redirect_to tickets_path, alert: "Terjadi kesalahan saat menyimpan data."
        end
      end

  end
  
  def read_all_notifications

    # begin
      ticket_ids = params[:id].split('-')
      ticket_history_ids = params[:ticket_history].split('-')
      
    #   pairwise_ticket_ids = ticket_ids.zip(ticket_history_ids)
    #   pairwise_ticket_ids.each do |pair|
    #     ticket_id, ticket_history_id = pair
    #     @ticket = Ticket.find_by(id: ticket_id)
      
    #     next unless @ticket.present? && ticket_history_id.present?
      
    #     @notif = NotificationReadTicket.new(
    #       read: true,
    #       user_id: current_user.id,
    #       ticket_id: @ticket.id,
    #       ticket_history_id: ticket_history_id
    #     )
    #     @notif.save 
    #   end
      
      
      
      
    #   respond_to do |format|
    #     format.html { redirect_to root_path, notice: "successfully read all messages." }
    #     format.json { head :no_content }
    #   end
    # rescue StandardError => e
    #   # Handle the error here
    #   redirect_to root_path ,alert: "An error occurred while reading the notifications: #{e.message}"
    # end
    if ticket_ids.present? && ticket_history_ids.present?
      pairwise_ticket_ids = ticket_ids.zip(ticket_history_ids)
      pairwise_ticket_ids.each do |ticket_id, ticket_history_id|
        @ticket = Ticket.find_by(id: ticket_id)
    
        next unless @ticket.present? && ticket_history_id.present?
    
        @notif = NotificationReadTicket.new(
          read: true,
          user_id: current_user.id,
          ticket_id: @ticket.id,
          ticket_history_id: ticket_history_id
        )
    
        unless @notif.save
          Rails.logger.error @notif.errors.full_messages.to_sentence
          redirect_to tickets_path, alert: "Terjadi kesalahan saat menyimpan data."
          return
        end
      end
    
      redirect_to answer_ticket_path(params[:id])
    else
      redirect_to tickets_path, alert: "Terjadi kesalahan saat membaca data."
    end
    
  end
  

  
  
  
  

  private
    # Use callbacks to share common setup or constraints between actions.
    # require 'securerandom'

    def current_ticket_count
      latest_ticket = Ticket.order(created_at: :desc).first
      latest_ticket_no = latest_ticket.no_tiket if latest_ticket
      ticket_number = latest_ticket_no.slice(8..-1).to_i if latest_ticket_no
    end
    
    def generate_ticket_number
      current_tickets_count = current_ticket_count
      next_ticket_number = current_tickets_count ? current_tickets_count + 1 : 1
      current_date = Time.now.strftime('%Y%m%d')
      # random_number = SecureRandom.random_number(9)
      "#{current_date}#{next_ticket_number}"
    end
    
    
    def set_ticket
      begin
        @ticket = Ticket.find(params[:id])
      rescue StandardError => e
        # Menangani kesalahan yang terjadi
        respond_to do |format|
          format.html { redirect_to root_path, alert: "Failed: #{e.message}" }
          format.json { head :unprocessable_entity }
        end
      end
    end

    # Only allow a list of trusted parameters through.
    def ticket_params
      params.require(:ticket).permit(:date_ticket,:catatan_status,:no_tiket, :nama_pemohon, :divisi_pemohon, :area_pemohon, :nama_pemilik_kendala, :divisi_pemilik_kendala, :area_pemilik_kendala, :kategori, :sub_kategori,:sub_layanan,:jenis_layanan, :assets, :keterangan, :status_teknisi, :teknisi, :status_tiket, :pemilik_kendala, :jabatan_pemilik_kendala, :priority, lampiran:[])
    end
end
