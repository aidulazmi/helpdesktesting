class CategoriesController < ApplicationController
  before_action :set_category, only: %i[ show edit update destroy ]
  load_and_authorize_resource
  respond_to :html, :json
  
  # GET /categories or /categories.json
  def index
    @categories = Category.all
  end

  # GET /categories/1 or /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
    @sub_category = 
    if (@category.sub_category.nil? or @category.sub_category.empty?)
      []
    else
      @category.sub_category[0, 1] == "[" ? JSON::parse(@category.sub_category) : []
    end

    if @sub_category.empty?
      @jenis_layanan = []
      @templates_id = []
      @approved = []
      else
        @jenis_layanan = Array.new(@sub_category.length, [])
        @templates_id = Array.new(@sub_category.length, [])
        @approved = Array.new(@sub_category.length, [])

      @jenis_layanan = @jenis_layanan.map.with_index do |jenis_layanan, i|
        if (@category.jenis_layanan.nil? or @category.jenis_layanan.empty?)
        []
        else
          @category.jenis_layanan[0, 1] == "[" ? JSON::parse(@category.jenis_layanan)[i] : []
        end
      end

      @templates_id = @templates_id.map.with_index do |template, i|
        if (@category.templates_id.nil? or @category.templates_id.empty?)
        []
        else
        @category.templates_id[0, 1] == "[" ? JSON::parse(@category.templates_id)[i] : []
        end
      end

      @approved = @approved.map.with_index do |approved, i|
        if (@category.approved.nil? or @category.approved.empty?)
        []
        else
          @category.approved[0, 1] == "[" ? JSON::parse(@category.approved)[i] : []
        end
      end
    end            

  end

  # POST /categories or /categories.json
  def create
    @category = Category.new(category_params)
    unless params[:category][:sub_category].nil?
      @array = []
      @array = params[:category][:sub_category]
      @array_jenis_layanan = []
      @array_jenis_layanan = params[:category][:jenis_layanan]
      @array_approved = []
      @array_approved = params[:category][:approved]
      @array_template_id = []
      @array_template_id = params[:category][:templates_id]
      @category.sub_category = @array
      @category.jenis_layanan = @array_jenis_layanan
      @category.approved = @array_approved
      @category.templates_id = @array_template_id
    end
    respond_to do |format|
      if @category.save
        # format.html { redirect_to category_url(@category), notice: "Category was successfully created." }
        format.html { redirect_to categories_url, notice: "Category was successfully created." }
        format.json { render :show, status: :created, location: @category }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
      # s
    end
  end

  # PATCH/PUT /categories/1 or /categories/1.json
  def update
    respond_to do |format|
      if @category.update(category_params)
        unless params[:category][:sub_category].nil?
          @array = []
          @array = params[:category][:sub_category]
          @array_jenis_layanan = []
          @array_jenis_layanan = params[:category][:jenis_layanan]
          @array_template_pesan = []
          @array_template_pesan = params[:category][:templates_id]
          @array_approved = []
          @array_approved = params[:category][:approved]
          @category.update(
            sub_category: @array,
            jenis_layanan: @array_jenis_layanan,
            templates_id: @array_template_pesan,
            approved: @array_approved
          )
        end
        
        # format.html { redirect_to category_url(@category), notice: "Category was successfully updated." }
        format.html { redirect_to categories_url, notice: "Category was successfully created." }
        format.json { render :show, status: :ok, location: @category }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  def delete_file
    @attachment = ActiveStorage::Attachment.find(params[:attachment_id])
    @attachment.purge # or use purge_later
    redirect_back(fallback_location: request.referer)
  end

  # DELETE /categories/1 or /categories/1.json
  def destroy
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url, notice: "Category was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def category_params
      params.require(:category).permit(:name, :sub_layanan, :sub_category, :service_type_id, :template, :status_template_tiket, :pic, :jenis_layanan, :jenis_layanan_kategori, :deskripsi_kategori, :approved,:lampiran_icon)
    end
end
