class ApplicationController < ActionController::Base
  before_action :verify_authenticity_token
  protect_from_forgery unless: -> { request.format.json? }
  before_action :authenticate_user!
  before_action :set_devise_timeout
  

  
  #session
  # before_action :check_last_activity #logout otomatis
  # before_action :set_session_timeout
  # before_action :check_last_activity
  #generasi session
 

  protect_from_forgery with: :exception
  # before_action :regenerate_session_id

  include ApplicationHelper
  #  include CsrfProtection

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, alert: 'Access Denied'
  end

  # rescue_from ActiveRecord::RecordNotFound, :with => :rescueParams #Redirect jika parsing data gak ketemu
  # # You want to get exceptions in development, but not in production.
  # unless Rails.application.config.consider_all_requests_local
  #   rescue_from ActionController::RoutingError, with: -> { render_404 }
  # # else
  # #   rescue_from ActionController::RoutingError, with: -> { render_found }
  # end
  # rescue_from AbstractController::ActionNotFound, with: -> { render_404 }

  layout :layout

  def rescueParams
    render_404
  end

  private

  
  def set_devise_timeout
    if current_user.present? && roleActive.present? && roleActive.id == 2
      request.env["devise.skip_trackable"] = false
      Devise.timeout_in = 15.minutes
    else
      request.env["devise.skip_trackable"] = true
      Devise.timeout_in = nil
    end
  end
  # def check_last_activity
  #   if current_user
  #     if session[:last_activity].present? && session[:last_activity] < (Time.now - 5.minutes)
  #       reset_session
  #       flash[:alert] = "Your session has been expired due to inactivity. Please login again."
  #       redirect_to root_path
  #     else
  #       session[:last_activity] = Time.now
  #     end
  #   end
  # end

  def layout
    # only turn it off for login pages:
    is_a?(Devise::SessionsController) ? "login" : "application"
    # or turn layout off for every devise controller:
  end

  # def regenerate_session_id
  #   session.delete(:_csrf_token)
  #   request.session_options[:renew] = true
  # end

  def render_404
    respond_to do |format|
      format.html { render template: 'pages/not_found_error', status: 404 }
      format.all { render nothing: true, status: 404 }
    end
  end

  def render_found
    respond_to do |format|
      format.html { render template: 'pages/internal_server_error', status: 500 }
      format.all { render nothing: true, status: 404 }
    end
  end
end
