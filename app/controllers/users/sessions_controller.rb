# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
    before_action :configure_sign_in_params, only: [:create]
    skip_before_action :verify_authenticity_token, only: :create
    # before_action :set_devise_timeout, if: :user_signed_in?

    # def set_devise_timeout
    #   if current_user.roles.ids.include?(2)
    #     request.env["devise.skip_trackable"] = false
    #     Devise.timeout_in = 3.seconds
    #   end
    # end
    
    # GET /resource/sign_in
    def new
      super
    end
  

    def decryptAES(encrypted_data, cipher_type="AES-256-CBC")
      # begin
        @decode = Base64.decode64(encrypted_data)
        cipher = OpenSSL::Cipher::Cipher.new(cipher_type)
        cipher.decrypt
        cipher.key = "1e4fedbbf9cdacaaf1213eb0f99d18af"
    
        cipher.iv = "f1213eb0f99d18af" if "f1213eb0f99d18af" != nil
        cipher.update(@decode) << cipher.final
      # rescue
      #   flash[:warning] = "Invalid email or password"
      # end
    end
  

    # POST /resource/sign_in

    # last 16-01-2023
    def create
      username = decryptAES(params['user']['username'])
      password = decryptAES(params['user']['password'])
    
      
        if username.match(/(<|>|;|'|"|--)/)
          flash[:danger] = "Invalid username"
          redirect_to new_user_session_path
        else
          user = User.find_by(username: username)
          if user && user.valid_password?(password)
            sign_in(user)
            @checkExistRole = RoleAssignment.where('user_id = ?', user.id).select('role_id')
            array_role = []
            @checkExistRole.each do |checkExistRole|
              array_role[checkExistRole.role_id.to_i] = checkExistRole.role_id
            end
      
            
            @roles_aktif = RoleAssignment.where('user_id = ? and role_id IN (?)', user.id, array_role).select('role_id').first
            flash[:notice] = "Login Success"
            super
            session[:active_role] = @roles_aktif.role_id

          elsif !user || user.nil? || user.present?
            super
            # @checkExistRole = RoleAssignment.where('user_id = ?', user.id).select('role_id')
            # array_role = []
            # @checkExistRole.each do |checkExistRole|
            #   array_role[checkExistRole.role_id.to_i] = checkExistRole.role_id
            # end
      
            # @roles_aktif = RoleAssignment.where('user_id = ? and role_id IN (?)', user.id, array_role).select('role_id').first
            # flash[:notice] = "Login Success"
            session[:active_role] = 2
            # x
          else
            flash[:alert] = "Invalid email or password"
            redirect_to new_user_session_path
            
            # x
          end
        end
     
    end
    
    # def create
    #   username = decryptAES(params['user']['username'])
    #   password = decryptAES(params['user']['password'])
    
    #   if username.match(/(<|>|;|'|"|--)/)
    #     flash[:error] = "Invalid username"
    #     redirect_to new_user_session_path
    #   else
    #     user = User.find_by(username: username)
    #     if user && user.valid_password?(password)
    #       sign_in(user)
    #       @checkExistRole = RoleAssignment.where('user_id = ?', user.id).select('role_id')
    #       array_role = []
    #       @checkExistRole.each do |checkExistRole|
    #         array_role[checkExistRole.role_id.to_i] = checkExistRole.role_id
    #       end
    
    #       @roles_aktif = RoleAssignment.where('user_id = ? and role_id IN (?)', user.id, array_role).select('role_id').first
    #       super
    #       session[:active_role] = @roles_aktif.role_id
    #     else
    #       flash[:error] = "Invalid email or password"
    #       render 'new'
    #     end
    #   end
    # end

    # def create

    #   username = decryptAES(params['user']['username'])
    #   password = decryptAES(params['user']['password'])   

    #   password_bcrypt = BCrypt::Password.create(password.to_s)

    #   @username = User.where('username = ?', username)


      
    #   unless username.match(/(<|>|;|'|"|--)/)
    #     @checkExistRole = RoleAssignment.where('user_id = ?',current_user).select('role_id')
    #     array_role = []
    #     @checkExistRole.each do |checkExistRole|
    #       array_role[checkExistRole.role_id.to_i] = checkExistRole.role_id
    #     end

    #     @roles_aktif = RoleAssignment.where('user_id = ? and role_id IN (?)', current_user, array_role).select('role_id').first
    #     super
        
    #     session[:active_role] = @roles_aktif.role_id
  
    #   end
    # end
  
    # DELETE /resource/sign_out
    def destroy
      reset_session
      super
    end
  
    protected
  
    def after_sign_out_path_for(resource_or_scope)
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      router_name = Devise.mappings[scope].router_name
      router_name ? send(router_name) : self
      "/login_page"
      #context.respond_to?(:root_path) ? context.root_path : "/"
    end
  
    # If you have extra params to permit, append them to the sanitizer.
    def configure_sign_in_params
      devise_parameter_sanitizer.permit(:sign_in, keys: [:username, :password])
    end
  end
  