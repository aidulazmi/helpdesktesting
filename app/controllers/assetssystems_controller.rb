class AssetssystemsController < ApplicationController
  before_action :set_assetssystem, only: %i[ show edit update destroy ]

  # GET /assetssystems or /assetssystems.json
  def index
    @assetssystems = Assetssystem.all
  end

  # GET /assetssystems/1 or /assetssystems/1.json
  def show
  end

  # GET /assetssystems/new
  def new
    @assetssystem = Assetssystem.new
  end

  # GET /assetssystems/1/edit
  def edit
  end

  # POST /assetssystems or /assetssystems.json
  def create
    @assetssystem = Assetssystem.new(assetssystem_params)

    respond_to do |format|
      if @assetssystem.save
        format.html { redirect_to assetssystem_url(@assetssystem), notice: "Assetssystem was successfully created." }
        format.json { render :show, status: :created, location: @assetssystem }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @assetssystem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assetssystems/1 or /assetssystems/1.json
  def update
    respond_to do |format|
      if @assetssystem.update(assetssystem_params)
        format.html { redirect_to assetssystem_url(@assetssystem), notice: "Assetssystem was successfully updated." }
        format.json { render :show, status: :ok, location: @assetssystem }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @assetssystem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assetssystems/1 or /assetssystems/1.json
  def destroy
    @assetssystem.destroy

    respond_to do |format|
      format.html { redirect_to assetssystems_url, notice: "Assetssystem was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assetssystem
      @assetssystem = Assetssystem.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def assetssystem_params
      params.require(:assetssystem).permit(:nama, :nama_perangkat, :tipe_perangkat, :lokasi, :tahun)
    end
end
