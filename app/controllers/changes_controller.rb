class ChangesController < ApplicationController
  before_action :set_change, only: %i[ show edit update destroy ]

  # GET /changes or /changes.json
  def index
    @changes = Change.all
  end

  # GET /changes/1 or /changes/1.json
  def show
  end

  # GET /changes/new
  def new
    @change = Change.new
  end

  # GET /changes/1/edit
  def edit
  end

  # POST /changes or /changes.json
  def create
    if params[:kategori_asset] == "Asset User"
      Assetsuser.update(params[:id], user: params[:change][:nama_user])
    end
    
    if params[:kategori_asset] == "Asset System"
      Assetsystem.update(params[:id], lokasi: params[:lokasi])
    end
    
    if params[:kategori_asset] == "Asset Digital"
      Assetsystem.update(params[:id], pemilik_proses: params[:pemilik_proses])
    end
    
    @change = Change.new(change_params)
    
    nama_perangkat = params[:change][:nama_perangkat]
    if nama_perangkat.present?
      values = nama_perangkat.split(" | ")
      user = values[0]
      nama_perangkat = values[1]
    else
      user = nil
      nama_perangkat = nil
    end
    
    
    respond_to do |format|
      if @change.save
        if user.present? && nama_perangkat.present?
          @change.update(nama_user: user, nama_perangkat: nama_perangkat)
        end
        
        format.html { redirect_to change_url(@change), notice: "Change was successfully created." }
        format.json { render :show, status: :created, location: @change }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @change.errors, status: :unprocessable_entity }
      end
    end
  end
  

  # PATCH/PUT /changes/1 or /changes/1.json
  def update
    x
    respond_to do |format|
      if @change.update(change_params)
        format.html { redirect_to change_url(@change), notice: "Change was successfully updated." }
        format.json { render :show, status: :ok, location: @change }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @change.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /changes/1 or /changes/1.json
  def destroy
    @change.destroy

    respond_to do |format|
      format.html { redirect_to changes_url, notice: "Change was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def delete_file
    @attachment = ActiveStorage::Attachment.find(params[:attachment_id])
    @attachment.purge # or use purge_later
    redirect_back(fallback_location: request.referer)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_change
      @change = Change.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def change_params
      params.require(:change).permit(:tanggal, :judul, :teknisi, :kategori_asset, :nama_system, :lokasi,:nama_perangkat, :nama_user, :nama_aplikasi, :pemilik_proses, :deskripsi,:lampiran_change)
    end
end
