class AssetsdigitalsController < ApplicationController
  before_action :set_assetsdigital, only: %i[ show edit update destroy ]

  # GET /assetsdigitals or /assetsdigitals.json
  def index
    @assetsdigitals = Assetsdigital.all
  end

  # GET /assetsdigitals/1 or /assetsdigitals/1.json
  def show
  end

  # GET /assetsdigitals/new
  def new
    @assetsdigital = Assetsdigital.new
  end

  # GET /assetsdigitals/1/edit
  def edit
  end

  # POST /assetsdigitals or /assetsdigitals.json
  def create
    @assetsdigital = Assetsdigital.new(assetsdigital_params)

    respond_to do |format|
      if @assetsdigital.save
        format.html { redirect_to assetsdigital_url(@assetsdigital), notice: "Asset Digital was successfully created." }
        format.json { render :show, status: :created, location: @assetsdigital }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @assetsdigital.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assetsdigitals/1 or /assetsdigitals/1.json
  def update
    respond_to do |format|
      if @assetsdigital.update(assetsdigital_params)
        format.html { redirect_to assetsdigital_url(@assetsdigital), notice: "Asset Digital was successfully updated." }
        format.json { render :show, status: :ok, location: @assetsdigital }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @assetsdigital.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assetsdigitals/1 or /assetsdigitals/1.json
  def destroy
    @assetsdigital.destroy

    respond_to do |format|
      format.html { redirect_to assetsdigitals_url, notice: "Asset Digital was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def import
    begin
      Assetsdigital.import(params[:file])
      redirect_to assetsdigitals_url, notice: "Import successful."
    rescue StandardError => e
      redirect_to assetsdigitals_url, alert: "Import failed: #{e.message}"
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assetsdigital
      @assetsdigital = Assetsdigital.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def assetsdigital_params
      params.require(:assetsdigital).permit(:nama_aplikasi, :pemilik_proses, :deskripsi, :pic, :bahasa, :framework, :current_version, :lisensi, :antivirus, :server_hardening, :intruder_incident, :availability, :pemeliharaan, :bug_fixing, :waktu_respon, :jam_layanan, :waktu_eskalasi, :waktu_respon2, :pemeliharaan, :periode, :retensi, :storage_server, :server, :internet, :proteksi_akses, :antivirus, :domain, :bug_fixing, :uji_keamanan, :biaya_hardware, :biaya_jaringan, :biaya_keamanan, :biaya_pemeliharaan, :total)
    end
end
