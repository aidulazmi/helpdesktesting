class LossEventsController < ApplicationController
  before_action :set_loss_event, only: %i[ show edit update destroy ]

  # GET /loss_events or /loss_events.json
  def index
    @loss_events = LossEvent.all
  end

  # GET /loss_events/1 or /loss_events/1.json
  def show
  end

  # GET /loss_events/new
  def new
    @loss_event = LossEvent.new
    @teknisi = RoleAssignment.includes(:user).where(role_id: 3).map{|value| value.user.name} 
  end

  # GET /loss_events/1/edit
  def edit
    @teknisi = RoleAssignment.includes(:user).where(role_id: 3).map{|value| value.user.name} 
  end

  # POST /loss_events or /loss_events.json
  def create
    @loss_event = LossEvent.new(loss_event_params)

    respond_to do |format|
      if @loss_event.save
        format.html { redirect_to loss_event_url(@loss_event), notice: "Loss event was successfully created." }
        format.json { render :show, status: :created, location: @loss_event }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @loss_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loss_events/1 or /loss_events/1.json
  def update
    respond_to do |format|
      if @loss_event.update(loss_event_params)
        format.html { redirect_to loss_event_url(@loss_event), notice: "Loss event was successfully updated." }
        format.json { render :show, status: :ok, location: @loss_event }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @loss_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loss_events/1 or /loss_events/1.json
  def destroy
    @loss_event.destroy

    respond_to do |format|
      format.html { redirect_to loss_events_url, notice: "Loss event was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loss_event
      @loss_event = LossEvent.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def loss_event_params
      params.require(:loss_event).permit(:nama, :tanggal, :tempat, :status, :pic, :deskripsi_kejadian, :kategori)
    end
end
