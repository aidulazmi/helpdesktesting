class PostController < ApplicationController
    def index
        @posts = Post.all
    end
    def new

        @post = Post.new
    end

    def storeData

        @post = Post.new(post_params)
        if @post.save
           
            redirect_to post_index_path, notice: "Berhasil menambahkan data!"
        else

            render :new, alert: "Gagal menambahkan data!"
        end
    end

    private
    def post_params
        params.require(:post).permit(:title, :header, :desc)
    end
end
