class AssetssitsController < ApplicationController
  before_action :set_assetssit, only: %i[ show edit update destroy ]

  # GET /assetssits or /assetssits.json
  def index
    @assetssits = Assetssit.all
  end

  # GET /assetssits/1 or /assetssits/1.json
  def show
  end

  # GET /assetssits/new
  def new
    @assetssit = Assetssit.new
  end

  # GET /assetssits/1/edit
  def edit
  end

  # POST /assetssits or /assetssits.json
  def create
    @assetssit = Assetssit.new(assetssit_params)

    respond_to do |format|
      if @assetssit.save
        format.html { redirect_to assetssit_url(@assetssit), notice: "Assetssit was successfully created." }
        format.json { render :show, status: :created, location: @assetssit }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @assetssit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assetssits/1 or /assetssits/1.json
  def update
    respond_to do |format|
      if @assetssit.update(assetssit_params)
        format.html { redirect_to assetssit_url(@assetssit), notice: "Assetssit was successfully updated." }
        format.json { render :show, status: :ok, location: @assetssit }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @assetssit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assetssits/1 or /assetssits/1.json
  def destroy
    @assetssit.destroy

    respond_to do |format|
      format.html { redirect_to assetssits_url, notice: "Assetssit was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assetssit
      @assetssit = Assetssit.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def assetssit_params
      params.require(:assetssit).permit(:nama_perangkat, :merk, :tipe)
    end
end
