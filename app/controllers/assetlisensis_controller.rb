class AssetlisensisController < ApplicationController
  load_and_authorize_resource
  before_action :set_assetlisensi, only: %i[ show edit update destroy ]

  # GET /assetlisensis or /assetlisensis.json
  def index
    @assetlisensis = Assetlisensi.all
  end

  # GET /assetlisensis/1 or /assetlisensis/1.json
  def show
  end

  # GET /assetlisensis/new
  def new
    @assetlisensi = Assetlisensi.new
  end

  # GET /assetlisensis/1/edit
  def edit
  end

  # POST /assetlisensis or /assetlisensis.json
  def create
    @assetlisensi = Assetlisensi.new(assetlisensi_params)

    respond_to do |format|
      if @assetlisensi.save
        format.html { redirect_to assetlisensi_url(@assetlisensi), notice: "Assetlisensi was successfully created." }
        format.json { render :show, status: :created, location: @assetlisensi }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @assetlisensi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assetlisensis/1 or /assetlisensis/1.json
  def update
    respond_to do |format|
      if @assetlisensi.update(assetlisensi_params)
        format.html { redirect_to assetlisensi_url(@assetlisensi), notice: "Assetlisensi was successfully updated." }
        format.json { render :show, status: :ok, location: @assetlisensi }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @assetlisensi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assetlisensis/1 or /assetlisensis/1.json
  def destroy
    @assetlisensi.destroy

    respond_to do |format|
      format.html { redirect_to assetlisensis_url, notice: "Assetlisensi was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assetlisensi
      @assetlisensi = Assetlisensi.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def assetlisensi_params
      params.require(:assetlisensi).permit(:type_lisensi, :nama_lisensi, :jumlah_lisensi, :divisi, :penerima, :lokasi, :tahun_pembelian, :waktu_instalasi, :waktu_expired, :nama_perangkat, :serial_number, :pic, :upload, :keterangan)
    end
end
