class TicketProblemsController < ApplicationController
  before_action :set_ticket_problem, only: %i[ show edit update destroy ]

  # GET /ticket_problems or /ticket_problems.json
  def index
    # @ticket_problems = TicketProblem.all
      if roleActive.id == 2  #self-services
        @ticket_problems = TicketProblem.joins(:ticket).where("tickets.nama_pemohon = ? OR tickets.nama_pemilik_kendala = ?", current_user.name, current_user.name).order(id: :desc)
      elsif roleActive.id == 3 #teknisi
        @ticket_problems = TicketProblem.joins(:ticket).where(tickets: {teknisi: current_user.name}).order(id: :desc)
      else
        @ticket_problems = TicketProblem.all.order(id: :desc)
      end
  end

  

  # GET /ticket_problems/1 or /ticket_problems/1.json
  def show
  end

  # GET /ticket_problems/new
  def new
    @ticket_problem = TicketProblem.new
  end

  # GET /ticket_problems/1/edit
  def edit
  end

  # POST /ticket_problems or /ticket_problems.json
  def create
    @ticket_problem = TicketProblem.new(ticket_problem_params)

    respond_to do |format|
      if @ticket_problem.save
        format.html { redirect_to ticket_problem_url(@ticket_problem), notice: "Ticket problem was successfully created." }
        format.json { render :show, status: :created, location: @ticket_problem }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @ticket_problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ticket_problems/1 or /ticket_problems/1.json
  def update
    respond_to do |format|
      if @ticket_problem.update(ticket_problem_params)
            # notif ke user tikcet fllwup
            TicketHistory.create(
              :ticket_id => @ticket_problem.ticket_id,
              :no_tiket => @ticket_problem.no_ticket_problem,
              :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
              :inputer => @ticket_problem.ticket.nama_pemohon,
              :status_tiket => @ticket_problem.status, #followup
              :issued_by => current_user.name,
              :keterangan => @ticket_problem.keterangan,
            )

            
        format.html { redirect_to ticket_problem_url(@ticket_problem), notice: "Ticket problem was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket_problem }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket_problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticket_problems/1 or /ticket_problems/1.json
  def destroy
    @ticket_problem.destroy

    respond_to do |format|
      format.html { redirect_to ticket_problems_url, notice: "Ticket problem was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket_problem
      @ticket_problem = TicketProblem.includes(:ticket).find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def ticket_problem_params
      params.require(:ticket_problem).permit(:ticket_id, :no_ticket_problem, :status, :teknisi, :priority, :kategori, :sub_kategori, :keterangan)
    end
end
