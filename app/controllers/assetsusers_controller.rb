class AssetsusersController < ApplicationController
  before_action :set_assetsuser, only: %i[ show edit update destroy ]

  # GET /assetsusers or /assetsusers.json
  def index
    if roleActive.id != 2

      @assetsusers = Assetsuser.all
            
      
    elsif roleActive.id == 2
       @assetsusers = Assetsuser.where(email: current_user.email)
    end
  end

  # GET /assetsusers/1 or /assetsusers/1.json
  def show
  end

  # GET /assetsusers/new
  
    def new
      ActiveRecord::Base.transaction do
        i = 0
        loop do
          i=i+1 
        @nama_komputer = "SOL#{params[:nama_perangkat]}#{Time.now.strftime('%y')}-#{params[:divisi]}#{i}"
        @assetsuser = Assetsuser.new
      break
        end
    end
  end

  # GET /assetsusers/1/edit
  def edit
  end

  # POST /assetsusers or /assetsusers.json
  def create
    @assetsuser = Assetsuser.new(assetsuser_params)
    # @getSequence = Assetsuser.last
    # if @getSequence.present?
    #   @split = @getSequence.nama_komputer.scan(/\d+/)
    #   @no = @split[1].last(3).to_i + 1
    # else
    #   @no = "001"
    # end
 
    # @assetsuser.nama_komputer = "SOL#{params[:assetsuser][:nama_perangkat]}#{Time.now.strftime('%y')}-#{params[:assetsuser][:divisi]}#{params[:assetsuser][:wo_ke]}#{@no}"
  
    respond_to do |format|
      if @assetsuser.save
        @getDeviceName = CollectionAsset.find_by(params[:assetsuser][:nama_perangkat])
        @updateNamaPerangkat = Assetsuser.last
        @updateNamaPerangkat.update(nama_perangkat: @getDeviceName.device_name)
        format.html { redirect_to assetsuser_url(@assetsuser), notice: "Asset User was successfully created." }
        format.json { render :show, status: :created, location: @assetsuser }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @assetsuser.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /assetsusers/1 or /assetsusers/1.json
  def update
    respond_to do |format|
      if @assetsuser.update(assetsuser_params)
        format.html { redirect_to assetsuser_url(@assetsuser), notice: "Asset User was successfully updated." }
        format.json { render :show, status: :ok, location: @assetsuser }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @assetsuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assetsusers/1 or /assetsusers/1.json
  def destroy
    @assetsuser.destroy

    respond_to do |format|
      format.html { redirect_to assetsusers_url, notice: "Asset User was successfully destroyed." }
      format.json { head :no_content }
    end
  end

    def cetak_pdf
      divisi = params[:divisi]
      status_laptop = params[:status_laptop]
      if divisi.present? || status_laptop.present? 
        @assetuser = Assetsuser.all
        @assetuser = @assetuser.where(divisi: divisi) if divisi.present?
        @assetuser = @assetuser.where(status_laptop: status_laptop) if status_laptop.present?
        @assetuser = @assetuser.order("assetsusers.created_at asc")
      else
        @assetuser = Assetsuser.all.order("assetsusers.created_at asc")
      end
      
      respond_to do |format|
        format.html
        format.pdf do
          render pdf: "assetsusers", 
          disable_smart_shrinking: true,
          layout:'pdf_simple.html',
          template: 'assetsusers/download',
          # page_size: 'A4',
          page_size: 'A0',
          page_width: '441mm',
          page_height: '389mm',
          orientation: 'Landscape',
          encoding:"UTF-8",
          show_as_html: params.key?('debug')
        end
      end
    end

  def cetak_excel
    divisi = params[:divisi]
    status_laptop = params[:status_laptop]
    if divisi.present? || status_laptop.present? 
      @assetuser = Assetsuser.all
      @assetuser = @assetuser.where(divisi: divisi) if divisi.present?
      @assetuser = @assetuser.where(status_laptop: status_laptop) if status_laptop.present?
      @assetuser = @assetuser.order("assetsusers.created_at asc")
    else
      @assetuser = Assetsuser.all.order("assetsusers.created_at asc")
    end
    @reporting_date = params[:date].nil? ? '': params[:date]
    @date = Time.zone.now.strftime("%m/%d/%Y at %I:%M%p") 
    render xlsx: "Data Asset User", template: "assetsusers/report.xlsx.axlsx"
  end
  
  def delete_file
    @attachment = ActiveStorage::Attachment.find(params[:attachment_id])
    @attachment.purge # or use purge_later
    redirect_back(fallback_location: request.referer)
  end

  def import
    begin
      data_count = Assetsuser.import(params[:file])
      redirect_to assetsusers_path, notice: "#{data_count} data imported successfully."
    rescue StandardError => e
      redirect_to assetsusers_path, alert: "Import failed: #{e.message}"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assetsuser
      @assetsuser = Assetsuser.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def assetsuser_params
      params.require(:assetsuser).permit(
        :lampiran, 
        :user, 
        :divisi, 
        :nip, 
        :lokasi, 
        :nama_perangkat, 
        :tahun, 
        :vendor, 
        :jenis_kontrak, 
        :wo_ke, 
        :nama_komputer,
        :serial_number,
        :mac_address, 
        :merk, 
        :tipe, 
        :no_inventaris, 
        :penyedia, 
        :keterangan,
        :status_pekerja,
        :email,
        :no_hp,
        :status_laptop,
        :periode_laptop,
        :spesifikasi,
        :no_hbb,
        :jenis_perangkat,
        :no_bast,
        :manual_user
        )
    end
end
