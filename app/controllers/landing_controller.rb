class LandingController < ApplicationController
  def index
    @roles_list = RoleAssignment.where("user_id=? and ROLE_ID=? ", current_user, session[:role_aktif]).order("ROLE_ID DESC").first 

    if @roles_list==nil
      @roles_list = RoleAssignment.where("user_id=?", current_user).order("ROLE_ID DESC").first 
    end 
    # PerizinanJob.set(wait: 15.seconds).perform_later
  end

  def set_role	
  	session[:active_role] = params[:set_id]
	  redirect_to params[:set_url]
  end
end