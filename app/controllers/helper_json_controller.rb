class HelperJsonController < ApplicationController
    skip_before_action :verify_authenticity_token, only: [:update_session_feature]

    include ApplicationHelper
    def destroy_chat
        begin
          @ticket_history = TicketHistory.find_by(id: params[:id])
          @ticket_history.destroy
      
          respond_to do |format|
            format.html { redirect_to answer_ticket_path(params[:ticket_id]), notice: "Chat was successfully destroyed." }
            format.json { head :no_content }
          end
        rescue StandardError => e
          # Menangani kesalahan yang terjadi
          respond_to do |format|
            format.html { redirect_to answer_ticket_path(params[:ticket_id]), alert: "Failed to destroy chat: #{e.message}" }
            format.json { head :unprocessable_entity }
          end
        end
      end
    def getTemplateTiket
        subLayanan = params[:id]
        render json: MessageTemplate.select('*')
                    .map{ |vendor|
                        {
                            :id => vendor.id,
                            :template => vendor.template,
                            :name => vendor.name
                        } #, :template => vendor.template
                    }
      end

      def update_session_feature
        session[:feature_id] = params[:session_id]
        render json: { status: 'success' }
      end  

    def getMerkPerangkat
        @kodeType = params[:id]
        @deviceType = CollectionAsset.find_by(kode: @kodeType).device_name
        @brands = Assetssit.where(nama_perangkat: @deviceType).distinct.pluck("merk")
        render json: @brands.to_json
    end
    def getSubKategori2
        @kodeType = params[:id]
        @deviceType = CollectionAsset.find_by(device_name: @kodeType)
        @brands = Category.where(name: @deviceType).distinct.pluck("sub_category")
        render json: @brands.to_json
    end
    def getTipePerangkat
        # HP-Laptop.split('-')
        # ["HP", "Laptop"]
        @identity = params[:id].split('-')
        @deviceType = CollectionAsset.find_by(kode: @identity[1]).device_name
        @devices = Assetssit.where(nama_perangkat: @deviceType).where(merk: @identity[0]).pluck("tipe")
        render json: @devices.to_json
    end

    def getSubLayanan
        categoryName = params[:id]
        subLayanan = Category.where(name: categoryName).distinct.pluck("sub_layanan")
        render json: subLayanan.to_json
    end
    
    def getLayanan
        categoryName = params[:id]
        subLayanan = CollectionAsset.where(jenis_layanan: categoryName).distinct.pluck("device_name")
        render json: subLayanan.to_json
    end

    def getSubKategori
        subLayanan = params[:id]
        render json: Category.select('*')
                    .where(name: subLayanan)
                    .map{ |vendor|
                        {:value => vendor.sub_category} #, :template => vendor.template
                    }
    end

    # def getTemplates
    #     sub_category = params[:sub_category]
    #     jenis_layanan = params[:jenis_layanan]
    #     kategori = params[:kategori]
      
    #     query = MessageTemplate.select('*')
    #     query = query.where(sub_category: sub_category) if sub_category.present?
    #     query = query.where(jenis_layanan: jenis_layanan) if jenis_layanan.present?
    #     query = query.where(kategori: kategori) if kategori.present?
      
    #     result = query.map do |vendor|
    #       {
    #         value: vendor.template,
    #         approvals: vendor.approvals.nil? ? "null" : vendor.approvals.to_s
    #       }
    #     end
      
    #     render json: result
    #   end
    def getTemplates
        sub_category = params[:id]
        jenis_layanan = params[:jenis_layanan]

        query = MessageTemplate.select('*')
        query = query.where(sub_category: sub_category) if sub_category.present?
        query = query.where(jenis_layanan: jenis_layanan) if jenis_layanan.present?

        render json: query.map{ |vendor|
                                {:value => vendor.template,
                                :approvals => vendor.approvals.nil? ? "null" : vendor.approvals.to_s}
                            }
    end

    # def getTemplatesBySubKategori
    #     sub_category = params[:id]
    #     render json:MessageTemplate.select('*')
    #                 .where(sub_category: "#{sub_category}"
    #                 #, jenis_layanan: ""
    #                 )
    #                 .map{ |vendor|
    #                 {:value => vendor.template,
    #                 :approvals => vendor.approvals.nil? ? "null" : vendor.approvals.to_s
    #                 }
    #                 }
    # end

    # def getTemplatesByJenisLayanan
    #     jenis_layanan = params[:id]
    #     render json:MessageTemplate.select('*')
    #                 .where(jenis_layanan: "#{jenis_layanan}")
    #                 .map{ |vendor|
    #                 {:value => vendor.template,
    #                 :approvals => vendor.approvals.nil? ? "null" : vendor.approvals.to_s}
    #                 }
    # end

    def getTemplatesInAdminByKategori
        sub_kategori = params[:sub_kategori]
        kategori_id = params[:kategori_id]
        f = []
        Category.where("sub_category LIKE ?", "%#{sub_kategori}%").order(name: :asc)
                                .where(name: kategori_id)
                                .order(name: :asc)
                                .map { |category|
        sub_categories = category.sub_category.gsub(/\[|\]|"/, '').split(/,\s*/)
        templates = category.templates_id.gsub(/\[|\]|"/, '').split(/,\s*/)
        approved = category.approved.gsub(/\[|\]|"/, '').split(/,\s*/)
        sub_categories.each_with_index { |sub_category, index|
            f[index] ||= {}
            f[index][:sub_category] = sub_category.strip
            f[index][:approved] = approved[index] 
            template_id = templates[index].strip
            template = MessageTemplate.find_by(id: template_id)
            f[index][:message_template] = template
        }
        }
        render json: f
    end

    def getTemplatesByKategori
        sub_kategori = params[:sub_kategori]
        kategori_id = params[:kategori_id]
        f = []
        Category.where("sub_category LIKE ?", "%#{sub_kategori}%").order(name: :asc)
                                .where(name: kategori_id)
                                .order(name: :asc)
                                .map { |category|
        sub_categories = category.sub_category.gsub(/\[|\]|"/, '').split(/,\s*/)
        templates = category.templates_id.gsub(/\[|\]|"/, '').split(/,\s*/)
        approved = category.approved.gsub(/\[|\]|"/, '').split(/,\s*/)
        sub_categories.each_with_index { |sub_category, index|
            f[index] ||= {}
            f[index][:sub_category] = sub_category.strip
            f[index][:approved] = approved[index] 
            template_id = templates[index].strip
            template = MessageTemplate.find_by(id: template_id)
            f[index][:message_template] = template
        }
        }
        render json: f
    end

    def getPerintilan
        subCategory = params[:id]
        render json: Category.select('*')
                    .where("sub_category LIKE ?", "%#{subCategory}%")
                    .map{ |vendor|
                        {:tipe_layanan => vendor.service_type.nama, :teknisi => vendor.pic,  :template => vendor.template, :status_tiket => vendor.status_template_tiket}
                    }
    end

        

    def getTeknisi
        id = params[:id]
        render json: Category.select('pic')
                        .where("name LIKE ?", "%#{id}%")
                        .map{ |pic|
                            pic.pic
                        }
    end

    def getPriority
        @priority = params[:id]
        render json: Position.select('*')
                            .where("nama LIKE ?", "%#{@priority}%")
                            .map{ |vendor|
                                {:value => vendor.priority}
                            }

      end

      def getStatusTeknisi
        teknisi = params[:id]
        @checkTechnician = Ticket.where('teknisi = ? and status_teknisi LIKE ?', teknisi, '%Waiting%')
        @countingTechnician = Ticket.where('teknisi = ? and status_teknisi LIKE ?', teknisi, "%Waiting%").order(:teknisi => :asc)
        if @checkTechnician.exists? 
            if @countingTechnician.count == 1
                render :json => "Antrian #{@countingTechnician.count} tiket".to_json
            else
                render :json => "Antrian #{@countingTechnician.count} tiket".to_json
            end
        else
            render :json => "Available".to_json
        end
      end

    #   def getStatusTeknisi
    #     teknisi = params[:id]
    #     @checkTechnician = Ticket.where('teknisi = ? and status_teknisi LIKE ?', teknisi, '%Waiting%')
    #     @countingTechnician = Ticket.where('teknisi = ? and status_teknisi LIKE ?', teknisi, "%Waiting%").order(:teknisi => :asc)
    #     if @checkTechnician.exists? 
    #         if @countingTechnician.count == 1
    #             render :json => "Antrian #{@countingTechnician.count} tiket".to_json
    #         else
    #             render :json => "Antrian #{@countingTechnician.count} tiket".to_json
    #         end
    #     else
    #         render :json => "Available".to_json
    #     end
    #   end

      def getStatusTeknisiDashboard
        # teknisi = params[:id]
        @checkTechnician = Ticket.where(' status_teknisi LIKE ?', '%Waiting%')
        @countingTechnician = Ticket.where(' status_teknisi LIKE ?', "%Waiting%").order(:teknisi => :asc)
        if @checkTechnician.exists? 
            if @countingTechnician.count == 1
                render :json => "Antrian #{@countingTechnician.count} tiket".to_json
            else
                render :json => "Antrian #{@countingTechnician.count} tiket".to_json
            end
        else
            render :json => "Available".to_json
        end
      end


      def switchRole
        session[:active_role] = params[:id]
      end
end
