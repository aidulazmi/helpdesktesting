class ProcessTicketsController < ApplicationController
    require 'action_view'
    include ActionView::Helpers::DateHelper
    
    # def autorespond

    # end

    def processTechnician
        @ticket = Ticket.find(params[:id])
    end

    def processClosingAdmin
        @ticket = Ticket.find(params[:id])
         #notif email proses tiket
    end

    def createSatisfaction
        unless params[:satisfaction_note].present?
            Ticket.update(params[:ticket_id], satisfaction: params[:satisfaction])
        else
            Ticket.update(params[:ticket_id], {satisfaction: params[:satisfaction], satisfaction_note: params[:satisfaction_note]})
        end
        redirect_back(fallback_location: root_path, notice: "Satisfaction has confirmed")
    end

    def createSatisfactionEskalasi
        unless params[:satisfaction_note].present?
            TicketProblem.update(params[:ticket_id], satisfaction: params[:satisfaction])
        else
            TicketProblem.update(params[:ticket_id], {satisfaction: params[:satisfaction], satisfaction_note: params[:satisfaction_note]})
        end
        redirect_back(fallback_location: root_path, notice: "Satisfaction Eskalasi has confirmed")
    end

    def createProblem
        begin
            @ticket = Ticket.find(params[:id])
        
            # notif eskalasi
            TicketHistory.create(
                ticket_id: params[:id],
                no_tiket: @ticket.no_tiket,
                waktu: Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                inputer: @ticket.nama_pemohon,
                status_tiket: "Problem",
                issued_by: current_user.name
            )

            @ticket_problem = TicketProblem.create(
            ticket_id: params[:id],
            status: "Processing Asign"
            )
            
            # id_length = @ticket_problem.id.to_s.length
            # @ticket_problem.update(no_ticket_problem: "#{Time.now.strftime('%Y%m%d')}#{@ticket_problem.id}")
            id_length = @ticket_problem.id.to_s.length
            current_date = Time.now.strftime('%Y%m%d')

            last_ticket_problem = TicketProblem.lock.last
            if last_ticket_problem.present?
            last_ticket_problem_number = last_ticket_problem.no_ticket_problem.to_i
            next_ticket_problem_number = last_ticket_problem_number + 1
            else
            next_ticket_problem_number = 1
            end

            formatted_ticket_problem_number = next_ticket_problem_number.to_s.rjust(3, '0')
            new_no_ticket_problem = "#{current_date}#{formatted_ticket_problem_number}"

            existing_ticket_problems = TicketProblem.where("no_ticket_problem LIKE ?", "#{current_date}%")
            existing_ticket_problem_numbers = existing_ticket_problems.pluck(:no_ticket_problem).map { |t| t[current_date.length..-1].to_i }

            while existing_ticket_problem_numbers.include?(next_ticket_problem_number)
            next_ticket_problem_number += 1
            formatted_ticket_problem_number = next_ticket_problem_number.to_s.rjust(3, '0')
            new_no_ticket_problem = "#{current_date}#{formatted_ticket_problem_number}"
            end

            @ticket_problem.update(no_ticket_problem: new_no_ticket_problem)


            if @ticket.status_tiket == "Closed"
                Ticket.update(params[:id], status_tiket: "Closed", :keterangan => "Tiket di Esklasi dengan Tiket baru")
            else
                # notif tiket utama closed
                @time_now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
                TicketHistory.create(
                    :ticket_id => params[:id],
                    :no_tiket => @ticket.no_tiket,
                    :waktu => @time_now,
                    :inputer => @ticket.nama_pemohon,
                    :status_tiket => "Closed",
                    :keterangan => "Tiket di Esklasi dengan Tiket baru",
                    :issued_by => current_user.name
                )
                @resolution_time = distance_of_time_in_words(@time_now, @ticket.created_at, true, :only => [:weeks, :days, :hours, :minutes])
                Ticket.update(params[:id], {:status_tiket => "Closed", :status_teknisi => "Available", :resolution_time => @resolution_time,  :keterangan => "Tiket di Esklasi dengan Tiket baru"})
            end
            
            redirect_to tickets_path, notice: "Ticket has been confirmed as a problem"
        rescue StandardError => e
            respond_to do |format|
              format.html { redirect_to tickets_path, alert: "Failed: #{e.message}" }
              format.json { head :unprocessable_entity }
            end
        end
      end
      

    def createClosing
        @ticket = Ticket.find(params[:ticket_id])
        @time_now = Time.now.strftime("%Y-%m-%d %H:%M:%S")
        TicketHistory.create(
            :ticket_id => params[:ticket_id],
            :no_tiket => @ticket.no_tiket,
            :waktu => @time_now,
            :inputer => @ticket.nama_pemohon,
            :status_tiket => params[:status_ticket],
            :keterangan => params[:keterangan],
            :issued_by => current_user.name
        )
        @resolution_time = distance_of_time_in_words(@time_now, @ticket.created_at, true, :only => [:weeks, :days, :hours, :minutes])
        Ticket.update(params[:ticket_id], {:status_tiket => params[:status_ticket], :status_teknisi => "Available", :resolution_time => @resolution_time})
        redirect_to tickets_path, notice: "Ticket has confirmed to " + params[:status_ticket]

        # notif email
         @ticket_resolution_time = @ticket.try(:resolution_time) || @resolution_time
         @open_url = "https://helpdesk.pgn-solution.co.id/tickets/#{@ticket.id}"
         @subject = "Tiket #{@ticket.no_tiket} telah Closed"
         @email_body = "Tiket Anda telah Closed.Mohon berikan rating untuk layanan yang telah diberikan. Informasi tambahan sebagai berikut:" +
         "<br>" +
         "No Tiket: "+@ticket.try(:no_tiket)+"<br>"+
         "Issued by: "+@ticket.try(:nama_pemilik_kendala)+"<br>"+
         "Inputer: "+@ticket.try(:nama_pemohon)+"<br>"+
         "Resolution Time: "+@ticket_resolution_time+"<br>"+
         # "Catatan: "+"Silakan berikan penilaian  Tiket Sesuai Ketentuan"+"<br>"+   
         "<br>Terimakasih telah menggunakan layanan Helpdesk, permintaan Bapak/Ibu sudah terselesaikan, harap ketersediaannya untuk memberikan penilaian pada bagian satisfaction."+"<br>"+        
      
         "<br>Salam,<br>"+
         "<b>HELP DESK<b><br><br><br>"+
         "<a href='"+@open_url+"' target='_blank'>Klik untuk melihat Detail Tiket dan berikan penilaian satisfaction.</a>"
         # @email_to = RoleAssignment.joins(:user).where("role_id = ?", 4).pluck("users.email")
         # menggabungkan email dari dua query yang berbeda
         emails = RoleAssignment.joins(:user).where("lower(regexp_replace(users.username, '[^\\w]+', '', 'g')) = ?", @ticket.try(:nama_pemilik_kendala).to_s.downcase.gsub(/[^\w]/, '')).pluck("users.email") 

         # menghapus duplikat email yang dihasilkan
         if emails.present?
            # menghapus duplikat email yang dihasilkan
            @email_to = emails.uniq
            begin
              TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
            rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
              logger.warn mailing_lists_error
              flash.discard[:notice] = "There was a problem with sending to destination email. Please give information to the SIT " + "#{mailing_lists_error}"
              return redirect_to root_path
            end
          else
            logger.warn "No recipient email found for ticket #{@ticket.no_tiket}"
          end
          
    end

    def createFollowupTechnician
        @ticket = Ticket.find(params[:ticket_id])
        TicketHistory.create(
            :ticket_id => params[:ticket_id],
            :no_tiket => @ticket.no_tiket,
            :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            :inputer => @ticket.nama_pemohon,
            :status_tiket => params[:status_ticket],
            :keterangan => params[:keterangan],
            :issued_by => current_user.name,
            :work_around => params[:work_around]
        )
        Change.create(
            :tanggal => Time.now.strftime("%Y-%m-%d"),
            :judul => params[:judul],
            :teknisi => current_user.name,
            :kategori_asset => params[:kategori_asset],
            :nama_perangkat => params[:nama_perangkat],
            :nama_user =>  @ticket.try(:nama_pemilik_kendala),
            :nama_aplikasi => params[:nama_aplikasi],
            :pemilik_proses => params[:pemilik_proses],
            :deskripsi => params[:keterangan],
            :ticket_id => params[:ticket_id]
        )
        Ticket.update(params[:ticket_id], :status_tiket => params[:status_ticket])
         #notif email proses tiket
        redirect_to tickets_path, notice: "Ticket has confirmed to " + params[:status_ticket]
    end

    def confirmWaiting #Technician
        @ticket = Ticket.find(params[:id])
        @getCategory = Category.find_by_name(@ticket.kategori)
        Ticket.update(params[:id], :status_tiket => "On Processing")
        # :status_tiket => @getCategory.status_template_tiket)
        TicketHistory.create(
            :ticket_id => params[:id],
            :no_tiket => @ticket.no_tiket,
            :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
            :inputer => @ticket.nama_pemohon,
            :status_tiket => "On Processing",
            #  @getCategory.status_template_tiket,
            :issued_by => current_user.name
        )

        #notif email proses tiket
        redirect_to tickets_path, notice: "Ticket has confirmed to " 
        # + @getCategory.status_template_tiket
    end

    def createPriority

    end

    # def followup
    #     @ticket = Ticket.find(params[:ticket_id])
    #     Change.create(
    #         :tanggal => Time.now.strftime("%Y-%m-%d"),
    #         :judul => params[:judul],
    #         :teknisi => current_user.name,
    #         :kategori_asset => params[:kategori_asset],
    #         :nama_perangkat => params[:nama_perangkat],
    #         :nama_user => params[:nama_user],
    #         :nama_aplikasi => params[:nama_aplikasi],
    #         :pemilik_proses => params[:pemilik_proses],
    #         :deskripsi => params[:keterangan],
    #     )
    #     Ticket.update(params[:ticket_id], :status_tiket => params[:status_ticket], :catatan_status => params[:catatan_status])
    #     redirect_to tickets_path, notice: "Ticket has Follow Up to " + params[:status_ticket]
    # end
end