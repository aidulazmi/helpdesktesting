class MessageTemplatesController < ApplicationController
  before_action :set_message_template, only: %i[ show edit update destroy ]

  # GET /message_templates or /message_templates.json
  def index
    @message_templates = MessageTemplate.all
  end

  # GET /message_templates/1 or /message_templates/1.json
  def show
  end

  # GET /message_templates/new
  def new
    @message_template = MessageTemplate.new
  end

  # GET /message_templates/1/edit
  def edit
  end

  # POST /message_templates or /message_templates.json
  def create
    @message_template = MessageTemplate.new(message_template_params)

    respond_to do |format|
      if @message_template.save
        format.html { redirect_to message_template_url(@message_template), notice: "Message template was successfully created." }
        format.json { render :show, status: :created, location: @message_template }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @message_template.errors, status: :unprocessable_entity }
      end
    end
    
  end

  # PATCH/PUT /message_templates/1 or /message_templates/1.json
  def update
    respond_to do |format|
      if @message_template.update(message_template_params)
        format.html { redirect_to message_template_url(@message_template), notice: "Message template was successfully updated." }
        format.json { render :show, status: :ok, location: @message_template }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @message_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /message_templates/1 or /message_templates/1.json
  def destroy
    @message_template.destroy

    respond_to do |format|
      format.html { redirect_to message_templates_url, notice: "Message template was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message_template
      @message_template = MessageTemplate.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def message_template_params
      params.require(:message_template).permit(:name, :template, :sub_category, :jenis_layanan, :kategori, :approvals)
    end
end
