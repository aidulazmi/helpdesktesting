class TicketApprovalsController < ApplicationController
  before_action :set_ticket_approval, only: %i[ show edit update destroy ]

  # GET /ticket_approvals or /ticket_approvals.json
  def index
    @ticket_approvals =  TicketApproval.where(user_id: current_user.id).where.not(confirm_admin: [nil, false])
    @ticket = Ticket.all
    @kategori = @ticket.select(:kategori).map(&:kategori).uniq
    @status = @ticket.select(:status_tiket).map(&:status_tiket).uniq    
  end

  # GET /ticket_approvals/1 or /ticket_approvals/1.json
  def show
  end

  # GET /ticket_approvals/new
  def new
    @ticket_approval = TicketApproval.new
  end

  # GET /ticket_approvals/1/edit
  def edit
  end

  # POST /ticket_approvals or /ticket_approvals.json
  def create
    @ticket_approval = TicketApproval.new(ticket_approval_params)

    respond_to do |format|
      if @ticket_approval.save
        format.html { redirect_to ticket_approval_url(@ticket_approval), notice: "Ticket approval was successfully created." }
        format.json { render :show, status: :created, location: @ticket_approval }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @ticket_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ticket_approvals/1 or /ticket_approvals/1.json
  def update
    respond_to do |format|
      if @ticket_approval.update(ticket_approval_params)
        format.html { redirect_to ticket_approval_url(@ticket_approval), notice: "Ticket approval was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket_approval }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  def approve
   
    @ticket_approval = TicketApproval.find(params[:id])
    respond_to do |format|
      if  @ticket_approval.present?
        @ticket_approval.update(
          status: "Approved",
          catatan: "Silakan Proses Tiket Sesuai Ketentuan"
        )
        @ticket = Ticket.find(@ticket_approval.ticket_id )
        @ticket.update(
          :status_tiket => "Approved"
        )

        #notif apps
        TicketHistory.create(
          :ticket_id => @ticket_approval.ticket.id,
          :no_tiket => @ticket_approval.ticket.no_tiket,
          :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
          :inputer => @ticket_approval.ticket.nama_pemohon,
          :status_tiket => "Approved", #followup
          :issued_by => current_user.name,
          :keterangan => "Silakan Proses Tiket Sesuai Ketentuan"
        )

        # notif email pemilik tiket + admin + teknisi
        # @open_url ="http://192.168.22.55:3003/tickets/#{@ticket_approval.ticket.id}"
        # @subject = "Tiket #{@ticket_approval.ticket.id} telah diapprove"
        # @email_body = "Telah diapprove tiket oleh #{@ticket_approval.user.name}.Dengan informasi sebagai berikut:" +
        # "<br>" +
        # "Inputer: "+@ticket_approval.ticket.try(:nama_pemohon)+"<br>"+
        # "Issued_by: "+@ticket_approval.ticket.try(:nama_pemilik_kendala)+"<br>"+
        # "Catatan: "+"Silakan Proses Tiket Sesuai Ketentuan"+"<br>"+         
        # "<br>Salam,<br>"+
        # "<b>HELP DESK<b><br><br><br>"+
        # "<a href='"+@open_url+"' target='_blank'>Klik untuk membuka Detail Tiket</a>"
        # @email_to = RoleAssignment.joins(:user).where("role_id = ?", 4).pluck("users.email")
        # begin
        #   TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
        # rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
        #   logger.warn mailing_lists_error
        #   flash.discard[:notice] = "There was a problem with sending to destination email.  Please give information to the SIT " + "#{mailing_lists_error}"
        #   return redirect_to root_path
        # end

        format.html { redirect_to ticket_approvals_url, notice: "Ticket Approved was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket_approval }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  def reject
    @ticket_approval = TicketApproval.find(params[:id])
   
    respond_to do |format|
      if  @ticket_approval.present?
        @ticket_approval.update(
          status: "Reject",
          catatan: params[:catatan]
        )
        @ticket = Ticket.find(@ticket_approval.ticket_id )
        @ticket.update(
          :status_tiket => "Reject"
        )

        #notif apps
        TicketHistory.create(
                  :ticket_id => @ticket_approval.ticket.id,
                  :no_tiket => @ticket_approval.ticket.no_tiket,
                  :waktu => Time.now.strftime("%Y-%m-%d %H:%M:%S"),
                  :inputer => @ticket_approval.ticket.nama_pemohon,
                  :status_tiket => "Reject", #followup
                  :issued_by => current_user.name,
                  :keterangan => params[:catatan]
        )
        
        #notif email
         # notif email pemilik tiket + admin + teknisi
        #  @open_url ="http://192.168.22.55:3003/tickets/#{@ticket_approval.ticket.id}"
        #  @subject = "Tiket #{@ticket_approval.ticket.id} telah direject"
        #  @email_body = "Telah direject tiket oleh #{@ticket_approval.user.name}.Dengan informasi sebagai berikut:" +
        #  "<br>" +
        #  "Inputer: "+@ticket_approval.ticket.try(:nama_pemohon)+"<br>"+
        #  "Issued_by: "+@ticket_approval.ticket.try(:nama_pemilik_kendala)+"<br>"+
        #  "Catatan: "+"#{@ticket_approval.try(:catatan)}"+"<br>"+       
        #  "<br>Salam,<br>"+
        #  "<b>HELP DESK<b><br><br><br>"+
        #  "<a href='"+@open_url+"' target='_blank'>Klik untuk membuka Detail Tiket</a>"
        #  @email_to = RoleAssignment.joins(:user).where("role_id = ?", 4).pluck("users.email")
        #  begin
        #    TicketMailer.send_email(@email_to, @subject, @email_body).deliver_now!
        #  rescue Net::SMTPFatalError, Net::SMTPSyntaxError, Net::SMTPAuthenticationError => mailing_lists_error
        #    logger.warn mailing_lists_error
        #    flash.discard[:notice] = "There was a problem with sending to destination email.  Please give information to the SIT " + "#{mailing_lists_error}"
        #    return redirect_to root_path
        #  end

        format.html { redirect_to ticket_approvals_url, alert: "Ticket Reject was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket_approval }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket_approval.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ticket_approvals/1 or /ticket_approvals/1.json
  def destroy
    @ticket_approval.destroy

    respond_to do |format|
      format.html { redirect_to ticket_approvals_url, notice: "Ticket approval was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket_approval
      @ticket_approval = TicketApproval.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def ticket_approval_params
      params.require(:ticket_approval).permit(:ticket_id, :user_id, :status, :catatan)
    end
end
