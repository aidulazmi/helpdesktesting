class CollectionAssetsController < ApplicationController
  before_action :set_collection_asset, only: %i[ show edit update destroy ]
  load_and_authorize_resource
  respond_to :html, :json
  
  # GET /assets or /assets.json
  def index
    @collection_assets = CollectionAsset.all
  end

  # GET /assets/1 or /assets/1.json
  def show
  end

  # GET /assets/new
  def new
    @collection_asset = CollectionAsset.new
  end

  # GET /assets/1/edit
  def edit
  end

  # POST /assets or /assets.json
  def create
    @collection_asset = CollectionAsset.new(collection_asset_params)

    respond_to do |format|
      if @collection_asset.save
        format.html { redirect_to collection_assets_path, notice: "Asset was successfully created." }
        format.json { render :show, status: :created, location: @collection_asset }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @collection_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /assets/1 or /assets/1.json
  def update
    respond_to do |format|
      if @collection_asset.update(collection_asset_params)
        format.html { redirect_to collection_asset_path(params[:id]), notice: "Asset was successfully updated." }
        format.json { render :show, status: :ok, location: @collection_asset }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @collection_asset.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /assets/1 or /assets/1.json
  def destroy
    @collection_asset.destroy

    respond_to do |format|
      format.html { redirect_to collection_assets_url, notice: "Asset was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_collection_asset
      @collection_asset = CollectionAsset.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def collection_asset_params
      params.require(:collection_asset).permit(:device_name, :jenis_layanan)
    end
end
