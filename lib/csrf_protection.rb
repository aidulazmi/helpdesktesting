# lib/csrf_protection.rb
module CsrfProtection
  extend ActiveSupport::Concern

  included do
    protect_from_forgery with: :exception
  end
end
