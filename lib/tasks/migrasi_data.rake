namespace :migrasi_data do
    desc 'Migrasi Data'
    task :set_department => [:environment] do
        Division.new.set_department
    end
    task :set_divisi => [:environment] do
        Division.new.set_divisi
    end
    task :set_direktorat => [:environment] do
        Division.new.set_direktorat
    end
    task :set_area => [:environment] do
        Area.new.set_area
    end
    task :set_auto_eskalasi => [:environment] do
        TicketHistory.new.autoEskalasi
    end
    task :set_auto_closed => [:environment] do
        TicketHistory.new.autoClosed
    end
    task :set_auto_satisfaction => [:environment] do
        TicketHistory.new.autoSatisfaction
    end
    
end