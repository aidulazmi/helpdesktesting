require 'net/ldap'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class LdapAuthenticatable < Authenticatable
      def authenticate!
        if params[:user]
          # begin
          
            ldap = Net::LDAP.new
            ldap.host = '192.168.60.159'
            ldap.port = 389
            # password_hash = BCrypt::Password.create(password)
            # ldap.auth 'uid=' + CGI.escapeHTML(username.gsub(/['"\\\x0]/, '\\\\\0').gsub("\\", "").downcase) + ', ou=people, dc=pgn-solution, dc=co, dc=id', CGI.escapeHTML(password)
            ldap.auth 'uid=' + username.gsub(/['"\\\x0]/, '\\\\\0').gsub("\\", "").downcase + ', ou=people, dc=pgn-solution, dc=co, dc=id', password
            if ldap.bind
              getUserExists = User.find_by_username(username.downcase)
              unless getUserExists.nil?
                user = User.find_or_create_by(username: username.downcase) do |user|
                  user.password = password
                end
              else
                user = User.find_or_create_by(username: username.downcase) do |user|
                  user.password = password
                  user.email = username + "@pgn-solution.co.id"
                  user.user_type = "LDAP"
                  user.name = username.gsub(".", " ").upcase
                  user.role_ids = 2
                end
              end
              success!(user)
            else
              ldapMagang = Net::LDAP.new
              ldapMagang.host = '192.168.60.159'
              ldapMagang.port = 389
              ldapMagang.auth 'uid=' + CGI.escapeHTML(username.gsub(/['"\\\x0]/, '\\\\\0').gsub("\\", "").downcase) + ', ou=magang, dc=pgn-solution, dc=co, dc=id', CGI.escapeHTML(password)
              if ldapMagang.bind
                getUserExists = User.find_by_username(username.downcase)
                unless getUserExists.nil?
                  user = User.find_or_create_by(username: username.downcase) do |user|
                    user.password = password
                  end
                else
                  user = User.find_or_create_by(username: username.downcase) do |user|
                    user.password = password
                    user.email = username + "@pgn-solution.co.id"
                    user.user_type = "LDAP"
                    user.name = username.gsub(".", " ").upcase
                    user.role_ids = 2
                  end
                end
                success!(user)
              else
                return fail(:invalid_login)
              end
            end
          # rescue => e
          #   return fail(:invalid_login)
          # end
        end
      end

      private

      def decryptAES(encrypted_data, cipher_type="AES-256-CBC")
        @decode = Base64.decode64(encrypted_data)
        cipher = OpenSSL::Cipher::Cipher.new(cipher_type)
        cipher.decrypt
        cipher.key = "1e4fedbbf9cdacaaf1213eb0f99d18af"
    
        cipher.iv = "f1213eb0f99d18af" if "f1213eb0f99d18af" != nil
        cipher.update(@decode) << cipher.final
      end
  
      
      def username
         decryptAES(params['user']['username'])
        # params[:user][:username]
      end

      def password
         decryptAES(params['user']['password'])   
        # params[:user][:password]
      end

    end
  end
end

Warden::Strategies.add(:ldap_authenticatable, Devise::Strategies::LdapAuthenticatable)