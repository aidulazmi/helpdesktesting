Myapp::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.

 #
  # config.action_cable.allowed_request_origins = ['http://192.168.60.175/']

  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations
  config.active_record.migration_error = :page_load

  #
  Rails.application.config.active_storage.service_limits = { max_file_size: 500.megabytes }

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = false
  config.assets.check_precompiled_asset = false
  config.active_storage.service = :local
  config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.perform_caching = false

  config.action_mailer.delivery_method = :smtp
  host = 'https://192.168.22.79/' #replace with your own url
  config.action_mailer.default_url_options = { host: host }
  

  # SMTP settings for gmail
  # config.action_mailer.smtp_settings = {
  #   :address              => "smtp.gmail.com",
  #   :port                 => 587,
  #   :user_name            => 'irvan6500@gmail.com',
  #   :password             => 'THA721TC85+@',
  #   :authentication       => "plain",
  #   :enable_starttls_auto => true
  # }


  # config.action_mailer.smtp_settings = {
  #   address:              'smtp.gmail.com',
  #   port:                 587,
  #   domain:               'gmail.com',
  #   :user_name            => 'irvan6500@gmail.com',
  #   :password             => 'wwbdwxjndgksjtjz',
  #   authentication:       'plain',
  #   # enable_starttls_auto: true
  # }

  config.action_mailer.smtp_settings = {
    address:              'mail.pgn-solution.co.id',
    port:                 465,
    domain:               'pgn-solution.co.id',
    :user_name            => 'irvan.hilmi@pgn-solution.co.id',
    :password             => 'Tha721tc85',
    tls:                   true,
    authentication:       :plain,
    enable_starttls_auto: true  }


  #payload
  

  # #rack-attack
  # config.middleware.use Rack::Attack
  
end
