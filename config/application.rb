require File.expand_path('../boot', __FILE__)
require 'action_cable'
require 'action_cable/engine'
require 'action_cable/connection/subscriptions'


require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)


module Myapp
  class Application < Rails::Application

    $request_count = {}
    config.exceptions_app = self.routes

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.exceptions_app = self.routes
    config.action_cable.mount_path = '/cable'
    
    # config.middleware.use ActionCable::ConnectionManagement


    # config.middleware.use Rack::Limiter, max: 10, period: 60
    #xss
    xss1 = config.action_view.sanitized_allowed_tags = ['strong', 'em', 'a']
    xss2 = config.action_view.sanitized_allowed_attributes = ['href', 'title']
    # rack-attack
    # config.middleware.use Rack::Attack
    Rack::Attack.throttle('req/ip', limit: 5, period: 1.minute) do |req|
      req.ip # Return the client IP address
    end

    class Rack::Attack
      Rack::Attack.blocklist("block /rails/info/properties") do |req|
        req.path.include?("/rails/info/properties")
      end
      
      Rack::Attack.blocklist("block /rails/info/routes") do |req|
        req.path.include?("/rails/info/routes")
      end
    end

    Rack::Attack.blocklisted_response = lambda do |env|
      [ 404,  # status
        {},   # headers
        ['']] # body
    end


    #header secure
    SecureHeaders::Configuration.default do |config| 
      config.csp = { 
        default_src:  %w('self' http://192.168.60.175/) , 
        img_src:  %w('self' http://192.168.60.175/) , 
        script_src:  %w('self' http://192.168.60.175/) , 
        style_src:  %w('self' http://192.168.60.175/ 'unsafe-inline') , 
        upgrade_insecure_requests:  true
       } 
    end

    config.assets.precompile += [ 'appviews.css', 'cssanimations.css', 'dashboards.css', 'forms.css', 'gallery.css', 'graphs.css', 'mailbox.css', 'miscellaneous.css', 'pages.css', 'tables.css', 'uielements.css', 'widgets.css', 'star-rating.css','bootsrap.css', ]
    config.assets.precompile += [ 'appviews.js', 'cssanimations.js', 'dashboards.js', 'forms.js', 'gallery.js', 'graphs.js', 'mailbox.js', 'miscellaneous.js', 'pages.js', 'tables.js', 'uielements.js', 'widgets.js', 'star-rating.js', 'bootsrap.js','jquery.min.js', ]
    config.time_zone = 'Asia/Jakarta'
    config.active_record.default_timezone = :local # Or :utc
    config.autoload_paths << Rails.root.join('lib')
  
  end
end
