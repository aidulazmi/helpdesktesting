Myapp::Application.routes.draw do

  resources :feature_assignments
  resources :features
  get 'landing/set_role'

  mount ActionCable.server => '/cable'
  resources :ticket_approvals do
    member do
      post 'approvals', to: "ticket_approvals#approve"
      post 'reject', to: "ticket_approvals#reject"
      # post 'ticket_approvals/reject'
      # post 'ticket_approvals/approve'
    end
  end  
  resources :ticket_problems
  resources :message_templates
  resources :assetlisensis
  resources :assetssystems
  resources :changes do
    member do 
      delete "delete_file/:attachment_id", to: 'changes#delete_file'  
    end
  end
  resources :process_tickets do
    member do
      get "processTechnician", to: "process_tickets#processTechnician"
      get "processClosingAdmin", to: "process_tickets#processClosingAdmin"
      get "confirmWaiting", to: "process_tickets#confirmWaiting"
      get "createProblem", to: "process_tickets#createProblem"
      get "createPriority", to: "process_tickets#createPriority"
    end
  end
  post 'process_tickets/createFollowupTechnician'
  post 'process_tickets/followup'
  post 'process_tickets/createClosing'
  post 'process_tickets/createSatisfaction'
  post 'process_tickets/createSatisfactionEskalasi'
  post 'users/updateProfileForSelfService'
  resources :loss_events
  resources :positions
  # post '/update_session', to: 'helper_json#update_session_feature'
  post '/update_session_feature', to: 'helper_json#update_session_feature'


  resources :helper_json do
    collection do

      get "/getTemplateTiket", to: "helper_json#getTemplateTiket"
    end
    member do
      
      get 'destroy_chat', to: 'helper_json#destroy_chat', as: 'ticket_destroy_chat'

      get "getTemplatesByJenisLayanan", to: "helper_json#getTemplatesByJenisLayanan"
      get "getTemplatesBySubKategori", to: "helper_json#getTemplatesBySubKategori"

      get "getTemplatesByKategori/:kategori_id", to: "helper_json#getTemplatesByKategori"
      get "getTemplatesInAdminByKategori/:kategori_id", to: "helper_json#getTemplatesInAdminByKategori"
      

      get "getTemplates/:jenis_layanan", to: "helper_json#getTemplates"

      get "getSubKategori", to: "helper_json#getSubKategori"
      get "getSubKategori2", to: "helper_json#getSubKategori2"
      get "getPriority", to: "helper_json#getPriority"
      get "getStatusTeknisi", to: "helper_json#getStatusTeknisi"
      get "switchRole", to: "helper_json#switchRole"
      get "getTeknisi", to: "helper_json#getTeknisi"
      get "getPic", to: "helper_json#getPic"
      get "getStatus", to: "helper_json#getPic"
      get "getMerkPerangkat", to: "helper_json#getMerkPerangkat"
      get "getTipePerangkat", to: "helper_json#getTipePerangkat"
      get "getSubLayanan", to: "helper_json#getSubLayanan"
      get "getPerintilan", to: "helper_json#getPerintilan"
      get "getLayanan", to: "helper_json#getLayanan"
    end
  end
  resources :tickets do
    # get "notification", to: "tickets#read_ticket"


    collection do
      get 'cetak_pdf', to: "tickets#cetak_pdf"
      get 'cetak_excel', to: "tickets#cetak_excel"
      get 'detail_notif', to: "tickets#detail_notif"
      
    end
    member do
      get "superadminEdit", to: "tickets#superadminEdit"
      get "follow_up", to: "tickets#follow_up"
      get "answer", to: "tickets#answer"
      get "notification", to: "tickets#read_ticket"
      get 'chat_notif/:ticket_history', to: 'tickets#read_ticket_chat', as: 'chat_notif_ticket'      
      get "read_all_notifications/:ticket_history", to: "tickets#read_all_notifications", as: "read_all_notifications"
    end

  end
  resources :collection_assets
  resources :categories do
    member do 
      delete "delete_file/:attachment_id", to: 'categories#delete_file'   
    end
  end
  resources :service_types
  resources :assetsdigitals do
    collection do 
      post :import
    end
  end

  resources :assetsusers do
    member do 
      delete "delete_file/:attachment_id", to: 'assetsusers#delete_file'   
    end
    collection do
      post :import
        get 'cetak_pdf', to: "assetsusers#cetak_pdf"
        get 'cetak_excel', to: "assetsusers#cetak_excel"
    end
  end
  resources :assetssits
  # You can have the root of your site routed with "root"
  root to: 'dashboards#home'


  devise_for :users, path: '', path_names: { sign_in: 'login_page', sign_out: 'logout'}, controllers:{
    sessions: 'users/sessions'
  }
  resources :areas
  resources :roles
  resources :users

  # get ":category_id/", to: "dashboards#home"


  #sub_category
resources :dashboards do
  get "/insiden/", to: "dashboards#insiden_layanan"
  get "/request/", to: "dashboards#request_layanan"

end


  # All routes
  get "dashboards/dashboard_1"
  get "dashboards/dashboard_2"
  get "dashboards/dashboard_3"
  get "dashboards/dashboard_4"
  get "dashboards/dashboard_4_1"
  get "dashboards/home"

  get "layoutsoptions/index"

  get "layoutsoptions/off_canvas"

  get "graphs/flot"
  get "graphs/morris"
  get "graphs/rickshaw"
  get "graphs/chartjs"
  get "graphs/peity"
  get "graphs/sparkline"

  get "mailbox/inbox"
  get "mailbox/email_view"
  get "mailbox/compose_email"
  get "mailbox/email_templates"
  get "mailbox/basic_action_email"
  get "mailbox/alert_email"
  get "mailbox/billing_email"

  get "widgets/index"

  get "forms/basic_forms"
  get "forms/advanced"
  get "forms/wizard"
  get "forms/file_upload"
  get "forms/text_editor"

  get "appviews/contacts"
  get "appviews/profile"
  get "appviews/projects"
  get "appviews/project_detail"
  get "appviews/file_menager"
  get "appviews/calendar"
  get "appviews/faq"
  get "appviews/timeline"
  get "appviews/pin_board"
  get "appviews/teams_board"
  get "appviews/clients"
  get "appviews/outlook_view"
  get "appviews/blog"
  get "appviews/article"
  get "appviews/issue_tracker"

  get "pages/search_results"
  get "pages/lockscreen"
  get "pages/invoice"
  get "pages/invoice_print"
  get "pages/login"
  get "pages/login_2"
  get "pages/forgot_password"
  get "pages/register"
  get "pages/not_found_error"
  get "pages/internal_server_error"
  get "pages/empty_page"

  get "miscellaneous/notification"
  get "miscellaneous/nestablelist"
  get "miscellaneous/timeline_second_version"
  get "miscellaneous/forum_view"
  get "miscellaneous/forum_post_view"
  get "miscellaneous/google_maps"
  get "miscellaneous/code_editor"
  get "miscellaneous/modal_window"
  get "miscellaneous/validation"
  get "miscellaneous/tree_view"
  get "miscellaneous/chat_view"
  get "miscellaneous/agile_board"
  get "miscellaneous/diff"
  get "miscellaneous/idle_timer"
  get "miscellaneous/spinners"
  get "miscellaneous/live_favicon"

  get "uielements/typography"
  get "uielements/icons"
  get "uielements/draggable_panels"
  get "uielements/buttons"
  get "uielements/video"
  get "uielements/tables_panels"
  get "uielements/notifications_tooltips"
  get "uielements/badges_labels_progress"

  get "gridoptions/index"

  get "tables/static_tables"
  get "tables/data_tables"
  get "tables/jqgrid"

  get "gallery/basic_gallery"
  get "gallery/bootstrap_carusela"

  get "cssanimations/index"

  get "landing/index"


  mount RailsPerformance::Engine, at: 'rails/performance'


  # match '*path', via: :all, to: 'pages#not_found_error', constraints: lambda { |req|
  #   req.path.exclude? 'rails/active_storage'
  # }
  # get '/500', to: 'errors#internal_server_error'
  # get '/404', to: 'errors#not_found'
  # get '/422', to: 'errors#unprocessable_entity'
  
  # match '/500', via: :all, to: 'pages#internal_server_error'
  # match '/page_cannot_be_found', via: :all, to: 'pages#page_cannot_be_found'

end
