# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(
    email: 'superadmin@example.org',
    password: '123456',
    username: 'superadmin',
    name: 'Superadmin'
)

Position.create(
    [
        {nama: 'Direksi', priority: 'High'},
        {nama: 'Kadiv', priority: 'Medium'},
        {nama: 'Kadep', priority: 'Medium'},
        {nama: 'Staff', priority: 'High'},
    ]
)

StatusTicket.create(
    [
        {nama: 'Processing Assign'},
        {nama: 'Processing Plan'},
        {nama: 'Waiting'},
        {nama: 'Problem'},
        {nama: 'Closed'},
    ]
)

Role.create(
    [
        {   name: 'Superadmin', 
            permissions: '
            ---
            :user:
            :manage: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            :read_public: true
            :role:
            :manage: true
            :index: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            :service_type:
            :manage: true
            :index: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            :category:
            :manage: true
            :index: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            :ticket:
            :manage: true
            :index: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            :collection_asset:
            :manage: true
            :index: true
            :create: true
            :destroy: true
            :update: true
            :read: true
            ', 
            type: "Role" 
        },
        
        {   name: 'Self-Service', 
            permissions:'
            ---
            :user:
            :manage: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :read_public: false
            :role:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :service_type:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :category:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :ticket:
            :manage: false
            :getSubKategori: true
            :index: false
            :create: true
            :destroy: true
            :update: true
            :read: true
            :asset:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            ', 
            type: "Role" 
        },
        
        {   name: 'Teknisi', 
            permissions:'
            ---
            :user:
            :manage: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :read_public: false
            :role:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :service_type:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :category:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            :ticket:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: true
            :read: true
            :asset:
            :manage: false
            :index: false
            :create: false
            :destroy: false
            :update: false
            :read: true
            ', 
            type: "Role" 
        },
    ]
)

RoleAssignment.create(
    user_id: 1,
    role_id: 1
)