class AddColumnTicketHistoryIdToNotificationReadTickets < ActiveRecord::Migration[5.2]
  def change
    add_reference :notification_read_tickets, :ticket_history, foreign_key: true
  end
end
