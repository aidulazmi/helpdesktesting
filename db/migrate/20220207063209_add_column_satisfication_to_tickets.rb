class AddColumnSatisficationToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :satisfaction, :integer
  end
end
