class AddColumnResolutionTimeToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :resolution_time, :string
  end
end
