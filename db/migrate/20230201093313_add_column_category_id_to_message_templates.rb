class AddColumnCategoryIdToMessageTemplates < ActiveRecord::Migration[5.2]
  def change
    add_reference :message_templates, :category, foreign_key: true
  end
end
