class AddColumnPriorityToPositions < ActiveRecord::Migration[5.2]
  def change
    add_column :positions, :priority, :string
  end
end
