class CreateFeatureAssignments < ActiveRecord::Migration[5.2]
  def change
    create_table :feature_assignments do |t|
      t.references :role, foreign_key: true
      t.references :feature, foreign_key: true

      t.timestamps
    end
  end
end
