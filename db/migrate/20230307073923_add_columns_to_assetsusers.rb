class AddColumnsToAssetsusers < ActiveRecord::Migration[5.2]
  def change
    add_column :assetsusers, :status_pekerja, :string
    add_column :assetsusers, :email, :string
    add_column :assetsusers, :no_hp, :string
    add_column :assetsusers, :status_laptop, :string
    add_column :assetsusers, :periode_laptop, :string
    add_column :assetsusers, :spesifikasi, :string
    add_column :assetsusers, :no_hbb, :string
    add_column :assetsusers, :no_bast, :string
  end
end
