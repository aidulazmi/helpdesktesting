class AddColumnCatatanStatusToTicketProblems < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_problems, :catatan_status, :string
  end
end
