class CreateLossEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :loss_events do |t|
      t.string :nama
      t.date :tanggal
      t.string :tempat
      t.string :status
      t.string :pic
      t.string :deskripsi_kejadian
      t.string :kategori

      t.timestamps
    end
  end
end
