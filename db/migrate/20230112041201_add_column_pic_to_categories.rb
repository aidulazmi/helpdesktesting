class AddColumnPicToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :pic, :string
  end
end
