class AddColumnApprovedToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :approved, :string
  end
end
