class CreateTicketProblems < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_problems do |t|
      t.references :ticket, foreign_key: true
      t.string :no_ticket_problem
      t.string :status
      t.string :teknisi
      t.string :priority
      t.string :kategori
      t.string :sub_kategori
      t.string :keterangan

      t.timestamps
    end
  end
end
