class CreateChanges < ActiveRecord::Migration[5.2]
  def change
    create_table :changes do |t|
      t.date :tanggal
      t.string :judul
      t.string :teknisi
      t.string :kategori_asset
      t.string :nama_perangkat
      t.string :ram
      t.string :nama_aplikasi
      t.string :pemilik_proses
      t.text :deskripsi

      t.timestamps
    end
  end
end
