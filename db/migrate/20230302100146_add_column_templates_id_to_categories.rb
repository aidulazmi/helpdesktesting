class AddColumnTemplatesIdToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :templates_id, :string
  end
end
