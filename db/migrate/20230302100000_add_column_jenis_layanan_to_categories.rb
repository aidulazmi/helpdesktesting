class AddColumnJenisLayananToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :jenis_layanan, :string
  end
end
