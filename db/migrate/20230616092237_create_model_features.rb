class CreateModelFeatures < ActiveRecord::Migration[5.2]
  def change
    create_table :model_features do |t|
      t.references :model, polymorphic: true
      t.references :feature, foreign_key: true

      t.timestamps
    end
  end
end
