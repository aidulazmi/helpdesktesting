class AddColumnJenisLayananKategoriToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :jenis_layanan_kategori, :string
  end
end
