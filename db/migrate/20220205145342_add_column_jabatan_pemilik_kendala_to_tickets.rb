class AddColumnJabatanPemilikKendalaToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :jabatan_pemilik_kendala, :string
  end
end
