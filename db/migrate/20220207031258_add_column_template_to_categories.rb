class AddColumnTemplateToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :template, :string
  end
end
