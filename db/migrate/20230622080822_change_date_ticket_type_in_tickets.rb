class ChangeDateTicketTypeInTickets < ActiveRecord::Migration[5.1]
  def change
    change_column :tickets, :date_ticket, :datetime, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
