class AddColumnSerialNumberToAssetsusers < ActiveRecord::Migration[5.2]
  def change
    add_column :assetsusers, :serial_number, :string
  end
end
