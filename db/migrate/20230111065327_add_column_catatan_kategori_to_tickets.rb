class AddColumnCatatanKategoriToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :catatan_kategori, :string
  end
end
