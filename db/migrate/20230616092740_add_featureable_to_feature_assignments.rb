class AddFeatureableToFeatureAssignments < ActiveRecord::Migration[5.2]
  def change
    add_column :feature_assignments, :featureable_type, :string
    add_column :feature_assignments, :featureable_id, :integer
    add_index :feature_assignments, :featureable_id
  end
end
