class RenameTableAssetsToCollectionAssets < ActiveRecord::Migration[5.2]
  def change
    rename_table :assets, :collection_assets
  end
end
