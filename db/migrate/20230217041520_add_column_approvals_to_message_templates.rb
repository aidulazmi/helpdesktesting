class AddColumnApprovalsToMessageTemplates < ActiveRecord::Migration[5.2]
  def change
    add_column :message_templates, :approvals, :boolean
  end
end
