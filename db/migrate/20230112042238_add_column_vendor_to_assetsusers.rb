class AddColumnVendorToAssetsusers < ActiveRecord::Migration[5.2]
  def change
    add_column :assetsusers, :vendor, :string
  end
end
