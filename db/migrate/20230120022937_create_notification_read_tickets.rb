class CreateNotificationReadTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :notification_read_tickets do |t|
      t.references :user, foreign_key: true
      t.references :ticket, foreign_key: true
      t.boolean :read
    end
  end
end
