class CreateAssetlisensis < ActiveRecord::Migration[5.2]
  def change
    create_table :assetlisensis do |t|
      t.string :type_lisensi
      t.string :nama_lisensi
      t.string :jumlah_lisensi
      t.string :divisi
      t.string :penerima
      t.string :lokasi
      t.string :tahun_pembelian
      t.date :waktu_instalasi
      t.date :waktu_expired
      t.string :nama_perangkat
      t.string :serial_number
      t.string :pic
      t.string :upload
      t.text :keterangan

      t.timestamps
    end
  end
end
