class CreateAssetsusers < ActiveRecord::Migration[5.2]
  def change
    create_table :assetsusers do |t|
      t.string :user
      t.string :divisi
      t.string :nip
      t.string :lokasi
      t.string :nama_perangkat
      t.string :tahun
      t.string :jenis_kontrak
      t.integer :wo_ke
      t.string :nama_komputer
      t.string :merk
      t.text :tipe
      t.string :no_inventaris
      t.string :penyedia
      t.text :keterangan

      t.timestamps
    end
  end
end
