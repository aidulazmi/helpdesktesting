class AddSatisfactionToTicketProblem < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_problems, :satisfaction, :integer
    add_column :ticket_problems, :satisfaction_note, :string
  end
end
