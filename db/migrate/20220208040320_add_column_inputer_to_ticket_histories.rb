class AddColumnInputerToTicketHistories < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_histories, :inputer, :string
  end
end
