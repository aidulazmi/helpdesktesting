class CreateTicketHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_histories do |t|
      t.references :ticket, foreign_key: true
      t.string :no_tiket
      t.string :status_tiket
      t.string :keterangan
      t.timestamp :waktu

      t.timestamps
    end
  end
end
