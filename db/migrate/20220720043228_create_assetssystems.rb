class CreateAssetssystems < ActiveRecord::Migration[5.2]
  def change
    create_table :assetssystems do |t|
      t.string :nama
      t.string :nama_perangkat
      t.string :type
      t.string :lokasi
      t.string :tahun

      t.timestamps
    end
  end
end
