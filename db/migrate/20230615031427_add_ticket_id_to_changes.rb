class AddTicketIdToChanges < ActiveRecord::Migration[5.2]
  def change
    add_column :changes, :ticket_id, :integer
    add_index :changes, :ticket_id
  end
end