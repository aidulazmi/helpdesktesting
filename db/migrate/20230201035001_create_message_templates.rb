class CreateMessageTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :message_templates do |t|
      t.string :template
      t.string :sub_category
      t.string :jenis_layanan

      t.timestamps
    end
  end
end
