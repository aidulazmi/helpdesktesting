class AddColumnNameToMessageTemplates < ActiveRecord::Migration[5.2]
  def change
    add_column :message_templates, :name, :string
  end
end
