class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :no_tiket
      t.string :nama_pemohon
      t.string :divisi_pemohon
      t.string :area_pemohon
      t.string :nama_pemilik_kendala
      t.string :divisi_pemilik_kendala
      t.string :area_pemilik_kendala
      t.string :kategori
      t.string :sub_kategori
      t.string :jenis_layanan
      t.references :asset, foreign_key: true
      t.string :keterangan
      t.string :teknisi
      t.string :status_teknisi
      t.string :status_tiket

      t.timestamps
    end
  end
end
