class AddColumWorkAroundToTicketHistories < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_histories, :work_around, :string
  end
end
