class AddColumnAssetsToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :assets, :string
  end
end
