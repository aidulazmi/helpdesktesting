class AddColumnIssuedByToTicketHistories < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_histories, :issued_by, :string
  end
end
