class AddColumnAreaIdToRoleAssignments < ActiveRecord::Migration[5.2]
  def change
    add_reference :role_assignments, :area, foreign_key: true
  end
end
