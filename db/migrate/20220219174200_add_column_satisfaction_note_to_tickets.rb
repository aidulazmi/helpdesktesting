class AddColumnSatisfactionNoteToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :satisfaction_note, :string
  end
end
