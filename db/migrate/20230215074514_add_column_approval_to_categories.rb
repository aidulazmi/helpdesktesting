class AddColumnApprovalToCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :categories, :approval, :boolean, default: false
  end
end
