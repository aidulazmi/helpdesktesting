class AddFeatureToFeatures < ActiveRecord::Migration[5.2]
  def change
    add_reference :features, :feature, polymorphic: true
  end
end
