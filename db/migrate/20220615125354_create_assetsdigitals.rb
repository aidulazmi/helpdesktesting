class CreateAssetsdigitals < ActiveRecord::Migration[5.2]
  def change
    create_table :assetsdigitals do |t|
      t.string :nama_aplikasi
      t.string :pemilik_proses
      t.string :deskripsi
      t.string :pic
      t.string :bahasa
      t.string :framework
      t.string :current_version
      t.string :lisensi
      t.string :antivirus
      t.string :server_hardening
      t.string :intruder_incident
      t.string :availability
      t.string :pemeliharaan
      t.string :bug_fixing
      t.date :waktu_respon
      t.string :jam_layanan
      t.date :waktu_eskalasi
      t.date :waktu_respon2
      t.string :pemeliharaan
      t.integer :periode
      t.integer :retensi
      t.string :storage_server
      t.string :server
      t.string :internet
      t.string :proteksi_akses
      t.string :antivirus
      t.string :domain
      t.string :bug_fixing
      t.string :uji_keamanan
      t.integer :biaya_hardware
      t.integer :biaya_jaringan
      t.integer :biaya_keamanan
      t.integer :biaya_pemeliharaan
      t.integer :total

      t.timestamps
    end
  end
end
