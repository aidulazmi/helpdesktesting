class CreateStatusTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :status_tickets do |t|
      t.string :nama

      t.timestamps
    end
  end
end
