class AddColumnConfirmAdminToTicketApprovals < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_approvals, :confirm_admin, :boolean
  end
end
