class DeleteReferencesTableAssetInTickets < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key :tickets, :assets
    remove_column :tickets, :asset_id, :integer
  end
end
