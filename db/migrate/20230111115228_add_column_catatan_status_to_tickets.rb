class AddColumnCatatanStatusToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :catatan_status, :string
  end
end
