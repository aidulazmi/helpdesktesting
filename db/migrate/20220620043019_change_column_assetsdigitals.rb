class ChangeColumnAssetsdigitals < ActiveRecord::Migration[5.2]
  def change
    change_column :assetsdigitals, :waktu_respon, :string
    change_column :assetsdigitals, :waktu_eskalasi, :string
  end
end
