class CreateTicketApprovals < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_approvals do |t|
      t.references :ticket, foreign_key: true
      t.references :user, foreign_key: true
      t.string :status
      t.string :catatan

      t.timestamps
    end
  end
end
