class AddDateTicketToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :date_ticket, :date
  end
end
