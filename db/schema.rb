# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_06_24_024437) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "areas", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assetlisensis", force: :cascade do |t|
    t.string "type_lisensi"
    t.string "nama_lisensi"
    t.string "jumlah_lisensi"
    t.string "divisi"
    t.string "penerima"
    t.string "lokasi"
    t.string "tahun_pembelian"
    t.date "waktu_instalasi"
    t.date "waktu_expired"
    t.string "nama_perangkat"
    t.string "serial_number"
    t.string "pic"
    t.string "upload"
    t.text "keterangan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assetsdigitals", force: :cascade do |t|
    t.string "nama_aplikasi"
    t.string "pemilik_proses"
    t.string "deskripsi"
    t.string "pic"
    t.string "bahasa"
    t.string "framework"
    t.string "current_version"
    t.string "lisensi"
    t.string "antivirus"
    t.string "server_hardening"
    t.string "intruder_incident"
    t.string "availability"
    t.string "pemeliharaan"
    t.string "bug_fixing"
    t.string "waktu_respon"
    t.string "jam_layanan"
    t.string "waktu_eskalasi"
    t.date "waktu_respon2"
    t.integer "periode"
    t.integer "retensi"
    t.string "storage_server"
    t.string "server"
    t.string "internet"
    t.string "proteksi_akses"
    t.string "domain"
    t.string "uji_keamanan"
    t.integer "biaya_hardware"
    t.integer "biaya_jaringan"
    t.integer "biaya_keamanan"
    t.integer "biaya_pemeliharaan"
    t.integer "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "assetssits", force: :cascade do |t|
    t.string "nama_perangkat"
    t.string "merk"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tipe"
    t.string "serial_number"
  end

  create_table "assetssystems", force: :cascade do |t|
    t.string "nama"
    t.string "nama_perangkat"
    t.string "type"
    t.string "lokasi"
    t.string "tahun"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tipe_perangkat"
  end

  create_table "assetsusers", force: :cascade do |t|
    t.string "user"
    t.string "divisi"
    t.string "nip"
    t.string "lokasi"
    t.string "nama_perangkat"
    t.string "tahun"
    t.string "jenis_kontrak"
    t.integer "wo_ke"
    t.string "nama_komputer"
    t.string "merk"
    t.text "tipe"
    t.string "no_inventaris"
    t.string "penyedia"
    t.text "keterangan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "serial_number"
    t.string "mac_address"
    t.string "vendor"
    t.string "status_pekerja"
    t.string "email"
    t.string "no_hp"
    t.string "status_laptop"
    t.string "periode_laptop"
    t.string "spesifikasi"
    t.string "no_hbb"
    t.string "no_bast"
    t.string "manual_user"
    t.string "jenis_perangkat"
  end

  create_table "books", force: :cascade do |t|
    t.text "title"
    t.string "author"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "sub_category"
    t.bigint "service_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "template"
    t.string "status_template_tiket"
    t.string "pic"
    t.boolean "approval", default: false
    t.string "approved"
    t.string "jenis_layanan"
    t.string "templates_id"
    t.string "jenis_layanan_kategori"
    t.string "deskripsi_kategori"
    t.index ["service_type_id"], name: "index_categories_on_service_type_id"
  end

  create_table "changes", force: :cascade do |t|
    t.date "tanggal"
    t.string "judul"
    t.string "teknisi"
    t.string "kategori_asset"
    t.string "nama_perangkat"
    t.string "ram"
    t.string "nama_aplikasi"
    t.string "pemilik_proses"
    t.text "deskripsi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nama_system"
    t.string "lokasi"
    t.string "nama_user"
    t.integer "ticket_id"
    t.index ["ticket_id"], name: "index_changes_on_ticket_id"
  end

  create_table "collection_assets", force: :cascade do |t|
    t.string "device_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "jenis_layanan"
  end

  create_table "divisions", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feature_assignments", force: :cascade do |t|
    t.bigint "role_id"
    t.bigint "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "featureable_type"
    t.integer "featureable_id"
    t.index ["feature_id"], name: "index_feature_assignments_on_feature_id"
    t.index ["featureable_id"], name: "index_feature_assignments_on_featureable_id"
    t.index ["role_id"], name: "index_feature_assignments_on_role_id"
  end

  create_table "features", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "feature_type"
    t.bigint "feature_id"
    t.index ["feature_type", "feature_id"], name: "index_features_on_feature_type_and_feature_id"
  end

  create_table "loss_events", force: :cascade do |t|
    t.string "nama"
    t.date "tanggal"
    t.string "tempat"
    t.string "status"
    t.string "pic"
    t.string "deskripsi_kejadian"
    t.string "kategori"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "message_templates", force: :cascade do |t|
    t.string "template"
    t.string "sub_category"
    t.string "jenis_layanan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "category_id"
    t.string "kategori"
    t.boolean "approvals"
    t.string "name"
    t.index ["category_id"], name: "index_message_templates_on_category_id"
  end

  create_table "model_features", force: :cascade do |t|
    t.string "model_type"
    t.bigint "model_id"
    t.bigint "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feature_id"], name: "index_model_features_on_feature_id"
    t.index ["model_type", "model_id"], name: "index_model_features_on_model_type_and_model_id"
  end

  create_table "notification_read_tickets", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "ticket_id"
    t.boolean "read"
    t.bigint "ticket_history_id"
    t.index ["ticket_history_id"], name: "index_notification_read_tickets_on_ticket_history_id"
    t.index ["ticket_id"], name: "index_notification_read_tickets_on_ticket_id"
    t.index ["user_id"], name: "index_notification_read_tickets_on_user_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.string "message"
    t.string "action"
    t.string "controller"
    t.boolean "read", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_notifications_on_role_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "novels", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pickets", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.integer "publication_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "positions", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "priority"
  end

  create_table "posts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "role_assignments", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "area_id"
    t.index ["area_id"], name: "index_role_assignments_on_area_id"
    t.index ["role_id"], name: "index_role_assignments_on_role_id"
    t.index ["user_id"], name: "index_role_assignments_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name", null: false
    t.text "permissions"
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_types", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "status_tickets", force: :cascade do |t|
    t.string "nama"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tests", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.integer "publication_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ticket_approvals", force: :cascade do |t|
    t.bigint "ticket_id"
    t.bigint "user_id"
    t.string "status"
    t.string "catatan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "confirm_admin"
    t.index ["ticket_id"], name: "index_ticket_approvals_on_ticket_id"
    t.index ["user_id"], name: "index_ticket_approvals_on_user_id"
  end

  create_table "ticket_histories", force: :cascade do |t|
    t.bigint "ticket_id"
    t.string "no_tiket"
    t.string "status_tiket"
    t.string "keterangan"
    t.datetime "waktu"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "inputer"
    t.string "issued_by"
    t.string "work_around"
    t.index ["ticket_id"], name: "index_ticket_histories_on_ticket_id"
  end

  create_table "ticket_problems", force: :cascade do |t|
    t.bigint "ticket_id"
    t.string "no_ticket_problem"
    t.string "status"
    t.string "teknisi"
    t.string "priority"
    t.string "kategori"
    t.string "sub_kategori"
    t.string "keterangan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "catatn_status"
    t.string "catatan_status"
    t.integer "satisfaction"
    t.string "satisfaction_note"
    t.index ["ticket_id"], name: "index_ticket_problems_on_ticket_id"
  end

  create_table "tickets", force: :cascade do |t|
    t.string "no_tiket"
    t.string "nama_pemohon"
    t.string "divisi_pemohon"
    t.string "area_pemohon"
    t.string "nama_pemilik_kendala"
    t.string "divisi_pemilik_kendala"
    t.string "area_pemilik_kendala"
    t.string "kategori"
    t.string "sub_kategori"
    t.string "jenis_layanan"
    t.string "keterangan"
    t.string "teknisi"
    t.string "status_teknisi"
    t.string "status_tiket"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pemilik_kendala"
    t.string "assets"
    t.string "jabatan_pemilik_kendala"
    t.string "priority"
    t.string "resolution_time"
    t.integer "satisfaction"
    t.string "satisfaction_note"
    t.string "judul"
    t.string "catatan_kategori"
    t.string "catatan_status"
    t.datetime "date_ticket", default: -> { "now()" }
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "username"
    t.string "name"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "sign_in_count"
    t.bigint "division_id"
    t.string "user_type"
    t.bigint "position_id"
    t.index ["division_id"], name: "index_users_on_division_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["position_id"], name: "index_users_on_position_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "categories", "service_types"
  add_foreign_key "feature_assignments", "features"
  add_foreign_key "feature_assignments", "roles"
  add_foreign_key "message_templates", "categories"
  add_foreign_key "model_features", "features"
  add_foreign_key "notification_read_tickets", "ticket_histories"
  add_foreign_key "notification_read_tickets", "tickets"
  add_foreign_key "notification_read_tickets", "users"
  add_foreign_key "notifications", "roles"
  add_foreign_key "notifications", "users"
  add_foreign_key "role_assignments", "areas"
  add_foreign_key "role_assignments", "roles"
  add_foreign_key "role_assignments", "users"
  add_foreign_key "ticket_approvals", "tickets"
  add_foreign_key "ticket_approvals", "users"
  add_foreign_key "ticket_histories", "tickets"
  add_foreign_key "ticket_problems", "tickets"
  add_foreign_key "users", "divisions"
  add_foreign_key "users", "positions"
end
