--
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.11 (Ubuntu 12.11-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE helpdesk;
--
-- Name: helpdesk; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE helpdesk WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE helpdesk OWNER TO postgres;

\connect helpdesk

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ar_internal_metadata OWNER TO postgres;

--
-- Name: areas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.areas (
    id bigint NOT NULL,
    nama character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.areas OWNER TO postgres;

--
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.areas_id_seq OWNER TO postgres;

--
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.areas_id_seq OWNED BY public.areas.id;


--
-- Name: assetsdigitals; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assetsdigitals (
    id bigint NOT NULL,
    nama_aplikasi character varying,
    pemilik_proses character varying,
    deskripsi character varying,
    pic character varying,
    bahasa character varying,
    framework character varying,
    current_version character varying,
    lisensi character varying,
    antivirus character varying,
    server_hardening character varying,
    intruder_incident character varying,
    availability character varying,
    pemeliharaan character varying,
    bug_fixing character varying,
    waktu_respon character varying,
    jam_layanan character varying,
    waktu_eskalasi character varying,
    periode integer,
    retensi integer,
    storage_server character varying,
    server character varying,
    internet character varying,
    proteksi_akses character varying,
    domain character varying,
    uji_keamanan character varying,
    biaya_hardware integer,
    biaya_jaringan integer,
    biaya_keamanan integer,
    biaya_pemeliharaan integer,
    total integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    waktu_respon2 character varying(25)
);


ALTER TABLE public.assetsdigitals OWNER TO postgres;

--
-- Name: assetsdigitals_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assetsdigitals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assetsdigitals_id_seq OWNER TO postgres;

--
-- Name: assetsdigitals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assetsdigitals_id_seq OWNED BY public.assetsdigitals.id;


--
-- Name: assetssits; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assetssits (
    id bigint NOT NULL,
    nama_perangkat character varying,
    merk character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    tipe text
);


ALTER TABLE public.assetssits OWNER TO postgres;

--
-- Name: assetssits_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assetssits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assetssits_id_seq OWNER TO postgres;

--
-- Name: assetssits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assetssits_id_seq OWNED BY public.assetssits.id;


--
-- Name: assetsusers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.assetsusers (
    id bigint NOT NULL,
    "user" character varying,
    divisi character varying,
    nip character varying,
    lokasi character varying,
    nama_perangkat character varying,
    tahun character varying,
    jenis_kontrak character varying,
    wo_ke integer,
    nama_komputer character varying,
    merk character varying,
    tipe text,
    no_inventaris character varying,
    penyedia character varying,
    keterangan text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.assetsusers OWNER TO postgres;

--
-- Name: assetsusers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.assetsusers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.assetsusers_id_seq OWNER TO postgres;

--
-- Name: assetsusers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.assetsusers_id_seq OWNED BY public.assetsusers.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    id bigint NOT NULL,
    name character varying,
    sub_category character varying,
    service_type_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    template character varying,
    status_template_tiket character varying,
    pic character varying(30)
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categories_id_seq OWNER TO postgres;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;


--
-- Name: collection_assets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.collection_assets (
    id bigint NOT NULL,
    device_name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    kode character varying(20)
);


ALTER TABLE public.collection_assets OWNER TO postgres;

--
-- Name: collection_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.collection_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collection_assets_id_seq OWNER TO postgres;

--
-- Name: collection_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.collection_assets_id_seq OWNED BY public.collection_assets.id;


--
-- Name: divisions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.divisions (
    id bigint NOT NULL,
    nama character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    kode character varying(10)
);


ALTER TABLE public.divisions OWNER TO postgres;

--
-- Name: divisions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.divisions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.divisions_id_seq OWNER TO postgres;

--
-- Name: divisions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.divisions_id_seq OWNED BY public.divisions.id;


--
-- Name: positions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.positions (
    id bigint NOT NULL,
    nama character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    priority character varying
);


ALTER TABLE public.positions OWNER TO postgres;

--
-- Name: positions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.positions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.positions_id_seq OWNER TO postgres;

--
-- Name: positions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.positions_id_seq OWNED BY public.positions.id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.posts (
    id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.posts OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;


--
-- Name: role_assignments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_assignments (
    id bigint NOT NULL,
    user_id bigint,
    role_id bigint,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    area_id bigint
);


ALTER TABLE public.role_assignments OWNER TO postgres;

--
-- Name: role_assignments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.role_assignments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_assignments_id_seq OWNER TO postgres;

--
-- Name: role_assignments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.role_assignments_id_seq OWNED BY public.role_assignments.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying NOT NULL,
    permissions text,
    type character varying NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: service_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.service_types (
    id bigint NOT NULL,
    nama character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.service_types OWNER TO postgres;

--
-- Name: service_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.service_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.service_types_id_seq OWNER TO postgres;

--
-- Name: service_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.service_types_id_seq OWNED BY public.service_types.id;


--
-- Name: status_tickets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status_tickets (
    id bigint NOT NULL,
    nama character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.status_tickets OWNER TO postgres;

--
-- Name: status_tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.status_tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_tickets_id_seq OWNER TO postgres;

--
-- Name: status_tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.status_tickets_id_seq OWNED BY public.status_tickets.id;


--
-- Name: ticket_histories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ticket_histories (
    id bigint NOT NULL,
    ticket_id bigint,
    no_tiket character varying,
    status_tiket character varying,
    keterangan character varying,
    waktu timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    inputer character varying,
    issued_by character varying
);


ALTER TABLE public.ticket_histories OWNER TO postgres;

--
-- Name: ticket_histories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ticket_histories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ticket_histories_id_seq OWNER TO postgres;

--
-- Name: ticket_histories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ticket_histories_id_seq OWNED BY public.ticket_histories.id;


--
-- Name: tickets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tickets (
    id bigint NOT NULL,
    no_tiket character varying,
    nama_pemohon character varying,
    divisi_pemohon character varying,
    area_pemohon character varying,
    nama_pemilik_kendala character varying,
    divisi_pemilik_kendala character varying,
    area_pemilik_kendala character varying,
    kategori character varying,
    sub_kategori character varying,
    jenis_layanan character varying,
    keterangan character varying,
    teknisi character varying,
    status_teknisi character varying,
    status_tiket character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    pemilik_kendala integer,
    assets character varying,
    jabatan_pemilik_kendala character varying,
    priority character varying,
    resolution_time character varying,
    satisfaction integer,
    satisfaction_note character varying
);


ALTER TABLE public.tickets OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tickets_id_seq OWNER TO postgres;

--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tickets_id_seq OWNED BY public.tickets.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    username character varying,
    name character varying,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    sign_in_count integer,
    division_id bigint,
    user_type character varying,
    position_id bigint,
    header character varying(100),
    telegram_id character varying(20)
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: areas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.areas ALTER COLUMN id SET DEFAULT nextval('public.areas_id_seq'::regclass);


--
-- Name: assetsdigitals id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetsdigitals ALTER COLUMN id SET DEFAULT nextval('public.assetsdigitals_id_seq'::regclass);


--
-- Name: assetssits id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetssits ALTER COLUMN id SET DEFAULT nextval('public.assetssits_id_seq'::regclass);


--
-- Name: assetsusers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetsusers ALTER COLUMN id SET DEFAULT nextval('public.assetsusers_id_seq'::regclass);


--
-- Name: categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);


--
-- Name: collection_assets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collection_assets ALTER COLUMN id SET DEFAULT nextval('public.collection_assets_id_seq'::regclass);


--
-- Name: divisions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisions ALTER COLUMN id SET DEFAULT nextval('public.divisions_id_seq'::regclass);


--
-- Name: positions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions ALTER COLUMN id SET DEFAULT nextval('public.positions_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);


--
-- Name: role_assignments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_assignments ALTER COLUMN id SET DEFAULT nextval('public.role_assignments_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: service_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.service_types ALTER COLUMN id SET DEFAULT nextval('public.service_types_id_seq'::regclass);


--
-- Name: status_tickets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status_tickets ALTER COLUMN id SET DEFAULT nextval('public.status_tickets_id_seq'::regclass);


--
-- Name: ticket_histories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_histories ALTER COLUMN id SET DEFAULT nextval('public.ticket_histories_id_seq'::regclass);


--
-- Name: tickets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets ALTER COLUMN id SET DEFAULT nextval('public.tickets_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: ar_internal_metadata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM stdin;
\.
COPY public.ar_internal_metadata (key, value, created_at, updated_at) FROM '$$PATH$$/3142.dat';

--
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.areas (id, nama, created_at, updated_at) FROM stdin;
\.
COPY public.areas (id, nama, created_at, updated_at) FROM '$$PATH$$/3143.dat';

--
-- Data for Name: assetsdigitals; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assetsdigitals (id, nama_aplikasi, pemilik_proses, deskripsi, pic, bahasa, framework, current_version, lisensi, antivirus, server_hardening, intruder_incident, availability, pemeliharaan, bug_fixing, waktu_respon, jam_layanan, waktu_eskalasi, periode, retensi, storage_server, server, internet, proteksi_akses, domain, uji_keamanan, biaya_hardware, biaya_jaringan, biaya_keamanan, biaya_pemeliharaan, total, created_at, updated_at, waktu_respon2) FROM stdin;
\.
COPY public.assetsdigitals (id, nama_aplikasi, pemilik_proses, deskripsi, pic, bahasa, framework, current_version, lisensi, antivirus, server_hardening, intruder_incident, availability, pemeliharaan, bug_fixing, waktu_respon, jam_layanan, waktu_eskalasi, periode, retensi, storage_server, server, internet, proteksi_akses, domain, uji_keamanan, biaya_hardware, biaya_jaringan, biaya_keamanan, biaya_pemeliharaan, total, created_at, updated_at, waktu_respon2) FROM '$$PATH$$/3175.dat';

--
-- Data for Name: assetssits; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assetssits (id, nama_perangkat, merk, created_at, updated_at, tipe) FROM stdin;
\.
COPY public.assetssits (id, nama_perangkat, merk, created_at, updated_at, tipe) FROM '$$PATH$$/3171.dat';

--
-- Data for Name: assetsusers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.assetsusers (id, "user", divisi, nip, lokasi, nama_perangkat, tahun, jenis_kontrak, wo_ke, nama_komputer, merk, tipe, no_inventaris, penyedia, keterangan, created_at, updated_at) FROM stdin;
\.
COPY public.assetsusers (id, "user", divisi, nip, lokasi, nama_perangkat, tahun, jenis_kontrak, wo_ke, nama_komputer, merk, tipe, no_inventaris, penyedia, keterangan, created_at, updated_at) FROM '$$PATH$$/3173.dat';

--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (id, name, sub_category, service_type_id, created_at, updated_at, template, status_template_tiket, pic) FROM stdin;
\.
COPY public.categories (id, name, sub_category, service_type_id, created_at, updated_at, template, status_template_tiket, pic) FROM '$$PATH$$/3145.dat';

--
-- Data for Name: collection_assets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.collection_assets (id, device_name, created_at, updated_at, kode) FROM stdin;
\.
COPY public.collection_assets (id, device_name, created_at, updated_at, kode) FROM '$$PATH$$/3147.dat';

--
-- Data for Name: divisions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.divisions (id, nama, created_at, updated_at, kode) FROM stdin;
\.
COPY public.divisions (id, nama, created_at, updated_at, kode) FROM '$$PATH$$/3149.dat';

--
-- Data for Name: positions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.positions (id, nama, created_at, updated_at, priority) FROM stdin;
\.
COPY public.positions (id, nama, created_at, updated_at, priority) FROM '$$PATH$$/3151.dat';

--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.posts (id, created_at, updated_at) FROM stdin;
\.
COPY public.posts (id, created_at, updated_at) FROM '$$PATH$$/3169.dat';

--
-- Data for Name: role_assignments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_assignments (id, user_id, role_id, created_at, updated_at, area_id) FROM stdin;
\.
COPY public.role_assignments (id, user_id, role_id, created_at, updated_at, area_id) FROM '$$PATH$$/3153.dat';

--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, permissions, type, created_at, updated_at) FROM stdin;
\.
COPY public.roles (id, name, permissions, type, created_at, updated_at) FROM '$$PATH$$/3155.dat';

--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.schema_migrations (version) FROM stdin;
\.
COPY public.schema_migrations (version) FROM '$$PATH$$/3157.dat';

--
-- Data for Name: service_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.service_types (id, nama, created_at, updated_at) FROM stdin;
\.
COPY public.service_types (id, nama, created_at, updated_at) FROM '$$PATH$$/3158.dat';

--
-- Data for Name: status_tickets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status_tickets (id, nama, created_at, updated_at) FROM stdin;
\.
COPY public.status_tickets (id, nama, created_at, updated_at) FROM '$$PATH$$/3160.dat';

--
-- Data for Name: ticket_histories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ticket_histories (id, ticket_id, no_tiket, status_tiket, keterangan, waktu, created_at, updated_at, inputer, issued_by) FROM stdin;
\.
COPY public.ticket_histories (id, ticket_id, no_tiket, status_tiket, keterangan, waktu, created_at, updated_at, inputer, issued_by) FROM '$$PATH$$/3162.dat';

--
-- Data for Name: tickets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tickets (id, no_tiket, nama_pemohon, divisi_pemohon, area_pemohon, nama_pemilik_kendala, divisi_pemilik_kendala, area_pemilik_kendala, kategori, sub_kategori, jenis_layanan, keterangan, teknisi, status_teknisi, status_tiket, created_at, updated_at, pemilik_kendala, assets, jabatan_pemilik_kendala, priority, resolution_time, satisfaction, satisfaction_note) FROM stdin;
\.
COPY public.tickets (id, no_tiket, nama_pemohon, divisi_pemohon, area_pemohon, nama_pemilik_kendala, divisi_pemilik_kendala, area_pemilik_kendala, kategori, sub_kategori, jenis_layanan, keterangan, teknisi, status_teknisi, status_tiket, created_at, updated_at, pemilik_kendala, assets, jabatan_pemilik_kendala, priority, resolution_time, satisfaction, satisfaction_note) FROM '$$PATH$$/3164.dat';

--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at, username, name, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, sign_in_count, division_id, user_type, position_id, header, telegram_id) FROM stdin;
\.
COPY public.users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, created_at, updated_at, username, name, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, sign_in_count, division_id, user_type, position_id, header, telegram_id) FROM '$$PATH$$/3166.dat';

--
-- Name: areas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.areas_id_seq', 55, true);


--
-- Name: assetsdigitals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assetsdigitals_id_seq', 4, true);


--
-- Name: assetssits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assetssits_id_seq', 3, true);


--
-- Name: assetsusers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.assetsusers_id_seq', 24, true);


--
-- Name: categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categories_id_seq', 15, true);


--
-- Name: collection_assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.collection_assets_id_seq', 4, true);


--
-- Name: divisions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.divisions_id_seq', 322, true);


--
-- Name: positions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.positions_id_seq', 5, true);


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.posts_id_seq', 1, false);


--
-- Name: role_assignments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.role_assignments_id_seq', 108, true);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 5, true);


--
-- Name: service_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.service_types_id_seq', 3, true);


--
-- Name: status_tickets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_tickets_id_seq', 7, true);


--
-- Name: ticket_histories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ticket_histories_id_seq', 344, true);


--
-- Name: tickets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tickets_id_seq', 195, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 47, true);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: areas areas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.areas
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);


--
-- Name: assetsdigitals assetsdigitals_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetsdigitals
    ADD CONSTRAINT assetsdigitals_pkey PRIMARY KEY (id);


--
-- Name: assetssits assetssits_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetssits
    ADD CONSTRAINT assetssits_pkey PRIMARY KEY (id);


--
-- Name: assetsusers assetsusers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.assetsusers
    ADD CONSTRAINT assetsusers_pkey PRIMARY KEY (id);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: collection_assets collection_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.collection_assets
    ADD CONSTRAINT collection_assets_pkey PRIMARY KEY (id);


--
-- Name: divisions divisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.divisions
    ADD CONSTRAINT divisions_pkey PRIMARY KEY (id);


--
-- Name: positions positions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: role_assignments role_assignments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_assignments
    ADD CONSTRAINT role_assignments_pkey PRIMARY KEY (id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: service_types service_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.service_types
    ADD CONSTRAINT service_types_pkey PRIMARY KEY (id);


--
-- Name: status_tickets status_tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status_tickets
    ADD CONSTRAINT status_tickets_pkey PRIMARY KEY (id);


--
-- Name: ticket_histories ticket_histories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_histories
    ADD CONSTRAINT ticket_histories_pkey PRIMARY KEY (id);


--
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_categories_on_service_type_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_categories_on_service_type_id ON public.categories USING btree (service_type_id);


--
-- Name: index_role_assignments_on_area_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_role_assignments_on_area_id ON public.role_assignments USING btree (area_id);


--
-- Name: index_role_assignments_on_role_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_role_assignments_on_role_id ON public.role_assignments USING btree (role_id);


--
-- Name: index_role_assignments_on_user_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_role_assignments_on_user_id ON public.role_assignments USING btree (user_id);


--
-- Name: index_ticket_histories_on_ticket_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_ticket_histories_on_ticket_id ON public.ticket_histories USING btree (ticket_id);


--
-- Name: index_users_on_division_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_users_on_division_id ON public.users USING btree (division_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX index_users_on_email ON public.users USING btree (email);


--
-- Name: index_users_on_position_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX index_users_on_position_id ON public.users USING btree (position_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON public.users USING btree (reset_password_token);


--
-- Name: users fk_rails_16a05b2834; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_16a05b2834 FOREIGN KEY (division_id) REFERENCES public.divisions(id);


--
-- Name: users fk_rails_2d26d9377b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_rails_2d26d9377b FOREIGN KEY (position_id) REFERENCES public.positions(id);


--
-- Name: role_assignments fk_rails_72bdc1088d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_assignments
    ADD CONSTRAINT fk_rails_72bdc1088d FOREIGN KEY (area_id) REFERENCES public.areas(id);


--
-- Name: role_assignments fk_rails_8ddd873ee0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_assignments
    ADD CONSTRAINT fk_rails_8ddd873ee0 FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: categories fk_rails_b04ea358cd; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT fk_rails_b04ea358cd FOREIGN KEY (service_type_id) REFERENCES public.service_types(id);


--
-- Name: role_assignments fk_rails_e4bfc1cd2c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_assignments
    ADD CONSTRAINT fk_rails_e4bfc1cd2c FOREIGN KEY (role_id) REFERENCES public.roles(id);


--
-- Name: ticket_histories fk_rails_f1103a6701; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ticket_histories
    ADD CONSTRAINT fk_rails_f1103a6701 FOREIGN KEY (ticket_id) REFERENCES public.tickets(id);


--
-- PostgreSQL database dump complete
--

